<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_sidebar extends CI_Model
{

    //VIEW LEFT MENU
    function left_menu()
    {
        $query = "SELECT * FROM tbl_menu WHERE deleted_date IS NULL AND parent = '' ORDER BY urutan";
        $data = $this->db->query($query);

        return $data;
    }

    function left_menu_child($id)
    {
        $query = "SELECT * FROM tbl_menu WHERE deleted_date IS NULL AND parent = '{$id}' ORDER BY urutan";
        $data = $this->db->query($query);

        return $data;
    }

    // ACCESS CONTROL LIST (action: view, add, edit, del)
    function access($action, $menucode)
    {
        $query = "  SELECT count(a.id_menuakses) menuview 
                    FROM menu_akses a 
                    LEFT JOIN tbl_menu b ON a.id_menu = b.id_menu AND b.deleted_date IS NULL
                    WHERE b.kode_menu= '{$menucode}'
                    AND a.grup_id= '{$this->session->userdata('grup_id')}'
                    AND  a.`{$action}` = 1";
        $data = $this->db->query($query);

        return $data->row();
    }
}
