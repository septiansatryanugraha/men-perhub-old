<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_utilities extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function selectSlider()
    {
        $sql = "SELECT * FROM tbl_slider
                WHERE deleted_date IS NULL";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectEmail()
    {
        $sql = "SELECT email FROM tbl_user 
                WHERE deleted_date IS NULL";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectJenjang()
    {
        $sql = "SELECT * FROM jenis_jenjang
                WHERE deleted_date IS NULL 
                AND jenjang <> 'Pembantu Penguji'";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectDiklat()
    {
        $sql = "SELECT * FROM `tbl_kategori_diklat`
                WHERE deleted_date IS NULL 
                ORDER BY id_kategori_diklat ASC";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectStatusGrup($where = [], $whereIn = [])
    {
        $sql = "SELECT * FROM status_grup
                WHERE deleted_date IS NULL";
        if (!empty($where)) {
            foreach ($where as $key => $value) {
                $sql .= " AND " . $key . " = '{$value}'";
            }
        }
        if (!empty($whereIn)) {
            $sql .= " AND nama IN (";
            foreach ($whereIn as $key => $value) {
                if ($key > 0) {
                    $sql .= ",";
                }
                $sql .= "'{$value}'";
            }
            $sql .= ")";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectKategoriStatus($where = [], $whereIn = [])
    {
        $sql = "SELECT * FROM kategori_status
                WHERE deleted_date IS NULL";
        if (!empty($where)) {
            foreach ($where as $key => $value) {
                $sql .= " AND " . $key . " = '{$value}'";
            }
        }
        if (!empty($whereIn)) {
            $sql .= " AND nama_kategori IN (";
            foreach ($whereIn as $key => $value) {
                if ($key > 0) {
                    $sql .= ",";
                }
                $sql .= "'{$value}'";
            }
            $sql .= ")";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectGroup()
    {
        $this->db->from('grup');
        $this->db->where('deleted_date IS NULL');
        $data = $this->db->get();

        return $data->result();
    }

    public function selectKontak()
    {
        $sql = "SELECT * FROM tbl_kontak
                WHERE deleted_date IS NULL";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function selectUserDiklat($id)
    {
        $sql = "SELECT * FROM `tbl_diklat`
                WHERE deleted_date IS NULL AND jenis_diklat ='{$id}' AND status_pengajuan='Lolos'
                AND tanggal between DATE_ADD(date(now()), INTERVAL -90 DAY) 
                AND date(now())";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function selectUserJenjang($id)
    {
        $sql = "SELECT * FROM `tbl_kenaikan_jenjang
                WHERE deleted_date IS NULL AND jenis_jenjang ='{$id}' AND status_pengajuan='Lolos'
                AND tanggal between DATE_ADD(date(now()), INTERVAL -90 DAY) 
                AND date(now())";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function selectEmailDiklat()
    {
        $sql = "SELECT email FROM tbl_diklat
                WHERE deleted_date IS NULL AND status_pengajuan='Lolos' 
                AND tanggal between DATE_ADD(date(now()), INTERVAL -90 DAY) AND date(now()) 
                GROUP BY email";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function selectEmailJenjang()
    {
        $sql = "SELECT email FROM tbl_kenaikan_jenjang
                WHERE deleted_date IS NULL AND status_pengajuan='Lolos'
                AND tanggal between DATE_ADD(date(now()), INTERVAL -90 DAY) AND date(now()) 
                GROUP BY email";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function getHistory($where = [])
    {
        $this->db->select('tbl_history.*');
        $this->db->from('tbl_history');
        if (!empty($where)) {
            foreach ($where as $key => $value) {
                $this->db->where('tbl_history.' . $key, $value);
            }
        }
        $this->db->where('tbl_history.deleted_date IS NULL');
        $this->db->order_by('tbl_history.created_date', 'DESC');
        $data = $this->db->get();

        return $data->result();
    }
}
