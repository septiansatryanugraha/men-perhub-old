<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kategori_status extends CI_Model
{
    const __tableName = 'kategori_status';
    const __tableId = 'id_kategori';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function select($where = [], $whereIn = [])
    {
        $this->db->select(self::__tableName . '.*');
        $this->db->from(self::__tableName);
        $this->db->where(self::__tableName . '.deleted_date IS NULL');
        if (!empty($where)) {
            foreach ($where as $key => $value) {
                $this->db->where(self::__tableName . '.' . $key, $value);
            }
        }
        if (!empty($whereIn)) {
            $this->db->where_in(self::__tableName . '.nama_kategori', array_values($whereIn));
        }
        $data = $this->db->get();

        return $data->result();
    }
}
