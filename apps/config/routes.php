<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

$route['default_controller'] = "Publik/Web";
$route['homepage'] = "Publik/Web";
$route['kontak-saran'] = "Publik/Web/kontak_saran";

$route['404_override'] = 'Default/Not_found';
$route['logout'] = 'Auth/logout';
$route['log-opd'] = 'Opd/Login_opd';
$route['login'] = 'Default/Auth';
$route['process-login'] = 'Default/Auth/login';
$route['logout-opd'] = 'Publik/Web/logout';


// FRONT
$route['beranda'] = 'Opd/Dashboard';

$route['profil-opd'] = 'Opd/Profil_opd/profil';
$route['update-profil-opd'] = 'Opd/Profil_opd/prosesUpdate';

$route['opd-history'] = 'Opd/Dashboard/history';
$route['opd-pembayaran'] = 'Opd/Dashboard/notifPembayaran';

$route['daftar-akun'] = 'Publik/Web/Register';
$route['ajax-list-kota'] = 'Publik/Web/getListKota';
$route['save-akun'] = 'Publik/Web/prosesDaftar';

$route['data-perpanjangan'] = 'Opd/Perpanjangan/home';
$route['ajax-perpanjangan'] = 'Opd/Perpanjangan/ajaxList';
$route['add-perpanjangan'] = 'Opd/Perpanjangan/Add';
$route['save-perpanjangan'] = 'Opd/Perpanjangan/prosesAdd';
$route['detail-history-perpanjangan'] = 'Opd/Perpanjangan/detailHistory';

$route['data-diklat'] = 'Opd/Diklat/home';
$route['ajax-diklat'] = 'Opd/Diklat/ajaxList';
$route['add-diklat'] = 'Opd/Diklat/Add';
$route['save-diklat'] = 'Opd/Diklat/prosesAdd';
$route['detail-history-diklat'] = 'Opd/Diklat/detailHistory';

$route['data-jenjang'] = 'Opd/Peningkatan_jenjang/home';
$route['ajax-jenjang'] = 'Opd/Peningkatan_jenjang/ajaxList';
$route['add-jenjang'] = 'Opd/Peningkatan_jenjang/Add';
$route['save-jenjang'] = 'Opd/Peningkatan_jenjang/prosesAdd';
$route['detail-history-jenjang'] = 'Opd/Peningkatan_jenjang/detailHistory';

$route['data-history'] = 'Opd/Perpanjangan/history';
$route['ajax-history'] = 'Opd/Perpanjangan/ajaxHistory';

$route['data-pembayaran'] = 'Opd/Pembayaran/home';
$route['ajax-pembayaran'] = 'Opd/Pembayaran/ajaxList';
$route['upload-pembayaran/(:any)'] = 'Opd/Pembayaran/uploadPembayaran/$1';
$route['uploading-pembayaran/(:any)'] = 'Opd/Pembayaran/prosesUpload/$1';


// ADMIN PANEL
/*   route modul profile */
$route['profile'] = 'Setting/Profile/index';
$route['update-profile'] = 'Setting/Profile/update';
$route['update-password'] = 'Setting/Profile/ubahPassword';

/*   route notification  */
$route['notif-perpanjangan'] = 'Dashboard/notifPerpanjangan';
$route['notif-diklat'] = 'Dashboard/notifDiklat';
$route['notif-jenjang'] = 'Dashboard/notifJenjang';
$route['notif-inbox'] = 'Dashboard/notifInbox';

/*   route modul master user  */
$route['master-user'] = 'Master/Master_user/index';
$route['ajax-master-user'] = 'Master/Master_user/ajaxList';
$route['edit-master-user/(:any)'] = 'Master/Master_user/Edit/$1';
$route['update-master-user/(:any)'] = 'Master/Master_user/prosesUpdate/$1';
$route['edit-catatan-user/(:any)'] = 'Master/Master_user/EditCatatan/$1';
$route['update-catatan-user/(:any)'] = 'Master/Master_user/prosesUpdateCatatan/$1';
$route['hapus-master-user'] = 'Master/Master_user/prosesDelete';
$route['histori-user/(:any)'] = 'Master/Master_user/history/$1';

/*   route modul slider   */
$route['slider'] = 'Master/Slider/index';
$route['ajax-slider'] = 'Master/Slider/ajaxList';
$route['add-slider'] = 'Master/Slider/add';
$route['save-slider'] = 'Master/Slider/prosesAdd';
$route['edit-slider/(:any)'] = 'Master/Slider/Edit/$1';
$route['update-slider/(:any)'] = 'Master/Slider/prosesUpdate/$1';
$route['hapus-slider'] = 'Master/Slider/prosesDelete';

/*   route modul informasi  */
$route['informasi'] = 'Master/Informasi/index';
$route['ajax-informasi'] = 'Master/Informasi/ajaxList';
$route['add-informasi'] = 'Master/Informasi/Add';
$route['save-informasi'] = 'Master/Informasi/prosesAdd';
$route['edit-informasi/(:any)'] = 'Master/Informasi/Edit/$1';
$route['update-informasi/(:any)'] = 'Master/Informasi/prosesUpdate/$1';
$route['hapus-informasi'] = 'Master/Informasi/prosesDelete';

/*   route modul surat panggilan  */
$route['master-surat'] = 'Master/Surat/index';
$route['ajax-surat'] = 'Master/Surat/ajaxList';
$route['add-surat'] = 'Master/Surat/Add';
$route['save-surat'] = 'Master/Surat/prosesAdd';
$route['edit-surat/(:any)'] = 'Master/Surat/Edit/$1';
$route['update-surat/(:any)'] = 'Master/Surat/prosesUpdate/$1';
$route['export-surat/(:any)'] = 'Master/Surat/exportSurat/$1';
$route['hapus-surat'] = 'Master/Surat/prosesDelete';

/*   route modul surat perintah bayar  */
$route['master-surat-bayar'] = 'Master/Surat_perintah_bayar/index';
$route['ajax-surat-bayar'] = 'Master/Surat_perintah_bayar/ajaxList';
$route['add-surat-bayar'] = 'Master/Surat_perintah_bayar/Add';
$route['save-surat-bayar'] = 'Master/Surat_perintah_bayar/prosesAdd';
$route['edit-surat-bayar/(:any)'] = 'Master/Surat_perintah_bayar/Edit/$1';
$route['update-surat-bayar/(:any)'] = 'Master/Surat_perintah_bayar/prosesUpdate/$1';
$route['export-surat-bayar/(:any)'] = 'Master/Surat_perintah_bayar/exportSuratBayar/$1';
$route['hapus-surat-bayar'] = 'Master/Surat_perintah_bayar/prosesDelete';

/*   route modul pengumuman  */
$route['pengumuman'] = 'Master/Pengumuman/index';
$route['ajax-pengumuman'] = 'Master/Pengumuman/ajaxList';
$route['add-pengumuman'] = 'Master/Pengumuman/Add';
$route['save-pengumuman'] = 'Master/Pengumuman/prosesAdd';
$route['edit-pengumuman/(:any)'] = 'Master/Pengumuman/Edit/$1';
$route['update-pengumuman/(:any)'] = 'Master/Pengumuman/prosesUpdate/$1';
$route['hapus-pengumuman'] = 'Master/Pengumuman/prosesDelete';

/*   route modul master perpanjangan  */
$route['download-berkas-file'] = 'Master/Master_perpanjangan/download';

$route['master-perpanjangan'] = 'Master/Master_perpanjangan/index';
$route['ajax-master-perpanjangan'] = 'Master/Master_perpanjangan/ajaxList';
$route['edit-perpanjangan/(:any)'] = 'Master/Master_perpanjangan/Edit/$1';
$route['update-perpanjangan/(:any)'] = 'Master/Master_perpanjangan/prosesUpdate/$1';

$route['master-perpanjangan-validasi'] = 'Master/Master_perpanjangan/homeValidasi';
$route['ajax-perpanjangan-validasi'] = 'Master/Master_perpanjangan/ajaxValidasi';
$route['edit-perpanjangan-validasi/(:any)'] = 'Master/Master_perpanjangan/editValidasi/$1';
$route['update-perpanjangan-validasi/(:any)'] = 'Master/Master_perpanjangan/prosesUpdateValidasi/$1';

$route['master-perpanjangan-approve'] = 'Master/Master_perpanjangan/homeApprove';
$route['ajax-perpanjangan-approve'] = 'Master/Master_perpanjangan/ajaxApprove';
$route['edit-perpanjangan-approve/(:any)'] = 'Master/Master_perpanjangan/editApprove/$1';
$route['update-perpanjangan-approve/(:any)'] = 'Master/Master_perpanjangan/prosesUpdateApprove/$1';

$route['edit-file-pnbp/(:any)'] = 'Master/Master_perpanjangan/editPnbp/$1';
$route['update-file-pnbp/(:any)'] = 'Master/Master_perpanjangan/prosesUpdatePnbp/$1';

$route['upload-pnbp/(:any)'] = 'Master/Master_perpanjangan/uploadPnbp/$1';
$route['uploading-pnbp/(:any)'] = 'Master/Master_perpanjangan/prosesUpload/$1';

$route['eksport-surat-pnbp-perpanjangan/(:any)'] = 'Master/Master_perpanjangan/suratPnbpPdf/$1';
$route['cetak-perpanjangan/(:any)'] = 'Master/Master_perpanjangan/cetak/$1';
$route['surat-pnbp-perpanjangan/(:any)'] = 'Master/Master_perpanjangan/suratPnbp/$1';

$route['hapus-perpanjangan'] = 'Master/Master_perpanjangan/prosesDelete';

/*   route modul master diklat  */
$route['download-berkas-file-diklat'] = 'Master/Master_diklat/download';

$route['master-diklat'] = 'Master/Master_diklat/index';
$route['ajax-master-diklat'] = 'Master/Master_diklat/ajaxList';
$route['edit-diklat/(:any)'] = 'Master/Master_diklat/Edit/$1';
$route['update-diklat/(:any)'] = 'Master/Master_diklat/prosesUpdate/$1';

$route['master-diklat-validasi'] = 'Master/Master_diklat/homeValidasi';
$route['ajax-diklat-validasi'] = 'Master/Master_diklat/ajaxValidasi';
$route['edit-diklat-validasi/(:any)'] = 'Master/Master_diklat/editValidasi/$1';
$route['update-diklat-validasi/(:any)'] = 'Master/Master_diklat/prosesUpdateValidasi/$1';

$route['master-diklat-approve'] = 'Master/Master_diklat/homeApprove';
$route['ajax-diklat-approve'] = 'Master/Master_diklat/ajaxApprove';
$route['edit-diklat-approve/(:any)'] = 'Master/Master_diklat/editApprove/$1';
$route['update-diklat-approve/(:any)'] = 'Master/Master_diklat/prosesUpdateApprove/$1';

$route['edit-nilai-diklat/(:any)'] = 'Master/Master_diklat/editNilai/$1';
$route['update-nilai-diklat/(:any)'] = 'Master/Master_diklat/prosesUpdateNilai/$1';

$route['view-nilai-diklat/(:any)'] = 'Master/Master_diklat/lihatNilai/$1';
$route['cetak-diklat/(:any)'] = 'Master/Master_diklat/cetak/$1';

$route['hapus-diklat'] = 'Master/Master_diklat/prosesDelete';

/*   route modul master jenjang  */
$route['download-berkas-file-jenjang'] = 'Master/Master_jenjang/download';

$route['master-jenjang'] = 'Master/Master_jenjang/index';
$route['ajax-master-jenjang'] = 'Master/Master_jenjang/ajaxList';
$route['edit-jenjang/(:any)'] = 'Master/Master_jenjang/Edit/$1';
$route['update-jenjang/(:any)'] = 'Master/Master_jenjang/prosesUpdate/$1';

$route['master-jenjang-validasi'] = 'Master/Master_jenjang/homeValidasi';
$route['ajax-jenjang-validasi'] = 'Master/Master_jenjang/ajaxValidasi';
$route['edit-jenjang-validasi/(:any)'] = 'Master/Master_jenjang/editValidasi/$1';
$route['update-jenjang-validasi/(:any)'] = 'Master/Master_jenjang/prosesUpdateValidasi/$1';

$route['master-jenjang-approve'] = 'Master/Master_jenjang/homeApprove';
$route['ajax-jenjang-approve'] = 'Master/Master_jenjang/ajaxApprove';
$route['edit-jenjang-approve/(:any)'] = 'Master/Master_jenjang/editApprove/$1';
$route['update-jenjang-approve/(:any)'] = 'Master/Master_jenjang/prosesUpdateApprove/$1';

$route['edit-nilai-jenjang/(:any)'] = 'Master/Master_jenjang/editNilai/$1';
$route['update-nilai-jenjang/(:any)'] = 'Master/Master_jenjang/prosesUpdateNilai/$1';
$route['view-nilai-jenjang/(:any)'] = 'Master/Master_jenjang/lihatNilai/$1';
$route['cetak-sertifikat-kenaikan/(:any)'] = 'Master/Master_jenjang/Cetak/$1';

$route['hapus-jenjang'] = 'Master/Master_jenjang/prosesDelete';

/*   route modul master pembayaran  */
$route['master-pembayaran'] = 'Master/Master_pembayaran/index';
$route['ajax-master-pembayaran'] = 'Master/Master_pembayaran/ajaxList';
$route['edit-pembayaran/(:any)'] = 'Master/Master_pembayaran/Edit/$1';
$route['update-pembayaran/(:any)'] = 'Master/Master_pembayaran/prosesUpload/$1';
$route['hapus-pembayaran'] = 'Master/Master_pembayaran/prosesDelete';

/*   route modul inbox   */
$route['inbox'] = 'Master/Inbox/index';
$route['ajax-inbox'] = 'Master/Inbox/ajaxList';
$route['view-inbox/(:any)'] = 'Master/Inbox/edit/$1';
$route['hapus-inbox'] = 'Master/Inbox/prosesDelete';

/*   route modul master history   */
$route['list-history'] = 'Master/Master_history/index';

/*   route modul report user   */
$route['report-user'] = 'Support/Report_user/index';
$route['ajax-report-user'] = 'Support/Report_user/ajaxReport';
$route['export-user'] = 'Support/Report_user/exportExcel';

/*   route modul report perpanjangan   */
$route['report-perpanjangan'] = 'Support/Report_perpanjangan/index';
$route['ajax-report-perpanjangan'] = 'Support/Report_perpanjangan/ajaxReport';
$route['export-perpanjangan'] = 'Support/Report_perpanjangan/exportExcel';

/*   route modul report diklat   */
$route['report-diklat'] = 'Support/Report_diklat/index';
$route['ajax-report-diklat'] = 'Support/Report_diklat/ajaxReport';
$route['export-diklat'] = 'Support/Report_diklat/exportExcel';

/*    route modul report jenjang  */
$route['report-jenjang'] = 'Support/Report_jenjang/index';
$route['ajax-report-jenjang'] = 'Support/Report_jenjang/ajaxReport';
$route['export-jenjang'] = 'Support/Report_jenjang/exportExcel';

/*   route modul konfigurasi menu  */
$route['konfigurasi-menu'] = 'Setting/Config_menu/index';
$route['ajax-konfigurasi-menu'] = 'Setting/Config_menu/ajaxList';
$route['edit-konfigurasi-menu/(:any)'] = 'Setting/Config_menu/edit/$1';
$route['update-konfigurasi-menu/(:any)'] = 'Setting/Config_menu/prosesUpdate/$1';
$route['hapus-konfigurasi-menu'] = 'Setting/Config_menu/prosesDelete';

/*   route modul konfigurasi kontak */
$route['konfigurasi-kontak'] = 'Setting/Contact/index';
$route['ajax-konfigurasi-kontak'] = 'Setting/Contact/ajaxList';
$route['edit-konfigurasi-kontak/(:any)'] = 'Setting/Contact/edit/$1';
$route['update-konfigurasi-kontak/(:any)'] = 'Setting/Contact/prosesUpdate/$1';
$route['hapus-konfigurasi-kontak'] = 'Setting/Contact/prosesDelete';

/*   route modul menu  */
$route['menu'] = 'Setting/Menu_evo/index';
$route['ajax-menu'] = 'Setting/Menu_evo/ajaxMenu';
$route['save-menu'] = 'Setting/Menu_evo/prosesTambah';
$route['save-sort-menu'] = 'Setting/Menu_evo/saveSort';
$route['edit-menu/(:any)'] = 'Setting/Menu_evo/edit/$1';
$route['update-menu/(:any)'] = 'Setting/Menu_evo/prosesUpdate/$1';
$route['hapus-menu'] = 'Setting/Menu_evo/deleteMenu';

/*   route modul user  */
$route['user'] = 'Setting/User/index';
$route['ajax-user'] = 'Setting/User/ajaxList';
$route['add-user'] = 'Setting/User/Add';
$route['save-user'] = 'Setting/User/prosesTambah';
$route['edit-user/(:any)'] = 'Setting/User/Edit/$1';
$route['update-user/(:any)'] = 'Setting/User/prosesUpdate/$1';
$route['hapus-user'] = 'Setting/User/prosesDelete';

/*   route modul grup  */
$route['user-grup'] = 'Setting/Grup/index';
$route['add-grup'] = 'Setting/Grup/Add';
$route['save-grup'] = 'Setting/Grup/prosesTambah';
$route['edit-grup/(:any)'] = 'Setting/Grup/Edit/$1';
$route['update-grup/(:any)'] = 'Setting/Grup/prosesUpdate/$1';
$route['hapus-grup'] = 'Setting/Grup/prosesDelete';
$route['edit-hak-akses/(:any)'] = 'Setting/Grup/hakAkses/$1';
$route['update-hak-akses/(:any)'] = 'Setting/Grup/prosesUpdateHakAkses/$1';
/* End of file routes.php */
/* Location: ./application/config/routes.php */