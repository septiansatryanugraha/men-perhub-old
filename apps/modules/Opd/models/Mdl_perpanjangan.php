<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_perpanjangan extends CI_Model
{
    const __tableName = 'tbl_perpanjangan';
    const __tableId = 'id_perpanjangan';

    function __construct()
    {
        parent::__construct();
    }

    function getWhere($where = [])
    {
        $this->db->select(self::__tableName . '.*');
        $this->db->from(self::__tableName);
        if (!empty($where)) {
            foreach ($where as $key => $value) {
                $this->db->where(self::__tableName . '.' . $key, $value);
            }
        }
        $this->db->where(self::__tableName . '.deleted_date IS NULL');
        $this->db->order_by(self::__tableName . '.created_date', 'DESC');
        $data = $this->db->get();

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }
}
