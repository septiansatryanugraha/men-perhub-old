<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_products extends CI_Model
{
    const __tableName = 'tbl_perpanjangan';
    const __tableName2 = 'tbl_diklat';
    const __tableName3 = 'tbl_kenaikan_jenjang';

    function __construct()
    {
        parent::__construct();
    }

    public function selectHistoryDiklat($id)
    {
        $sql = "SELECT * FROM tbl_history_diklat WHERE deleted_date IS NULL AND id_user = '{$id}' ";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectHistoryJenjang($id)
    {
        $sql = "SELECT * FROM tbl_history_jenjang WHERE deleted_date IS NULL AND id_user = '{$id}' ";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectHistoriNaskah($id)
    {
        $sql = "SELECT * FROM tbl_history_dokumen WHERE deleted_date IS NULL AND kode = '{$id}' order by id_history desc";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selekKonten()
    {
        $sql = "SELECT * FROM tbl_bantuan WHERE deleted_date IS NULL";
        $query = $this->db->query($sql);

        return $query->result();
    }

    function totalData($column, $value)
    {
        $this->db->where($column, $value);
        $this->db->where(self::__tableName . '.deleted_date IS NULL');
        $query = $this->db->get(self::__tableName);
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    function totalData2($column, $value)
    {
        $this->db->where($column, $value);
        $this->db->where(self::__tableName2 . '.deleted_date IS NULL');
        $query = $this->db->get(self::__tableName2);
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    function totalData3($column, $value)
    {
        $this->db->where($column, $value);
        $this->db->where(self::__tableName3 . '.deleted_date IS NULL');
        $query = $this->db->get(self::__tableName3);
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    function updateStatusPembayaran($id)
    {
        $update_query = "UPDATE tbl_bukti_pembayaran SET status_baca=1 WHERE status_baca=0 AND id_user = '{$id}'";
        $this->db->query($update_query);

        return $this->db->affected_rows();
    }

    function selectNotifPembayaran($id)
    {
        $selek_query = "SELECT * FROM tbl_bukti_pembayaran WHERE deleted_date IS NULL AND id_user = '{$id}' ORDER BY id_pembayaran DESC LIMIT 5";
        $data = $this->db->query($selek_query);

        return $data->result();
    }

    public function totalCountPembayaran($id)
    {
        $data = $this->db
            ->where('deleted_date IS NULL')
            ->where('id_user=', $id)
            ->where('status_baca=', 0)
            ->get('tbl_bukti_pembayaran');

        return $data->num_rows();
    }

    function updateStatusHistory($id)
    {
        $update_query = "UPDATE tbl_history SET status_baca=1 WHERE status_baca=0 AND id_user = '{$id}'";
        $this->db->query($update_query);

        return $this->db->affected_rows();
    }

    function selectNotifHistory($id)
    {
        $selek_query = "SELECT * FROM tbl_history WHERE deleted_date IS NULL AND id_user = '{$id}' ORDER BY id_history DESC LIMIT 5";
        $data = $this->db->query($selek_query);

        return $data->result();
    }

    public function totalCountHistory($id)
    {
        $data = $this->db
            ->where('deleted_date IS NULL')
            ->where('id_user=', $id)
            ->where('status_baca=', 0)
            ->get('tbl_history');

        return $data->num_rows();
    }
}
