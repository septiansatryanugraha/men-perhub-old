</head>
<style type="text/css">
    .jarak-logo-utama {
        margin-top: -7px;
    }
    .jarak-logo {
        margin-top: -30px;
        margin-left: 0px;
        font-size: 10px;
    }
    .navbar-nav>.notifications-menu>.dropdown-menu>li .menu2, .navbar-nav>.messages-menu>.dropdown-menu>li .menu2, .navbar-nav>.tasks-menu>.dropdown-menu>li .menu2 {
        max-height: 200px;
        margin: 0;
        padding-left: 12px;
        padding-right: 12px;
        list-style: none;
        overflow-x: hidden;
        border-bottom: 1px solid grey;
    }
    .navbar-nav>.notifications-menu>.dropdown-menu>li .menu1, .navbar-nav>.messages-menu>.dropdown-menu>li .menu1, .navbar-nav>.tasks-menu>.dropdown-menu>li .menu1 {
        max-height: 200px;
        margin: 0;
        padding-left: 12px;
        padding-right: 12px;
        list-style: none;
        overflow-x: hidden;
        border-bottom: 1px solid grey;
        /* START TOOLTIP STYLES */
        [tooltip] {
            position: relative; /* opinion 1 */
        }
        [tooltip]::after {
            content: attr(tooltip); /* magic! */
            /* most of the rest of this is opinion */
            font-family: Helvetica, sans-serif;
            text-align: center;
            /* 
              Let the content set the size of the tooltips 
              but this will also keep them from being obnoxious
            */
            min-width: 3em;
            max-width: 21em;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            padding: 1ch 1.5ch;
            border-radius: .3ch;
            box-shadow: 0 1em 2em -.5em rgba(0, 0, 0, 0.35);
            background: #333;
            color: #fff;
            z-index: 1000; /* absurdity 2 */
        }
        /* Make the tooltips respond to hover */
        [tooltip]:hover::before,
            [tooltip]:hover::after {
            display: block;
        }
        /* don't show empty tooltips */
        [tooltip='']::before,
        [tooltip='']::after {
            display: none !important;
        }
        /* FLOW: UP */
        [tooltip]:not([flow])::before,
            [tooltip][flow^="up"]::before {
            bottom: 150%;
            border-bottom-width: 0;
            border-top-color: #333;
        }
        [tooltip]:not([flow])::after,
            [tooltip][flow^="up"]::after {
            bottom: calc(150% + 5px);
        }
        [tooltip]:not([flow])::before,
            [tooltip]:not([flow])::after,
            [tooltip][flow^="up"]::before,
            [tooltip][flow^="up"]::after {
            left: 50%;
            transform: translate(-50%, -.5em);
        }
        /* KEYFRAMES */
        @keyframes tooltips-vert {
            to {
                opacity: .9;
                transform: translate(-50%, 0);
            }
        }
        @keyframes tooltips-horz {
            to {
                opacity: .9;
                transform: translate(0, -50%);
            }
        }
        /* FX All The Things */ 
        [tooltip]:not([flow]):hover::before,
            [tooltip]:not([flow]):hover::after,
            [tooltip][flow^="up"]:hover::before,
            [tooltip][flow^="up"]:hover::after,
            [tooltip][flow^="down"]:hover::before,
            [tooltip][flow^="down"]:hover::after {
            animation: tooltips-vert 300ms ease-out forwards;
        }
        [tooltip][flow^="left"]:hover::before,
            [tooltip][flow^="left"]:hover::after,
            [tooltip][flow^="right"]:hover::before,
            [tooltip][flow^="right"]:hover::after {
            animation: tooltips-horz 300ms ease-out forwards;
        }
    }
</style>
<body class="skin-blue">
    <!-- Site wrapper -->
    <div class="wrapper">
        <header class="main-header">
            <a href="#" class="logo"><b><div class=" brand-text jarak-logo-utama">E-KOMPETENSI</b></div><div class=" brand-text jarak-logo">APLIKASI SERTIFIKASI PENGUJI</div></a>       
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown notifications-menu" data-toggle="tooltip" data-placement="bottom" title="Notifikasi Pembayaran">
                            <a href="#" class="dropdown-toggle hokya" data-toggle="dropdown">
                                <i class="fa fa-money"></i>
                                <span class="label label-warning count"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header"><i class="fa fa-inbox text-aqua"></i> Notifikasi Terbaru Pembayaran</li>
                                <li><ul class="menu1"></ul></li>
                            </ul>
                        </li>
                        <li class="dropdown notifications-menu" data-toggle="tooltip" data-placement="bottom" title="Notifikasi History Pengajauan">
                            <a href="#" class="dropdown-toggle hokya2" data-toggle="dropdown">
                                <i class="fa fa-history"></i>
                                <span class="label label-warning count2"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header"><i class="fa fa-inbox text-aqua"></i> Notifikasi History Pengajuan</li>
                                <li><ul class="menu2"></ul></li>
                            </ul>
                        </li>
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?php if ($user->photo == '') { ?>
                                    <img src="<?= base_url(); ?>assets/tambahan/gambar/no-image.jpg" class="user-image" />
                                <?php } else { ?>
                                    <img src="<?= base_url(); ?><?= $user->photo; ?>" class="user-image" alt="User Image" />
                                <?php } ?>
                                <span class="hidden-xs"><?= $user->nama_lengkap; ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <?php if ($user->photo == '') { ?>
                                        <img src="<?= base_url(); ?>assets/tambahan/gambar/no-image.jpg" class="img-circle" />
                                    <?php } else { ?>
                                        <img src="<?= base_url(); ?><?= $user->photo; ?>" class="img-circle" alt="User Image" />
                                    <?php } ?>
                                    <p><?= $user->nama_lengkap; ?><small><?= $user->dinas; ?></small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?= site_url('profil-opd') ?>" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?= site_url('logout-opd') ?>" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- =============================================== -->
        <script>
            $(document).ready(function () {
                function load_unseen_notification2(view = '') {
                    $.ajax({
                        url: "<?= base_url('opd-history'); ?>",
                        method: "POST",
                        data: {view: view},
                        dataType: "json",
                        success: function (data)
                        {
                            $('.menu2').html(data.notification);
                            if (data.unseen_notification > 0)
                            {
                                $('.count2').html(data.unseen_notification);
                            }
                        }
                    });
                }

                load_unseen_notification2();

                $(document).on('click', '.hokya2', function () {
                    $('.count2').html('');
                    load_unseen_notification2('yes');
                });

                setInterval(function () {
                    load_unseen_notification2();
                }, 5000);

            });

            $(document).ready(function () {
                function load_unseen_notification(view = '') {
                    $.ajax({
                        url: "<?= base_url('opd-pembayaran'); ?>",
                        method: "POST",
                        data: {view: view},
                        dataType: "json",
                        success: function (data)
                        {
                            $('.menu1').html(data.notification);
                            if (data.unseen_notification > 0)
                            {
                                $('.count').html(data.unseen_notification);
                            }
                        }
                    });
                }

                load_unseen_notification();

                $(document).on('click', '.hokya', function () {
                    $('.count').html('');
                    load_unseen_notification('yes');
                });

                setInterval(function () {
                    load_unseen_notification();
                }, 5000);

            });
        </script>