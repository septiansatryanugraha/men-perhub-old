<!-- Left side column. contains the sidebar -->
<style type="text/css">
    .welcome {
        margin-left: 15px;
        color:white;
        margin-bottom: 2px;
    }
</style>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <p class="welcome">Selamat Datang di Aplikasi</p>
            <div class="info">
                <p><?= $user->nama_lengkap; ?></p>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">List Menu</li>
            <li class="nav-item dropdown <?= $this->uri->segment(1) == 'beranda' ? 'active' : '' ?>">
                <a href="<?= site_url('beranda'); ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-item dropdown <?= $this->uri->segment(1) == 'data-perpanjangan' ? 'active' : '' ?>">
                <a href="<?= site_url('data-perpanjangan'); ?>">
                    <i class="fa fa-file-text"></i>
                    <span>Perpanjangan Sertifikasi</span>
                </a>
            </li>
            <?php if ($dataSelek1 == 'Open') { ?>
                <li class="nav-item dropdown <?= $this->uri->segment(1) == 'data-diklat' ? 'active' : '' ?>">
                    <a href="<?= site_url('data-diklat'); ?>">
                        <i class="fa fa-file-text"></i>
                        <span>Diklat PKB</span>
                        <span class="pull-right-container">
                            <small class="label pull-right bg-red">dibuka</small>
                        </span>
                    </a>
                </li>
            <?php } ?>
            <?php if ($dataSelek2 == 'Open') { ?>
                <li class="nav-item dropdown <?= $this->uri->segment(1) == 'data-jenjang' ? 'active' : '' ?>">
                    <a href="<?= site_url('data-jenjang'); ?>">
                        <i class="fa fa-file-text"></i>
                        <span>Peningkatan Jenjang</span>
                        <span class="pull-right-container">
                            <small class="label pull-right bg-red">dibuka</small>
                        </span>
                    </a>
                </li>
            <?php } ?>
            <li class="nav-item dropdown <?= $this->uri->segment(1) == 'data-history' ? 'active' : '' ?>">
                <a href="<?= site_url('data-history'); ?>">
                    <i class="fa fa-history"></i>
                    <span>Histori Pengajuan</span>
                </a>
            </li>
            <li class="nav-item dropdown <?= $this->uri->segment(1) == 'data-pembayaran' ? 'active' : '' ?>">
                <a href="<?= site_url('data-pembayaran'); ?>">
                    <i class="fa fa-money"></i>
                    <span>History Pembayaran</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">