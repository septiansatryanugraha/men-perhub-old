<div class="content">
    <div class="box">
        <div class="box-header" style="margin-top: 10px; margin-left: 15px;">
            <div class="col-md-2" style="padding: 0;">
                <a href='<?= base_url("add-jenjang"); ?>'><button class="btn btn-success" ><i class="glyphicon glyphicon-plus-sign"></i> Tambah Pengajuan</button></a></a>
            </div>
        </div>		
        <div class="box-body" style="margin: 15px 15px;">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Kode Pengajuan</th>
                        <th>Email</th>
                        <th>Keterangan Status</th>
                        <th width="100px">Action</th>
                    </tr>	
                </thead>
                <tbody></tbody>
            </table>
        </div>	
    </div>
    <div id="modal"></div>
</div>	

<script type="text/javascript">
    //untuk load data table ajax	
    var save_method; //for save method string
    var table;

    $(document).ready(function () {
        //datatables
        table = $('#table').DataTable({
            "processing": true, //Feature control the processing indicator.
            // "scrollX": true,
            "order": [], //Initial no order.
            oLanguage: {
                "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='26px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Data tidak ada di server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?= site_url('ajax-jenjang') ?>",
                "type": "POST"
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
        });
    });

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    $(document).on("click", ".detail-history", function () {
        var kode_pengajuan = $(this).attr("data-id");

        $.ajax({
            method: "POST",
            url: "<?= base_url('detail-history-jenjang'); ?>",
            data: "kode_pengajuan=" + kode_pengajuan
        }).done(function (data) {
            $('#modal').html(data);
            $('#detail-history').modal('show');
        })
    })
</script>