<style>

    #btn_loading {
        display: none;
    }

    .space {
        margin-top: 8px;
    }


</style>

<section class="content">

    <form class="form-horizontal" id="form-upload" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id_pembayaran" value="<?= $brand->id_pembayaran; ?>">
        <input type="hidden" name="kode_pengajuan" value="<?= $brand->kode_pengajuan; ?>">
        <input type="hidden" name="id_user" value="<?= $sessid; ?>">
        <input type="hidden" name="email" value="<?= $email; ?>">
        <input type="hidden" name="nama" value="<?= $nama; ?>">


        <div class="row">

            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="nav-tabs-custom">

                        <div class="box-header with-border">
                            <h3 class="box-title">Upload Pembayaran PNBP</h3>
                        </div>

                        <div class="box-body">

                            <div class="form-group row">      
                                <label for="inputFoto" class="col-sm-2 control-label">File Pembayaran</label>
                                <div class="col-sm-6 space">
                                    <input type="file" name="file_upload_pembayaran" id="file_upload_pembayaran" onchange="return valid1()"/>
                                    <br>
                                    <?php if ($brand->file_upload_pembayaran != '') { ?>
                                        <a href="<?= base_url() . $brand->file_pnbp; ?>"><b><font face="verdana" size="2" color="red"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?= $brand->nama_file_upload; ?></font></b></a>
                                    <?php } else { ?>
                                    <?php } ?>
                                </div>

                                <div class="col-sm-2 space">
                                    <small class="label pull-center bg-red">format .pdf | max 300 kb </small>
                                </div>
                            </div>



                            <div class="box-footer">
                                <div id="buka">
                                    <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-upload"></i> Upload</button>
                                </div>

                                <div id="btn_loading">
                                    <button type="submit" class="btn btn-success btn-flat" disabled><i class='fa fa-refresh fa-spin'></i> Tunggu...</button>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

            </div>
    </form>
</section>  

<script type="text/javascript">
    function valid1()
    {
        var fileInput = document.getElementById("file_upload_pembayaran").value;
        if (fileInput != '')
        {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                // swal("Peringatan", "File harus format .pdf", "warning");
                toastr.error('File harus format .pdf', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                document.getElementById("file_upload_pembayaran").value = '';
                return false;
            }
            var ukuran = document.getElementById("file_upload_pembayaran");
            if (ukuran.files[0].size > 307200)  // validasi ukuran size file
            {
                // swal("Peringatan", "File harus maksimal 5MB", "warning");
                toastr.error('File harus maksimal 300 kb', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                ukuran.value = '';
                return false;
            }
            return true;
        }
    }
</script>



<script type="text/javascript">

    $('#form-upload').submit(function (e) {
        var error = 0;
        var message = "";

        var data = $(this).serialize();


        var file_upload_pembayaran = $("#file_upload_pembayaran").val();
        var file_upload_pembayaran = file_upload_pembayaran.trim();

        if (error == 0) {
            if (file_upload_pembayaran.length == 0) {
                error++;
                message = "File Pembayaran wajib di isi.";
            }
        }

        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?= base_url('Opd/Pembayaran/prosesUpload'); ?>',
                type: 'POST',
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    setTimeout(location.reload.bind(location), 1000);
                    swal("Success", result.pesan, "success");
                } else if (result.status == 'kosong') {
                    $("#spinner").hide();
                    toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
            e.preventDefault();
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });

</script>