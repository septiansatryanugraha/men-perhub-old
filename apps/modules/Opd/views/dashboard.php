<style>
    #graph {
        height: 330px;
        width: 100%;
        margin: 0 auto
    } 
</style>
<section class="content">
    <div class="row">
        <div class="col-lg-4 col-xs-4">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><div class="counter" data-target="<?= $total_perpanjangan ?>">0</div></h3>
                    <p>Total Data Perpanjangan</p>
                </div>
                <div class="icon">
                    <i class="fa fa-file-text"></i>
                </div>
                <a href="#" class="ajaxify small-box-footer klik">Total Data Sekarang </a>
            </div>
        </div>
        <div class="col-lg-4 col-xs-4">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><div class="counter" data-target="<?= $total_jenjang ?>">0</div></h3>
                    <p>Total Data Peningkatan Jenjang</p>
                </div>
                <div class="icon">
                    <i class="fa fa-file-text"></i>
                </div>
                <a href="#" class="ajaxify small-box-footer klik">Total Data Sekarang </a>
            </div>
        </div>
        <div class="col-lg-4 col-xs-4">
            <div class="small-box bg-orange">
                <div class="inner">
                    <h3><div class="counter" data-target="<?= $total_diklat ?>">0</div></h3>
                    <p>Total Data Dilkat PKB</p>
                </div>
                <div class="icon">
                    <i class="fa fa-file-text"></i>
                </div>
                <a href="#" class="ajaxify small-box-footer klik">Total Data Sekarang </a>
            </div>
        </div>
    </div>
    <!-- pesan & new member-->
    <div class="row">
        <section class="col-lg-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-history" aria-hidden="true"></i>
                    <!-- pesan customer -->
                    <h3 class="box-title">History Kompetensi Diklat PKB</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="list-data" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Jenis Jenjang</th>
                                        <th>Nilai</th>
                                        <th>Status</th>
                                        <th>Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($history_diklat)) {
                                        $no = 1;
                                        foreach ($history_diklat as $data) {

                                            ?>
                                            <tr>
                                                <td><?= $no ?></td>
                                                <td><?= $data->jenis_diklat ?></td>
                                                <td><?= $data->kumulatif ?></td>
                                                <td><?= $data->status ?></td>
                                                <td><?= date_indo(date($data->created_date)) ?></td>
                                            </tr>
                                            <?php
                                            $no++;
                                        }

                                        ?>

                                    </tbody>
                                <?php } else { ?>
                                    <td colspan="5"><center><?= 'Belum ada data' ?></center></td>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- data new member-->
        <section class="col-lg-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-history"></i>
                    <h3 class="box-title">History Kompetensi Jenjang</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="list-data" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Jenis Jenjang</th>
                                        <th>Nilai</th>
                                        <th>Status</th>
                                        <th>Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($history_jenjang)) {
                                        $no = 1;
                                        foreach ($history_jenjang as $data) {

                                            ?>
                                            <tr>
                                                <td><?= $no ?></td>
                                                <td><?= $data->jenis_jenjang ?></td>
                                                <td><?= $data->kumulatif ?></td>
                                                <td><?= $data->status ?></td>
                                                <td><?= date_indo(date($data->created_date)) ?></td>
                                            </tr>
                                            <?php
                                            $no++;
                                        }

                                        ?>

                                    </tbody>
                                <?php } else { ?>
                                    <td colspan="5"><center><?= 'Belum ada data' ?></center></td>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
<script>
// Animasi angka bergerak dashboard
    const counters = document.querySelectorAll('.counter');
    const speed = 1000; // The lower the slower

    counters.forEach(counter => {
        const updateCount = () => {
            const target = +counter.getAttribute('data-target');
            const count = +counter;
            // Lower inc to slow and higher to slow
            const inc = target / speed;
            if (count < target) {
                // Add inc to count and output in counter
                counter.innerText = count + inc;
                // Call function every ms
                setTimeout(updateCount, 1);
            } else {
                counter.innerText = target;
            }
        };
        updateCount();
    });
</script>