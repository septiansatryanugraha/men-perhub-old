<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= $title . " - Kementerian Perhubungan"; ?></title>
        <link rel="icon" href="<?= base_url() ?>/assets/tambahan/gambar/logo-dishub.png">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/eksternal/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/eksternal/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/AdminLTE.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/iCheck/square/blue.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/toastr/toastr.css">
    </head>
    <style type="text/css">
        .login-page { background: url(<?= base_url(); ?>assets/tambahan/gambar/login-background.jpg) !important; no-repeat; }
    </style>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <img src="<?= base_url(); ?>assets/tambahan/gambar/logo-kementrian2.png" height="80px"> 
            </div>
            <div class="login-box-body">
                <p class="login-box-msg">
                    Silahkan Login 
                </p>
                <form action="<?= base_url('Opd/Login_opd'); ?>" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" id="username" placeholder="Username" name="_email" onblur="validate()" />
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" id="password" placeholder="Password" name="_password" onblur="validate()" />
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <label>
                        <input type="checkbox" class="form-ok"> <font style='color: black; font-size: 13px;'> <b>Show password</b></font><br>
                    </label>
                    <div class="row">
                        <div class="col-xs-offset-8 col-xs-4">
                            <button type="submit" id="btnSignUp" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.login-box -->
        <!-- jQuery 2.2.3 -->
        <script src="<?= base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?= base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= base_url(); ?>assets/toastr/toastr.js"></script>
        <script type="text/javascript" >
            function formValidation(oEvent) {
                oEvent = oEvent || window.event;
                var txtField = oEvent.target || oEvent.srcElement;

                var t1ck = true;
                var msg = " ";
                if (document.getElementById("username").value.length < 3)
                {
                    t1ck = false;
                    msg = msg + "Your name should be minimun 3 char length";
                }
                if (document.getElementById("password").value.length < 5)
                {
                    t1ck = false;
                    msg = msg + "Your name should be minimun 3 char length";
                }
                // alert(msg + t1ck);
                if (t1ck) {
                    document.getElementById("btnSignUp").disabled = false;
                } else {
                    document.getElementById("btnSignUp").disabled = true;
                }
            }

            function resetForm() {
                document.getElementById("btnSignUp").disabled = true;
                var frmMain = document.forms[0];
                frmMain.reset();
            }

            window.onload = function () {

                var btnSignUp = document.getElementById("btnSignUp");

                var username = document.getElementById("username");
                var password = document.getElementById("password");


                var t1ck = false;
                document.getElementById("btnSignUp").disabled = true;
                username.onkeyup = formValidation;
                password.onkeyup = formValidation;
            }
            $(document).ready(function () {
                $('.form-ok').click(function () {
                    if ($(this).is(':checked')) {
                        $('#password').attr('type', 'text');
                    } else {
                        $('#password').attr('type', 'password');
                    }
                });
            });
        </script>
        <?php
        $formErrorUsername = form_error('_email');
        $formErrorPassword = form_error('_password');
        if (!empty($formErrorUsername) || !empty($formErrorPassword)):

            ?>
        <?php endif; ?>

        <?php
        $pesan = $this->session->flashdata('error_msg');
        if (!empty($pesan)):

            ?>
            <!-- SCRIPT SWEETALERT INLINE -->
            <script>
                $(window).on('load', function () {
                    let pesan = "<?= $pesan ?>";
                    // swal('Oops!',pesan,'error');
                    toastr.error(pesan, 'Peringatan', {timeOut: 5000});
                });
            </script>
        <?php endif; ?>
    </body>
    <!-- END BODY -->
</html>

<script type="text/javascript">
    $(".success").click(function () {
        toastr.success('We do have the Kapua suite available.', 'Success Alert', {timeOut: 5000})
    });
    $(".error").click(function () {
        toastr.error('You Got Error', 'Inconceivable!', {timeOut: 5000})
    });
    $(".info").click(function () {
        toastr.info('It is for your kind information', 'Information', {timeOut: 5000})
    });
    $(".warning").click(function () {
        toastr.warning('It is for your kind warning', 'Warning', {timeOut: 5000})
    });
</script>