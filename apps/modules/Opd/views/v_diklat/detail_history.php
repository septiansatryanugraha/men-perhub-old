<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <!-- /.box-header -->
                <div class="box-body">		
                    <table id="tableku" class=" table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Kode Pengajuan</th>
                                <th>Status</th>
                                <th>Keterangan Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($dataHistory as $data) {

                                ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $data->kode_pengajuan ?></td>
                                    <td><?= $data->status ?></td>
                                    <td><?= '' . $data->keterangan_status . '<br><b>' . date('d-m-Y H:i:s', strtotime($data->created_date)) . '</b>' ?></td>
                                </tr>
                                <?php
                                $no++;
                            }

                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
//        $('#tableku').DataTable();
    });
</script>