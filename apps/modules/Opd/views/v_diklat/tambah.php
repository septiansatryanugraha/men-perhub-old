<style>
    #btn_loading {
        display: none;
    }
    .margin-callout {
        margin-left: 20px;
        margin-top: 20px;
    }
</style>
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="box">
        <div class="row">
            <div class="col-md-9">
                <div class="box-header with-border">
                    <h3 class="box-title">Pengajuan Diklat PKB</h3>
                </div>
                <div class="callout callout-danger margin-callout">
                    <h4>Perhatikan dengan baik !</h4>
                    <p>Pastikan sebelum melakukan upload dokumen Pengajuan Diklat PKB harus sesuai dengan persyaratan yang terlampir karena dokumen yang sesuai dengan persyaratan akan kami verifikasi :</p>
                    <p>1. Upload pengantar permohonan dari Direktur/Kadis/Ka. BPTD/ Ka. UPT/Pimpinan Pengujian
                        APM/Swasta untuk mengikuti Diklat PKB.</p>
                    <p>2.  Upload surat pernyataan dari Direktur/Kadis/Ka. BPTD/ Ka. UPT/Pimpinan Pengujian APM/
                        Swasta bahwa ybs sedang ditugaskan di bidang pengujian. </p>
                    <p>3.  <b>Bagi ASN yang berstatus P3K</b> wajib upload surat pernyataan dari Direktur/Kadis/Ka. BPTD/
                        Ka. UPT yang mengizinkan ybs untuk mengikuti Diklat PKB . </p>
                    <p>4.  Upload Penilaian Prestasi kerja bernilai Baik 1 tahun
                        terakhir/DP3. </p>
                    <p>5.  Upload DRH. </p>
                    <p>6.  Upload surat keterangan sehat jasmani & rohani dari dokter pemerintah. </p>
                </div>     
                <form class="form-horizontal" id="form-pengajuan" method="POST" enctype="multipart/form-data">
                    <input type="hidden" value="<?= isset($_POST['folder']) ? $_POST['folder'] : time() ?>" name="folder">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Jenis Diklat</label>
                            <div class="col-sm-3">
                                <select name="id_kategori_diklat" class="form-control select-diklat" id="id_kategori_diklat">
                                    <option></option>
                                    <?php foreach ($jenisDiklat as $data) { ?>
                                        <option value="<?= $data->id_kategori_diklat ?>"><?= $data->kategori_diklat; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Pengantar Permohonan<sup style="color:red; font-weight:bold;">*</sup></label>
                            <div class="col-sm-5">
                                <input type="file"  name="others[]" id="pengantar_permohonan"  onchange="return cekMandatoryPdf('pengantar_permohonan')"/>
                            </div>
                            <div class="col-sm-2">
                                <small class="label pull-center bg-red">format .pdf | max 300 kb </small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Surat Pernyataan<sup style="color:red; font-weight:bold;">*</sup></label>
                            <div class="col-sm-5">
                                <input type="file"  name="others[]" id="surat_pernyataan"  onchange="return cekMandatoryPdf('surat_pernyataan')"/>
                            </div>
                            <div class="col-sm-2">
                                <small class="label pull-center bg-red">format .pdf | max 300 kb </small>
                            </div>
                        </div>
                        <?php if ($statusPegawai == 'P3K') { ?>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Surat Pernyataan ( Bagi ASN dengan status P3K )<sup style="color:red; font-weight:bold;">*</sup></label>
                                <div class="col-sm-5">
                                    <input type="file"  name="others[]" id="surat_pernyataan2"  onchange="return cekMandatoryPdf('surat_pernyataan2')"/>
                                </div>
                                <div class="col-sm-2">
                                    <small class="label pull-center bg-red">format .pdf | max 300 kb </small>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Penilaian Prestasi<sup style="color:red; font-weight:bold;">*</sup></label>
                            <div class="col-sm-5">
                                <input type="file"  name="others[]" id="penilaian_prestasi"  onchange="return cekMandatoryPdf('penilaian_prestasi')"/>
                            </div>
                            <div class="col-sm-2">
                                <small class="label pull-center bg-red">format .pdf | max 300 kb </small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">DRH<sup style="color:red; font-weight:bold;">*</sup></label>
                            <div class="col-sm-5">
                                <input type="file"  name="others[]" id="drh"  onchange="return cekMandatoryPdf('drh')"/>
                            </div>
                            <div class="col-sm-2">
                                <small class="label pull-center bg-red">format .pdf | max 300 kb </small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Surat Keterangan Sehat<sup style="color:red; font-weight:bold;">*</sup></label>
                            <div class="col-sm-5">
                                <input type="file"  name="others[]" id="sks"  onchange="return cekMandatoryPdf('sks')"/>
                            </div>
                            <div class="col-sm-2">
                                <small class="label pull-center bg-red">format .pdf | max 300 kb </small>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div id="buka">
                                <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                            <div id="btn_loading">
                                <button type="submit" class="btn btn-success btn-flat" disabled><i class='fa fa-refresh fa-spin'></i> Tunggu...</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.row -->
    </div>
</section> 

<script type="text/javascript">
    $('#form-pengajuan').submit(function (e) {
        e.preventDefault();
        var error = 0;
        var message = "";

        if (error == 0) {
            var id_kategori_diklat = $("#id_kategori_diklat").val();
            var id_kategori_diklat = id_kategori_diklat.trim();
            if (id_kategori_diklat.length == 0) {
                error++;
                message = "Jenis Diklat wajib di isi.";
            }
        }
        if (error == 0) {
            var pengantar_permohonan = $("#pengantar_permohonan").val();
            var pengantar_permohonan = pengantar_permohonan.trim();
            if (pengantar_permohonan.length == 0) {
                error++;
                message = "Surat Pengantar wajib di isi.";
            }
        }
        if (error == 0) {
            var surat_pernyataan = $("#surat_pernyataan").val();
            var surat_pernyataan = surat_pernyataan.trim();
            if (surat_pernyataan.length == 0) {
                error++;
                message = "Surat Pernyataan wajib di isi.";
            }
        }
        if (error == 0) {
            var drh = $("#drh").val();
            var drh = drh.trim();
            if (drh.length == 0) {
                error++;
                message = "File DRH wajib di isi.";
            }
        }
        if (error == 0) {
            var sks = $("#sks").val();
            var sks = sks.trim();
            if (sks.length == 0) {
                error++;
                message = "File Surat Keterangan Sehat wajib di isi.";
            }
        }
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?= base_url('save-diklat'); ?>',
                type: 'POST',
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    setTimeout("window.location='<?= base_url("data-diklat"); ?>'", 1000);
                    swal("Success", result.pesan, "success");
                } else if (result.status == 'kosong') {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });

    function cekMandatoryPdf(variable) {
        var fileInput = document.getElementById(variable).value;
        if (fileInput != '') {
            var checkfile = fileInput.toLowerCase();
            // validasi ekstensi file
            if (!checkfile.match(/(\.pdf)$/)) {
                // swal("Peringatan", "File harus format .pdf", "warning");
                toastr.error('File harus format .pdf', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                document.getElementById(variable).value = '';
                return false;
            }
            var ukuran = document.getElementById(variable);
            // validasi ukuran size file
            if (ukuran.files[0].size > 307200) {
                // swal("Peringatan", "File harus maksimal 5MB", "warning");
                toastr.error('File harus maksimal 300 kb', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                ukuran.value = '';
                return false;
            }
            return true;
        }
    }

    $(function () {
        $(".select-diklat").select2({
            placeholder: " -- Pilih jenis diklat -- "
        });
    });
</script>