<style>
    #btn_loading {
        display: none;
    }
    .field-icon {
        float: left;
        margin-left: 93%;
        margin-top: -27px;
        position: relative;
        z-index: 2;
    }
    #slider  img  { 
        max-width:180px;
        margin-left:0px;
        margin-top:20px;
        margin-bottom:20px; 
    }
</style>

<section class="content">
    <div class="box">
        <div class="row">
            <div class="col-md-9"> 
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Profi OPD</h3>
                </div>
                <form class="form-horizontal" id="form-tambah" method="POST">
                    <input type="hidden" name="id_user" value="<?= $sessid; ?>">
                    <input type="hidden"  name="email" value="<?= $brand->email; ?>">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap </label>
                            <div class="col-sm-6">
                                <input type="text" name="nama_lengkap" class="form-control" placeholder="Nama Lengkap" id="nama_lengkap" max-length="35" autocomplete="off" value="<?= $user->nama_lengkap; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Tempat Lahir</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir" id="tempat_lahir" onkeyup="javascript:capitalize(this.id, this.value);" value="<?= $user->tempat_lahir; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Lahir</label>
                            <div class="col-sm-4">
                                <input type="text" id="tgl_lahir" class="form-control tgl_lahir" name="tgl_lahir" placeholder="Tanggal Lahir" value="<?= date('d-m-Y', strtotime($user->tgl_lahir)); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jenis Kelamin</label>
                            <div class="col-sm-2">
                                <select name="jkel" class="form-control select-jkel" id="jkel">
                                    <?php foreach ($jkel as $data) { ?>
                                        <option value="<?= $data->nama_kategori ?>" <?= ($data->nama_kategori == $user->jkel) ? "selected" : ""; ?>><?= $data->nama_kategori; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Status Kepegawaian</label>
                            <div class="col-sm-2">
                                <select name="status_kepegawaian" class="form-control select-jkel" id="status_kepegawaian">
                                    <?php foreach ($status_pegawai as $data) { ?>
                                        <option value="<?= $data->nama_kategori ?>" <?= ($data->nama_kategori == $user->status_kepegawaian) ? "selected" : ""; ?>><?= $data->nama_kategori; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Instansi</label>
                            <div class="col-sm-4">
                                <select name="instansi" class="form-control select-jkel" id="instansi">
                                    <?php foreach ($instansi as $data) { ?>
                                        <option value="<?= $data->nama_kategori ?>" <?= ($data->nama_kategori == $user->instansi) ? "selected" : ""; ?>><?= $data->nama_kategori; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Asal Instansi</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="asal_instansi" placeholder="Asal instansi" id="asal_instansi" value="<?= $user->asal_instansi; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Alamat Instansi</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="alamat_instansi" placeholder="Alamat instansi" id="alamat_instansi" value="<?= $user->alamat_instansi; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">No Registrasi</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="no_reg" placeholder="No Registrasi" id="alamat_instansi" value="<?= $user->no_reg; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">No Handphone </label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="nomor_hp" placeholder="Masukan Nomor Handphone" autocomplete="off" value="<?= $user->nomor_hp; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-5">
                                <input type="text" name="email" class="form-control" placeholder="Masukan Email" autocomplete="off" value="<?= $email; ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Password </label>
                            <div class="col-sm-5">
                                <input type="password" name="password" class="form-control" id="password-field" placeholder="Password"  autocomplete="off" value="<?= base64_decode($user->password); ?>">
                                <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Profil</label>
                            <div class="col-sm-6">
                                <div id="slider">
                                    <?php if ($user->photo != NULL) { ?>
                                        <img class="img-thumbnail" src='<?= base_url(); ?><?= $user->photo; ?>'>
                                    <?php } else { ?>
                                        <img class="img-thumbnail" src="<?= base_url(); ?>assets/tambahan/gambar/no-image.jpg" width="150px">
                                    <?php } ?>
                                </div>
                                <input type="file"  name="photo" id="gambar" onchange="return fileValidation()"/>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div id="buka">
                                <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                            </div>
                            <div id="btn_loading">
                                <button type="submit" class="btn btn-success btn-flat" disabled><i class='fa fa-refresh fa-spin'></i> Tunggu...</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $(".tgl_lahir").datepicker({
                orientation: "left",
                autoclose: !0,
                format: 'dd-mm-yyyy'
            })

            $(".select-jkel").select2({
                placeholder: " -- Pilih jenis kelamin -- "
            });
        });

        $('#form-tambah').submit(function (e) {
            var data = $(this).serialize();
            var error = 0;
            var message = "";
            if (error == 0) {
                var nama_lengkap = $("#nama_lengkap").val();
                var nama_lengkap = nama_lengkap.trim();
                if (nama_lengkap.length == 0) {
                    error++;
                    message = "Nama Lengkap Wajib di isi.";
                }
            }
            if (error == 0) {
                var password_field = $("#password-field").val();
                var password_field = password_field.trim();
                if (password_field.length == 0) {
                    error++;
                    message = "Password wajib di isi.";
                }
            }
            if (error == 0) {
                $.ajax({
                    method: 'POST',
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    url: '<?= base_url('update-profil-opd'); ?>',
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        setTimeout(location.reload.bind(location), 1000);
                        swal("Success", result.pesan, "success");
                    } else {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        swal("Warning", result.pesan, "warning");
                    }
                })
                e.preventDefault();
            } else {
                swal("Peringatan", message, "warning");
                return false;
            }
        });
        // untuk show hide password
        $(".toggle-password").click(function () {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    </script>
</section>