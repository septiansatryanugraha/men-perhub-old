<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends MX_Controller
{
    const __tableName = 'tbl_bukti_pembayaran';
    const __tableName2 = 'tbl_history';
    const __tableId = 'id_pembayaran';

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_pembayaran');
        $this->load->model('M_user');
    }

    public function index($param = '')
    {
        header('Location: ' . base_url() . '');
    }

    function home()
    {
        if ($this->lib->login() == "") {
            $this->session->set_flashdata('not_login', '<div class="ui success message"><i class="close icon"></i><div class="header">Silahkan login terlebih dahulu.</div></div>');
            redirect('homepage');
        } else {
            $data = [
                'judul' => "Pembayaran Billing ",
                'deskripsi' => "Manage Data Pembayaran",
            ];

            $data['user'] = $this->M_user->selectById($this->session->userdata('id'));

            $this->load->view('template/head', $data);
            $this->load->view('template/topbar', $data);
            $this->load->view('template/sidebar', $data);
            $this->load->view('_heading/_headerContent', $data);
            $this->load->view('v_pembayaran/home', $data);
            $this->load->view('template/js');
            $this->load->view('template/foot');
        }
    }

    public function ajaxList()
    {
        $list = $this->Mdl_pembayaran->getWhere('id_user', $this->session->userdata('id'));

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->kode_pengajuan;
            $row[] = $brand->keterangan;
            $buttonUpload = '';

            //add html for action
            if ($brand->status == 'Lunas') {
                $action = '<span class="badge bg-blue">Lunas</span>';
            } else {
                $action = " <div class='btn-group'>";
                $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
                $action .= "    <ul class='dropdown-menu align-left pull-right'>";
                $action .= "    <li><a href='" . base_url('upload-pembayaran') . "/" . $brand->id_pembayaran . "'><i class='fa fa-edit'></i> Upload Pembayaran</a></li>";
                $action .= "    </ul>";
                $action .= "</div>";
            }
            $row[] = $action;

            $row[] = $buttonUpload;
            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function uploadPembayaran($id)
    {
        $brand = $this->Mdl_pembayaran->selectById($id);
        if ($brand != null) {
            $data = [
                'judul' => "Pembayaran Billing",
                'deskripsi' => " Upload Pembayaran Billing PNBP ",
                'sessid' => $this->session->userdata('id'),
                'email' => $this->session->userdata('email'),
                'nama' => $this->session->userdata('nama_lengkap'),
            ];

            $data['brand'] = $this->Mdl_pembayaran->selectById($id);
            $data['user'] = $this->M_user->selectById($this->session->userdata('id'));

            $this->load->view('template/head', $data);
            $this->load->view('template/topbar', $data);
            $this->load->view('template/sidebar', $data);
            $this->load->view('_heading/_headerContent', $data);
            $this->load->view('v_pembayaran/upload_pembayaran', $data);
            $this->load->view('template/js');
            $this->load->view('template/foot');
        } else {
            echo "<script>alert('Pembayaran tidak tersedia.'); window.location = '" . base_url('data-pembayaran') . "';</script>";
        }
    }

    public function prosesUpload($id)
    {
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');
        $namaLengkap = $this->session->userdata('nama_lengkap');

        $errCode = 0;
        $errMessage = "";

        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID is invalid.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->Mdl_pembayaran->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = "Pembayaran tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $config['upload_path'] = "./upload/berkas_pembayaran/";
                $config['allowed_types'] = 'pdf';
                $config['max_size'] = '2048'; //maksimum besar file 2M
                $config['encrypt_name'] = TRUE;
                $config['overwrite'] = TRUE;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload("file_upload_pembayaran")) {
                    $path['link'] = "upload/berkas_pembayaran/";
                    $imageData = $this->upload->data();

                    $data = [
                        'nama_file_upload' => $imageData['file_name'],
                        'file_upload_pembayaran' => $path['link'] . '' . $imageData['file_name'],
                        'updated_date' => $datetime,
                        'updated_by' => $namaLengkap,
                    ];
                    $result = $this->db->update('tbl_bukti_pembayaran', $data, ['id_pembayaran' => $id]);

                    $data2 = [
                        'kode_pengajuan' => $checkValid->kode_pengajuan,
                        'id_user' => $this->session->userdata('id'),
                        'keterangan_status' => 'user <b>' . $namaLengkap . '</b> Sedang melakukan proses upload pembayaran Billing PNBP ke sistem E-Kompetensi',
                        'status' => 'Melakukan Pembayaran',
                        'created_by' => 'System',
                        'created_date' => $datetime,
                    ];
                    $result = $this->db->insert('tbl_history', $data2);

                    $config = [
                        'useragent' => 'Codeigniter',
                        'protocol' => 'smtp',
                        'smtp_host' => 'smtp.gmail.com',
                        'smtp_port' => 587,
                        'smtp_crypto' => 'tls',
                        'smtp_timeout' => 100,
                        'smtp_user' => 'angkasamuda20@gmail.com',
                        'smtp_pass' => 'agkasamuda2020',
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'newline' => "\r\n"
                    ];

                    $this->load->library('email');
                    $this->email->initialize($config);

                    $this->email->from($this->session->userdata('email'));
                    $this->email->to('angkasamuda20@gmail.com');

                    $this->email->subject('Notifikasi Informasi Pembayaran Biling PNBP');
                    $message = '
		<h3 align="center">Info Pembayaran Billing PNBP</h3>
		<center><table border="1" width="50%" cellpadding="5">
		<tr>
       	<td width="30%">No Kode Pengajuan</td>
	   	<td width="70%">' . $checkValid->kode_pengajuan . '</td>
       	</tr>

       	<tr>
       	<td width="30%">Email Dari</td>
	   	<td width="40%">' . $this->session->userdata('email') . '</td>
       	</tr>

       	<tr>
       	<td width="30%">Nama User</td>
	   	<td width="40%">' . $namaLengkap . '</td>
       	</tr>

       	<tr>
       	<td width="30%">keterangan</td>
	   	<td width="40%">Upload pembayaran Billing PNBP</td>
       	</tr>

       	</table></center>';

                    $this->email->message($message);
                    $this->email->attach($imageData['full_path']);
                    $this->email->send();
                }
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
