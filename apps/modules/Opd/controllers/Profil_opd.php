<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil_opd extends MX_Controller
{
    const __tableName = 'tbl_user';
    const __tableName2 = 'tbl_login';
    const __tableId = 'id_user';
    const __tableIdname = 'name';

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_kategori_status');
        $this->load->model('M_user');
    }

    public function index($param = '')
    {
        header('Location: ' . base_url() . '');
    }

    public function profil()
    {
        if ($this->lib->login() != "") {

            $data = array(
                'title' => 'Profil User',
                'sessid' => $this->session->userdata('id'),
                'nama_lengkap' => $this->session->userdata('nama_lengkap'),
                'nomor_hp' => $this->session->userdata('nomor_hp'),
                'email' => $this->session->userdata('email'),
                'nama' => $this->session->userdata('nama'),
                'password' => $this->session->userdata('password'),
                'judul' => "Profil User",
                'deskripsi' => "Manage Data Profil User",
            );

            $data['user'] = $this->M_user->selectById($this->session->userdata('id'));
            $data['jkel'] = $this->M_kategori_status->select([], ['Pria', 'Wanita']);
            $data['status_pegawai'] = $this->M_kategori_status->select([], ['PNS', 'P3K', 'NON ASN']);
            $data['instansi'] = $this->M_kategori_status->select([], ['Dishub Provinsi', 'Dishub Kabupaten', 'Dishub Kota', 'Ditjen Perhubungan Darat', 'Lembaga Diklat']);

            $this->load->view('template/head', $data);
            $this->load->view('template/topbar', $data);
            $this->load->view('template/sidebar', $data);
            $this->load->view('_heading/_headerContent', $data);
            $this->load->view('v_opd/profil', $data);
            $this->load->view('template/js');
            $this->load->view('template/foot');
        } else {
            $this->session->set_flashdata('not_login', '<p style="color: red;">Silahkan login upload dokumen pengajuan.</p>');
            redirect('login');
        }
    }

    public function prosesUpdate()
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $id = $this->session->userdata('id');
        $namaLengkap = $this->input->post('nama_lengkap');
        $tempatLahir = $this->input->post('tempat_lahir');
        $tglLahir = $this->input->post('tgl_lahir');
        $jkel = $this->input->post('jkel');
        $statusPegawai = $this->input->post('status_kepegawaian');
        $instansi = $this->input->post('instansi');
        $asalInstansi = $this->input->post('asal_instansi');
        $alamatInstansi = $this->input->post('alamat_instansi');
        $noReg = $this->input->post('no_reg');
        $nomorHp = $this->input->post('nomor_hp');
        $password = $this->input->post('password');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $checkValid = $this->M_user->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($namaLengkap) == 0) {
                $errCode++;
                $errMessage = "Nama lengkap wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tempatLahir) == 0) {
                $errCode++;
                $errMessage = "Tempat Lahir wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglLahir) == 0) {
                $errCode++;
                $errMessage = "Tanggal Lahir wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($jkel) == 0) {
                $errCode++;
                $errMessage = "Jenis Kelamin wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($statusPegawai) == 0) {
                $errCode++;
                $errMessage = "Status Kepegawaian wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($instansi) == 0) {
                $errCode++;
                $errMessage = "Instansi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($asalInstansi) == 0) {
                $errCode++;
                $errMessage = "Asal instansi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($alamatInstansi) == 0) {
                $errCode++;
                $errMessage = "Alamat Instansi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noReg) == 0) {
                $errCode++;
                $errMessage = "No Registrasi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($nomorHp) == 0) {
                $errCode++;
                $errMessage = "Nomor HP wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($password) == 0) {
                $errCode++;
                $errMessage = "Password wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'nama_lengkap' => $namaLengkap,
                    'tempat_lahir' => $tempatLahir,
                    'tgl_lahir' => date('Y-m-d', strtotime($tglLahir)),
                    'jkel' => $jkel,
                    'status_pegawai' => $statusPegawai,
                    'instansi' => $instansi,
                    'asal_instansi' => $asalInstansi,
                    'alamat_instansi' => $alamatInstansi,
                    'no_reg' => $noReg,
                    'nomor_hp' => $nomorHp,
                    'password' => base64_encode($password),
                    'ip_address' => $this->input->ip_address(),
                    'user_agent' => $this->input->user_agent(),
                    'updated_by' => $username,
                    'updated_date' => $datetime
                ];
                $path['link'] = "upload/foto/" . md5($checkValid->kode_otomatis) . "/";
                $upath = './' . $path['link'];
                if (!file_exists($upath)) {
                    mkdir($upath, 0777);
                }

                $config['upload_path'] = $upath;
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = '2048'; //maksimum besar file 2M
                $config['file_name'] = 'file_' . time() . '_' . $_FILES['photo']['name'];
                $config['overwrite'] = TRUE;
                $this->load->library('upload', $config);
                $photo = $this->upload->data();
                if ($this->upload->do_upload('photo')) {
                    $data = array_merge($data, [
                        'photo' => $path['link'] . '' . $photo['file_name'],
                    ]);
                }
                $result = $this->db->update(self::__tableName, $data, [self::__tableId => $id]);
                $result = $this->db->update(self::__tableName2, ['password' => base64_encode($password)], ['email' => $checkValid->email]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
