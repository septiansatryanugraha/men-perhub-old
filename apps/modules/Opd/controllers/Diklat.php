<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diklat extends MX_Controller
{
    const __tableName = 'tbl_diklat';
    const __tableName2 = 'tbl_history';
    const __tableId = 'id_perpanjangan';

    private $allowed_img_types = 'gif|jpg|png|jpeg|JPG|PNG|JPEG|pdf';

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_diklat');
        $this->load->model('M_user');
        $this->load->model('M_kategori_diklat');
        $this->load->model('M_generate_code');
        $this->load->model('M_utilities');
    }

    public function index($param = '')
    {
        header('Location: ' . base_url() . '');
    }

    function home()
    {
        if ($this->lib->login() == "") {
            $this->session->set_flashdata('not_login', '<div class="ui success message"><i class="close icon"></i><div class="header">Silahkan login terlebih dahulu.</div></div>');
            redirect('homepage');
        } else {

            $data = [
                'title' => 'Diklat PKB ',
                'judul' => "Pengajuan Diklat PKB",
                'deskripsi' => "Manage Data Pengajuan ",
            ];

            $data['user'] = $this->M_user->selectById($this->session->userdata('id'));

            $this->load->view('template/head', $data);
            $this->load->view('template/topbar', $data);
            $this->load->view('template/sidebar', $data);
            $this->load->view('_heading/_headerContent', $data);
            $this->load->view('v_diklat/home', $data);
            $this->load->view('template/js');
            $this->load->view('template/foot');
        }
    }

    public function ajaxList()
    {
        $list = $this->Mdl_diklat->getWhere(['id_user' => $this->session->userdata('id')]);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->kode_pengajuan;
            $row[] = $brand->email;
            $row[] = '' . $brand->keterangan_status . '<br><b>' . date('d-m-Y', strtotime($brand->created_date)) . '</b></br><b>
			Status Pengajuan : ' . $brand->status_pengajuan . '</b>';

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            $action .= "    <li><a href='#' class='detail-history' data-toggle='tooltip' data-placement='top' data-id='" . $brand->kode_pengajuan . "'><i class='glyphicon glyphicon-info-sign'></i> Detail</a></li>";
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function Add()
    {
        if ($this->lib->login() != "") {
            $this->load->module('template');

            $data = [
                'title' => "Diklat PKB",
                'judul' => "Pengajuan Diklat PKB",
                'deskripsi' => "Manage Data Pengajuan ",
                'jenisDiklat' => $this->M_kategori_diklat->select(),
                'statusPegawai' => $this->session->userdata('status_kepegawaian'),
            ];

            $data['user'] = $this->M_user->selectById($this->session->userdata('id'));

            $this->load->view('template/head', $data);
            $this->load->view('template/topbar', $data);
            $this->load->view('template/sidebar', $data);
            $this->load->view('_heading/_headerContent', $data);
            $this->load->view('v_diklat/tambah', $data);
            $this->load->view('template/js');
            $this->load->view('template/foot');
        } else {
            $this->session->set_flashdata('not_login', '<p style="color: red;">ilahkan login untuk upload dokumen pengajuan.</p>');
            redirect('homepage');
        }
    }

    public function prosesAdd()
    {
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');
        $namaLengkap = $this->session->userdata('nama_lengkap');

        $errCode = 0;
        $errMessage = "";

        $idKategoriDiklat = $this->input->post('id_kategori_diklat');

        if ($errCode == 0) {
            if (strlen($idKategoriDiklat) == 0) {
                $errCode++;
                $errMessage = "Jenis Diklat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKategoriDiklat = $this->M_kategori_diklat->selectById($idKategoriDiklat);
            if ($checkKategoriDiklat == null) {
                $errCode++;
                $errMessage = "Jenis Diklat tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $kode = $this->M_generate_code->getNextDiklatPkb();

                $folder = ['date' => date('Ymd'), 'code' => md5($kode)];

                $data = [
                    'id_user' => $this->session->userdata('id'),
                    'id_kategori_diklat' => $idKategoriDiklat,
                    'kode_pengajuan' => $kode,
                    'email' => $this->session->userdata('email'),
                    'keterangan_status' => 'user <b>' . $namaLengkap . '</b> Sedang melakukan proses pengajuan untuk mengikuti Diklat ' . $checkKategoriDiklat->kategori_diklat . ' ke sistem E-Kompetensi',
                    'folder' => $folder['date'] . '/' . $folder['code'],
                    'status_pengajuan' => 'Pending',
                    'tgl_ba' => $date,
                    'tgl_surat' => $date,
                    'tanggal' => $date,
                    'created_date' => $datetime,
                    'created_by' => $namaLengkap,
                    'updated_date' => $datetime,
                    'updated_by' => $namaLengkap,
                ];
                $result = $this->db->insert('tbl_diklat', $data);

                $data2 = [
                    'kode_pengajuan' => $kode,
                    'id_user' => $this->session->userdata('id'),
                    'keterangan_status' => 'user <b>' . $namaLengkap . '</b> Sedang melakukan proses pengajuan untuk mengikuti Diklat ' . $checkKategoriDiklat->kategori_diklat . ' ke sistem E-Kompetensi',
                    'status' => 'Pending',
                    'created_by' => 'System',
                    'created_date' => $datetime,
                ];
                $result = $this->db->insert('tbl_history', $data2);

                $this->doUploadOthersImages($folder);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function detailHistory()
    {
        $kodePengajuan = $_POST['kode_pengajuan'];
        $dataHistory = $this->M_utilities->getHistory(['kode_pengajuan' => $kodePengajuan]);
        if ($dataHistory) {
            $data['dataHistory'] = $dataHistory;

            echo '  <div class="modal fade" id="detail-history" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document" style="width: 70%;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">History</h4>
                                </div>
                                ' . $this->load->view('v_diklat/detail_history', $data, TRUE) . '
                            </div>
                        </div>
					</div>';
        } else {
            echo "<script>alert('History tidak tersedia.'); window.location = '" . base_url('data-diklat') . "';</script>";
        }
    }

    private function doUploadOthersImages($folder = [])
    {
        $upath = './upload/berkas_diklat/' . $folder['date'] . '/';
        if (!file_exists($upath)) {
            mkdir($upath, 0777);
        }
        $upath = './upload/berkas_diklat/' . $folder['date'] . '/' . $folder['code'] . '/';
        if (!file_exists($upath)) {
            mkdir($upath, 0777);
        }

        $this->load->library('upload');

        $files = $_FILES;
        $cpt = count($_FILES['others']['name']);
        for ($i = 0; $i < $cpt; $i++) {
            unset($_FILES);
            $_FILES['others']['name'] = $files['others']['name'][$i];
            $_FILES['others']['type'] = $files['others']['type'][$i];
            $_FILES['others']['tmp_name'] = $files['others']['tmp_name'][$i];
            $_FILES['others']['error'] = $files['others']['error'][$i];
            $_FILES['others']['size'] = $files['others']['size'][$i];

            $this->upload->initialize(array(
                'upload_path' => $upath,
                'allowed_types' => $this->allowed_img_types
            ));
            $this->upload->do_upload('others');
        }
    }
}
