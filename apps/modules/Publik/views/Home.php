<style type="text/css">
    #mapid { height: 400px; }
    li{
        margin-bottom: 5px;
    }
    #btn_loading { display: none; }
    .field-icon {
        float: left;
        margin-left: 90%;
        margin-top: -25px;
        font-size: 17px;
        position: relative;
        z-index: 2;
        color: #ccc;
    }

    .treadmill {
        overflow: hidden;
        padding: 0 20px;
    }

    .treadmill-unit {
        overflow: hidden;
        position: relative;
    }
    .select2-container .select2-choice > .select2-chosen{
        color:#000;
    }
    .gap {
        margin-bottom: 20px;
    }

    .frame {
        border-radius:5px;
    }

    /* START TOOLTIP STYLES */
    [tooltip] {
        position: relative; /* opinion 1 */
    }

    /* Applies to all tooltips */
    [tooltip]::before,
    [tooltip]::after {
        text-transform: none; /* opinion 2 */
        font-size: .9em; /* opinion 3 */
        line-height: 1;
        user-select: none;
        pointer-events: none;
        position: absolute;
        display: none;
        opacity: 0;
    }
    [tooltip]::before {
        content: '';
        border: 5px solid transparent; /* opinion 4 */
        z-index: 1001; /* absurdity 1 */
    }
    [tooltip]::after {
        content: attr(tooltip); /* magic! */

        /* most of the rest of this is opinion */
        font-family: Helvetica, sans-serif;
        text-align: center;

        /* 
          Let the content set the size of the tooltips 
          but this will also keep them from being obnoxious
        */
        min-width: 3em;
        max-width: 21em;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        padding: 1ch 1.5ch;
        border-radius: .3ch;
        box-shadow: 0 1em 2em -.5em rgba(0, 0, 0, 0.35);
        background: #333;
        color: #fff;
        z-index: 1000; /* absurdity 2 */
    }

    /* Make the tooltips respond to hover */
    [tooltip]:hover::before,
    [tooltip]:hover::after {
        display: block;
    }

    /* don't show empty tooltips */
    [tooltip='']::before,
    [tooltip='']::after {
        display: none !important;
    }

    /* FLOW: UP */
    [tooltip]:not([flow])::before,
    [tooltip][flow^="up"]::before {
        bottom: 150%;
        margin-left: -70px;
        border-bottom-width: 0;
        border-top-color: #333;
    }
    [tooltip]:not([flow])::after,
    [tooltip][flow^="up"]::after {
        bottom: calc(150% + 5px);
        margin-left: -70px;
    }
    [tooltip]:not([flow])::before,
    [tooltip]:not([flow])::after,
    [tooltip][flow^="up"]::before,
    [tooltip][flow^="up"]::after {
        left: 50%;
        transform: translate(-50%, -.5em);
    }

    /* KEYFRAMES */
    @keyframes tooltips-vert {
        to {
            opacity: .9;
            transform: translate(-50%, 0);
        }
    }

    @keyframes tooltips-horz {
        to {
            opacity: .9;
            transform: translate(0, -50%);
        }
    }

    /* FX All The Things */ 
    [tooltip]:not([flow]):hover::before,
    [tooltip]:not([flow]):hover::after,
    [tooltip][flow^="up"]:hover::before,
    [tooltip][flow^="up"]:hover::after,
    [tooltip][flow^="down"]:hover::before,
    [tooltip][flow^="down"]:hover::after {
        animation: tooltips-vert 300ms ease-out forwards;
    }

    [tooltip][flow^="left"]:hover::before,
    [tooltip][flow^="left"]:hover::after,
    [tooltip][flow^="right"]:hover::before,
    [tooltip][flow^="right"]:hover::after {
        animation: tooltips-horz 300ms ease-out forwards;
    }
</style>
<?= $script_captcha; // javascript recaptcha ?>



<br><br><br>
<div class="container">

    <br><br><br>
    <div class="row" >


        <div class="col-md-8 wow slideInLeft">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp; Informasi Dashboard</h3>
                </div>
                <div class="panel-body">

                    <div  id="top" class="callbacks_container gap">
                        <ul class="rslides" id="slider4">
                            <?php foreach ($slider as $key) { ?>
                                <li>
                                    <img class="frame" src="<?= base_url(); ?><?= $key->gambar ?>" width="100%" alt="">
                                    <div class="caption">
                                        <div class="slide-text-info">
                                            <b style='color: #ffffff; font-size: 25px;'><?= $key->judul ?></b>
                                            <p style='color: #ffffff; font-size: 15px;'><?= $key->deskripsi ?></p> 
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>


                    <div class="col-md-12" style="background-color:#337ab7; padding:7px; color: white">
                        <div class="col-md-12">
                            <marquee><h5 style="text-align:center"><b>Dapatakan Informasi terbaru dari kami terkait proses dan jadwal sertifikasi </b></h5></marquee>
                        </div>

                    </div>    
                    <div id="mytreadmill" class="treadmill list"> 
                        <?php
                        foreach ($posting as $key) {
                            echo "<div class='treadmill-unit'>";
                            echo anchor('Publik/Web/info/' . $key->id_broadcast, "
    <small class='pull-right time'><i class='fa fa-clock-o'> &nbsp;" . date('d-m-Y', strtotime($key->created_date)) . "</i>&nbsp; </small>
    <h4><i class='fa fa-info-circle'></i>&nbsp; " . $key->judul . "</h4> 
    <p>" . $key->konten . "</p>", "class='clear fancybox fancybox.iframe'");
                            echo "</div>";
                        }

                        ?>
                    </div>
                </div>  
            </div>
        </div>

        <div class="col-md-4 wow slideInRight">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; Login User</h3>
                </div>
                <div class="panel-body">
                    <form action="<?= base_url('Publik/Web/login_user'); ?>" method="post" id="FormLogin">
                        <div>
                            <div class="form-group">
                                <label for="berat">Email</label><br>
                                <div class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-inbox"></i></span>
                                    <input class="form-control" placeholder="Email" id="username" type="text" name="email" />
                                </div></div>
                            <div class="form-group">
                                <label for="berat">Password</label><br>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>

                                    <input class="form-control placeholder-no-fix" id="password-field" type="password" autocomplete="off" placeholder="Password" name="password"/><span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                </div></div>

                            <?= $captcha ?><br>

                            <div id='btn_loading'></div>
                            <div id="hilang">
                                <div class="form-actions">      
                                    <button type="submit" id="btnSignUp" class="btn btn-primary pull-right">
                                        <i class="fa fa-lock" aria-hidden="true"></i> &nbsp;Login
                                    </button>
                                </div></div> 

                            <div id="buka">
                                <div class="form-actions">      
                                    <button type="submit" id="btnSignUp" class="btn btn-primary pull-right">
                                        <i class="fa fa-unlock" aria-hidden="true"></i> &nbsp;Login
                                    </button>
                                </div></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>

</div>

<?php foreach ($kontak as $key) { ?>
    <div class="promo wow slideInUp">
        <div class="container">
            <div class="row" >
                <div class="col-md-4 ">
                    <h3><i class="fa fa-user" aria-hidden="true"></i>&nbsp; Kontak Kami</h3>
                    <p>Jika ada kendala silahkan hubungi<br>
                    <li><i class="fa fa-phone"></i>&nbsp; Hotline : <?= $key->no_telp ?></li>
                    <li><i class="fa fa-envelope-o"></i>&nbsp; Email : <?= $key->email ?></li>
                    <li><i class="fa fa-map-marker"></i>&nbsp; Alamat : <?= $key->alamat ?></li>

                    <div class="icon-box "><span tooltip="Kontak Saran">
                            <div class="icon-box-left"><a href="<?= site_url() . 'kontak-saran.html' ?>"><img src="<?= base_url(); ?>assets/publik/img/images/mail-box.png" height="100" width="90"></a></div></span></div>

                </div>
                <div class="col-md-8">
                    <h3><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp; Tentang E-KOMPETENSI</h3>
                    <p align="justify"><?= $key->deskripsi ?></p>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('#mytreadmill').startTreadmill({
            runAfterPageLoad: true,
            direction: "down",
            speed: "slow",
            viewable: 5,
            pause: false
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.fancybox').fancybox({
            'width': '80%',
            'height': '500',
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'type': 'iframe'
        });

    });
</script>


<script type="text/javascript" >
    function formValidation(oEvent) {
        oEvent = oEvent || window.event;
        var txtField = oEvent.target || oEvent.srcElement;

        var t1ck = true;
        var msg = " ";
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;


        if (document.getElementById("username").value.match(mailformat))
        {
            t1ck = true;
            msg = msg + "Benar";
        } else {
            t1ck = false;
            msg = msg + "Email tidak valid";
        }

// if(document.getElementById("username").value.length < 5 )
// { t1ck=false; msg = msg + "Your name should be minimun 3 char length";} 

        if (document.getElementById("password-field").value.length < 3)
        {
            t1ck = false;
            msg = msg + "Your name should be minimun 3 char length";
        }

// alert(msg + t1ck);

        if (t1ck) {
            document.getElementById("btnSignUp").disabled = false;
        } else {
            document.getElementById("btnSignUp").disabled = true;
        }
    }

    function resetForm() {
        document.getElementById("btnSignUp").disabled = true;
        var frmMain = document.forms[0];
        frmMain.reset();
    }

    window.onload = function () {

        var btnSignUp = document.getElementById("btnSignUp");
        var username = document.getElementById("username");
        var password = document.getElementById("password-field");

        var t1ck = false;
        document.getElementById("btnSignUp").disabled = true;
        username.onkeyup = formValidation;
        password.onkeyup = formValidation;
    }
</script>


<script>
// untuk show hide password
    $(".toggle-password").click(function () {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>


<script>
    $(function () {
        $("#buka").hide();
        $('#FormLogin').submit(function (e) {
            e.preventDefault();
            $.ajax({
                beforeSend: function () {
                    $("#buka").hide();
                    $("#hilang").hide();
                    $("#btn_loading").html("<div class='form-actions'><button class='btn btn-primary pull-right' disabled><i class='fa fa-refresh fa-spin'></i> &nbsp;Wait..</button></div>");
                    $("#btn_loading").show();
                },
                url: $(this).attr('action'),
                type: "POST",
                cache: false,
                data: $(this).serialize(),
                dataType: 'json',
                success: function (json) {
                    if (json.status == true) {
                        $("#btn_loading").hide();
                        $("#buka").show();
                        toastr.success(json.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                        window.location = json.url_home;
                    } else {
                        $("#btn_loading").hide();
                        $("#hilang").show();
                        toastr.error(json.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                    }
                }
            });
        });
    });
</script>

<script>
    $(document).ready(function () {
        $("#slider4").responsiveSlides({
            auto: true,
            pager: true,
            nav: true,
            speed: 500,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });
    });
</script>
