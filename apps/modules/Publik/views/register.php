<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="<?= base_url(); ?>assets/eksternal/font-awesome.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/publik/registrasi/css/gsdk-bootstrap-wizard.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?= base_url(); ?>assets/publik/registrasi/css/demo.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/select2/select2.min.css">
        <script src="<?= base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
        <script src="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    </head>
    <style type="text/css">
        #btn_loading {
            display: none;
        }
        .field-icon {
            float: left;
            margin-left: 90%;
            margin-top: -25px;
            font-size: 17px;
            position: relative;
            z-index: 2;
            color: #ccc;
        }
        .pilih-reg{
            width: 150px;
        }
        .select-jenis-jenjang{
            width: 270px;
        }
        .select-status-kepegawaian{
            width: 290px;
        }
        .select-jkel{
            width: 290px;
        }
        .select-pegawai{
            width: 290px;
        }
        .select-instansi{
            width: 200px;
        }
        .select-provinsi{
            width: 225px;
        }
        .select-kota{
            width: 350px;
        }
        .jarak-label {
            margin-top: 20px;
        }
        .footer2{
            margin-top: 30px
        }
    </style>
    <body>
        <div class="image-container set-full-height" style="background-color: #DADADA;">
            <style type="text/css">
                .klas-logo{
                    padding-top: 20px;
                    margin-bottom: -80px;
                }
            </style>
            <div class="wow slideInUp">
                <center><div class="klas-logo">
                        <a href="<?= base_url() ?>"><img class="image" src="<?= base_url(); ?>assets/publik/img/images/logo-dinas.png" width="300px"></a>
                    </div></center>
                <!--   Big container   -->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <div class="wizard-container">
                                <div class="card wizard-card" data-color="azzure" id="wizard">
                                    <form id="form-daftar" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" value="<?= isset($_POST['folder']) ? $_POST['folder'] : time() ?>" name="folder">
                                        <!--You can switch ' data-color="azzure" '  with one of the next bright colors: "blue", "green", "orange", "red"-->
                                        <div class="wizard-header">
                                            <h3>Pendaftaran <b>Akun Baru</b><br>
                                                <small>Silahkan daftar dan buat akun baru di E-Kopetensi / Sudah punya akun ? <a href="<?= base_url() ?>">Silahkan login</a></small>
                                            </h3>
                                        </div>
                                        <div class="wizard-navigation">
                                            <ul>
                                                <li><a href="#details" data-toggle="tab">Data diri</a></li>
                                                <li><a href="#captain" data-toggle="tab">Instansi</a></li>
                                                <li><a href="#description" data-toggle="tab">Kelengkapan Dokumen</a></li>
                                            </ul>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane" id="details">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <h4 class="info-text"> Silahkan lengkapi data diri anda</h4>
                                                    </div>
                                                    <div class="col-sm-10 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label>Nama Lengkap</label>
                                                            <input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" placeholder="Nama lengkap beserta gelar">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label>Jenis Kelamin</label>
                                                            <select name="jkel" class="form-control select-jkel" id="jkel">
                                                                <option></option>
                                                                <?php foreach ($jkel as $data) { ?>
                                                                    <option value="<?= $data->nama_kategori ?>"><?= $data->nama_kategori; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 ">
                                                        <div class="form-group">
                                                            <label>Status Kepegawaian</label>
                                                            <select name="status_pegawai" class="form-control select-pegawai" id="status_pegawai">
                                                                <option></option>
                                                                <?php foreach ($status_pegawai as $data) { ?>
                                                                    <option value="<?= $data->nama_kategori ?>"><?= $data->nama_kategori; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label>Tempat Lahir</label>
                                                            <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" placeholder="tempat lahir" onkeyup="javascript:capitalize(this.id, this.value);">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 ">
                                                        <div class="form-group">
                                                            <label>Tanggal Lahir</label>
                                                            <input type="text" class="form-control tgl_lahir" name="tgl_lahir" id="tgl_lahir" placeholder="Tanggal Lahir">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label>Email</label>
                                                            <input type="text" class="form-control" name="emails" id="emails" placeholder="Masukan Email" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 ">
                                                        <div class="form-group">
                                                            <label>No Handphone</label>
                                                            <input type="text" class="form-control" name="no_hp" id="no_hp" placeholder="Nomor Handphone" onkeypress="return hanyaAngka(event)">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="captain">
                                                <h4 class="info-text"> Silahkan lengkapi data asal instansi</h4>
                                                <div class="row">
                                                    <div class="col-sm-4 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label>Kategori Instansi Asal</label>
                                                            <select  class="form-control select-instansi" name="instansi" id="instansi">
                                                                <option></option>
                                                                <?php foreach ($instansi as $data) { ?>
                                                                    <option value="<?= $data->nama_kategori ?>"><?= $data->nama_kategori; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 ">
                                                        <div class="form-group">
                                                            <label>Alamat Instansi</label>
                                                            <input type="text" name="alamat_instansi" class="form-control" id="alamat_instansi" placeholder="Alamat Instansi" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-10 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label>Asal Instansi</label>
                                                            <input type="text" name="asal_instansi" class="form-control" id="asal_instansi" placeholder="contoh : UP PKB Cilincing Dinas Perhubungan Provinsi DKI Jakarta" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label>Sudah Punya No Registrasi ?</label>
                                                            <select name="type" id="type" class="form-control pilih-reg">
                                                                <option></option>
                                                                <option value="belum-punya">Belum Punya</option>
                                                                <option value="punya">Punya</option>
                                                            </select> 
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-10 col-sm-offset-1"></div>
                                                    <div class="menu-show">
                                                        <div class="row col-sm-offset-1">
                                                            <div class="col-sm-4 ">
                                                                <div class="form-group">
                                                                    <label>Provinsi</label>
                                                                    <select name="id_provinsi" id="id_provinsi" class="form-control select-provinsi">
                                                                        <option></option>
                                                                        <?php foreach ($provinsi as $data) { ?>
                                                                            <option value="<?= $data->id_provinsi; ?>"><?= $data->nama_provinsi; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                <div id="loadingImg">
                                                                    <img src="<?= base_url(); ?>assets/publik/img/images/loading-bubble.gif">
                                                                    <p><font face="raleway" size="2" color="red">Tunggu sebentar...</font></p>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 ">
                                                                <div class="form-group">
                                                                    <label>Kab / Kota</label>
                                                                    <select name="id_kota" id="id_kota" class="form-control select-kota">
                                                                    </select>
                                                                    <div id="loadingImg2">
                                                                        <img src="<?= base_url(); ?>assets/publik/img/images/loading-bubble.gif">
                                                                        <p><font face="raleway" size="2" color="red">Tunggu sebentar...</font></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row col-sm-offset-1">
                                                            <div class="col-sm-5">
                                                                <div class="form-group">
                                                                    <label>Jenjang</label>
                                                                    <select name="id_jenis_jenjang" class="form-control select-jenis-jenjang" id="id_jenis_jenjang">
                                                                        <option></option>
                                                                        <?php foreach ($jenisJenjang as $data) { ?>
                                                                            <option value="<?= $data->id; ?>" kode="<?= $data->kode; ?>"><?= $data->jenjang; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                <div id="loadingImg3">
                                                                    <img src="<?= base_url(); ?>assets/publik/img/images/loading-bubble.gif">
                                                                    <p><font face="raleway" size="2" color="red">Tunggu sebentar...</font></p>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-5 ">
                                                                <div class="form-group">
                                                                    <label>Tipe Status Kepegawaian</label>
                                                                    <select name="id_status_kepegawaian" class="form-control select-status-kepegawaian" id="id_status_kepegawaian">
                                                                        <option></option>
                                                                        <?php foreach ($statusKepegawaian as $data) { ?>
                                                                            <option value="<?= $data->id ?>" kode="<?= $data->kode; ?>"><?= $data->nama; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                <div id="loadingImg4">
                                                                    <img src="<?= base_url(); ?>assets/publik/img/images/loading-bubble.gif">
                                                                    <p><font face="raleway" size="2" color="red">Tunggu sebentar...</font></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2 col-sm-offset-1 ">
                                                            <div class="form-group">
                                                                <label></label>
                                                                <input type="text" class="form-control" id="show_kode_kota" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2 ">
                                                            <div class="form-group">
                                                                <label></label>
                                                                <input type="text" class="form-control" id="show_kode_jenjang" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2 ">
                                                            <div class="form-group">
                                                                <label></label>
                                                                <input type="text" class="form-control" id="show_kode_kepegawaian" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 ">
                                                            <div class="form-group">
                                                                <label></label>
                                                                <input type="text" class="form-control" id="kode" name="kode" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="col-sm-10 col-sm-offset-1">
                                                   <div class="form-group">
                                                   <label>Nomor Reg Penguji ( jika sudah memiliki )</label>
                                                   <input type="text" name="no_reg" class="form-control" id="no_reg" placeholder="Nomor Reg Penguji" autocomplete="off">
                                                   </div>
                                                   </div> -->
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="description">
                                                <div class="row">
                                                    <h4 class="info-text"> Silahkan lengkapi berkas file pendaftaran</h4>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label>KTP</label>
                                                            <input type="file" name="others[]" id="ktp" onchange="return cekMandatoryPdf('ktp')"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 jarak-label ">
                                                        <div class="form-group">
                                                            <h4><span class="label label-default">Max: 300 kb. Ekstensi .pdf</span></h4>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label>Sertifikat Jenjang Kopetensi</label>
                                                            <input type="file" name="others[]" id="sertifikat_kopetensi" onchange="return cekMandatoryPdf('sertifikat_kopetensi')"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 jarak-label ">
                                                        <div class="form-group">
                                                            <h4><span class="label label-default">Max: 300 kb. Ekstensi .pdf</span></h4>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label>STTPL</label>
                                                            <input type="file" name="others[]" id="sttpl" onchange="return cekMandatoryPdf('sttpl')"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 jarak-label ">
                                                        <div class="form-group">
                                                            <h4><span class="label label-default">Max: 300 kb. Ekstensi .pdf</span></h4>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label>Ijazah / SKL ( Surat Keterangan Lulus )</label>
                                                            <input type="file" name="others[]" id="ijazah" onchange="return cekMandatoryPdf('ijazah')"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 jarak-label ">
                                                        <div class="form-group">
                                                            <h4><span class="label label-default">Max: 300 kb. Ekstensi .pdf</span></h4>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label>SIM</label>
                                                            <input type="file" name="others[]" id="sim" onchange="return cekMandatoryPdf('sim')"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 jarak-label ">
                                                        <div class="form-group">
                                                            <h4><span class="label label-default">Max: 300 kb. Ekstensi .pdf</span></h4>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label>TTD Penguji</label>
                                                            <input type="file" name="others[]" id="ttd" onchange="return cekMandatoryPdf('ttd')"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 jarak-label ">
                                                        <div class="form-group">
                                                            <h4><span class="label label-default">Max: 300 kb. Ekstensi .pdf</span></h4>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label>Password</label>
                                                            <input type="password" class="form-control" placeholder="password" id="password-field" name="password" aria-describedby="sizing-addon2" value="<?= $dec_pass; ?>"><span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 ">
                                                        <div class="form-group">
                                                            <label>Konfirmasi Password</label>
                                                            <input type="password" class="form-control" placeholder="password" id="password-field2" name="password" aria-describedby="sizing-addon2" value="<?= $dec_pass; ?>"><span toggle="#password-field2" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <label>Foto Profil</label>
                                                            <div id="slider">
                                                                <img class="img-thumbnail" src="<?= base_url(); ?>/assets/publik/img/images/tidak-ada.jpg" width="150px" /></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 col-sm-offset-1">
                                                        <div class="form-group">
                                                            <input type="file"  name="photo" id="gambar" onchange="return foto_regis()"/></div>
                                                    </div>
                                                    <div class="col-sm-3 ">
                                                        <div class="form-group">
                                                            <h4><span class="label label-default">Max: 300 kb. Ekstensi .jpg | .png</span></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wizard-footer">
                                            <div class="pull-right" id="buka">
                                                <button type='button' class='btn btn-next btn-fill btn-info btn-wd btn-sm' name='next'> lanjut <i class="fa fa-arrow-right"></i></button>
                                                <button name="simpan" id="simpan" type="submit" class='btn btn-finish btn-fill btn-info btn-wd btn-sm'><i class="fa fa-save"></i> Daftar</button>
                                            </div>
                                            <div class="pull-right" id="btn_loading">
                                                <button name="simpan" id="simpan" type="submit" class='btn btn-finish btn-fill btn-info btn-wd btn-sm' disabled><i class='fa fa-refresh fa-spin'></i> Wait..</button>
                                            </div>
                                            <div class="pull-left">
                                                <button type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous'><i class="fa fa-arrow-left"></i> kembali</button>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </form>
                                </div>
                            </div> <!-- wizard container -->
                        </div>
                    </div> <!-- row -->
                </div> <!--  big container -->
                <div class="footer2">
                    <div class="container">
                        <div class="col-sm-10 col-sm-offset-1">
                            <div class="ui info message">
                                <div class="header"><center><h4>Informasi !!</h4></center></div><br>
                                <ul>
                                    <li>* Foto terbaru bagi ASN mengenakan Pakaian Penguji / PDH dan NON ASN mengenakan kemeja putih latar abu-abu.</li>
                                    <li>* Notifikasi pengaktifan akun akan dikirim melalui email ( <b>email yang di daftarkan</b> ).</li>
                                    <li>* Pastikan data sesuai dengan ketentuan yang telah tercantum di form registrasi karena data yang sesuai akan kami proses. </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <br><br>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $("#form-daftar").submit(function (e) {
                e.preventDefault();
                var error = 0;
                var message = "";

                if (error == 0) {
                    var nama_lengkap = $("#nama_lengkap").val();
                    var nama_lengkap = nama_lengkap.trim();
                    if (nama_lengkap.length == 0) {
                        error++;
                        message = "Nama wajib di isi.";
                    }
                }
                if (error == 0) {
                    var jkel = $("#jkel").val();
                    var jkel = jkel.trim();
                    if (jkel.length == 0) {
                        error++;
                        message = "Jenis Kelamin wajib di isi.";
                    }
                }
                if (error == 0) {
                    var status_pegawai = $("#status_pegawai").val();
                    var status_pegawai = status_pegawai.trim();
                    if (status_pegawai.length == 0) {
                        error++;
                        message = " Status Pegawai wajib di isi.";
                    }
                }
                if (error == 0) {
                    var tempat_lahir = $("#tempat_lahir").val();
                    var tempat_lahir = tempat_lahir.trim();
                    if (tempat_lahir.length == 0) {
                        error++;
                        message = " Tempat Lahir wajib di isi.";
                    }
                }
                if (error == 0) {
                    var tgl_lahir = $("#tgl_lahir").val();
                    var tgl_lahir = tgl_lahir.trim();
                    if (tgl_lahir.length == 0) {
                        error++;
                        message = " Tanggal Lahir wajib di isi.";
                    }
                }
                if (error == 0) {
                    var emails = $("#emails").val();
                    var emails = emails.trim();
                    if (emails.length == 0) {
                        error++;
                        message = " Email harus diisi !!";
                    } else if (!cekemail(emails)) {
                        error++;
                        message = "Format Email tidak sesuai (admin@gmail.com). ";
                    }
                }
                if (error == 0) {
                    var no_hp = $("#no_hp").val();
                    var no_hp = no_hp.trim();
                    if (no_hp.length == 0) {
                        error++;
                        message = "No Telpon wajib di isi.";
                    }
                }
                if (error == 0) {
                    var instansi = $("#instansi").val();
                    var instansi = instansi.trim();
                    if (instansi.length == 0) {
                        error++;
                        message = "Instansi wajib di isi.";
                    }
                }
                if (error == 0) {
                    var alamat_instansi = $("#alamat_instansi").val();
                    var alamat_instansi = alamat_instansi.trim();
                    if (alamat_instansi.length == 0) {
                        error++;
                        message = "Alamat Instansi wajib di isi.";
                    }
                }
                if (error == 0) {
                    var asal_instansi = $("#asal_instansi").val();
                    var asal_instansi = asal_instansi.trim();
                    if (asal_instansi.length == 0) {
                        error++;
                        message = "Asal Instansi wajib di isi.";
                    }
                }
                if (error == 0) {
                    var jenis_menu = $("#type").val();
                    var jenis_menu = jenis_menu.trim();
                    if (jenis_menu.length == 0) {
                        error++;
                        message = "Type No registrasi wajib di isi.";
                    }
                }
                if (error == 0) {
                    var provinsi = $("#id_provinsi").val();
                    var provinsi = provinsi.trim();
                    if (provinsi.length == 0) {
                        error++;
                        message = "Provinsi wajib di isi.";
                    }
                }
                if (error == 0) {
                    var kota = $("#id_kota").val();
                    var kota = kota.trim();
                    if (kota.length == 0) {
                        error++;
                        message = "Kota wajib di isi.";
                    }
                }
                if (error == 0) {
                    var id_jenis_jenjang = $("#id_jenis_jenjang").val();
                    var id_jenis_jenjang = id_jenis_jenjang.trim();
                    if (id_jenis_jenjang.length == 0) {
                        error++;
                        message = "Jenjang wajib di isi.";
                    }
                }
                if (error == 0) {
                    var id_status_kepegawaian = $("#id_status_kepegawaian").val();
                    var id_status_kepegawaian = id_status_kepegawaian.trim();
                    if (id_status_kepegawaian.length == 0) {
                        error++;
                        message = "Status Kepegawaian wajib di isi.";
                    }
                }
                if (error == 0) {
                    var ktp = $("#ktp").val();
                    var ktp = ktp.trim();
                    if (ktp.length == 0) {
                        error++;
                        message = "File KTP wajib di isi.";
                    }
                }
                if (error == 0) {
                    var sertifikat_kopetensi = $("#sertifikat_kopetensi").val();
                    var sertifikat_kopetensi = sertifikat_kopetensi.trim();
                    if (sertifikat_kopetensi.length == 0) {
                        error++;
                        message = "File Sertifikasi Kopetensi wajib di isi.";
                    }
                }
                if (error == 0) {
                    var sttpl = $("#sttpl").val();
                    var sttpl = sttpl.trim();
                    if (sttpl.length == 0) {
                        error++;
                        message = "File STTPL wajib di isi.";
                    }
                }
                if (error == 0) {
                    var ijazah = $("#ijazah").val();
                    var ijazah = ijazah.trim();
                    if (ijazah.length == 0) {
                        error++;
                        message = "File Ijazah / SKL wajib di isi.";
                    }
                }
                if (error == 0) {
                    var sim = $("#sim").val();
                    var sim = sim.trim();
                    if (sim.length == 0) {
                        error++;
                        message = "File SIM wajib di isi.";
                    }
                }
                if (error == 0) {
                    var ttd = $("#ttd").val();
                    var ttd = ttd.trim();
                    if (ttd.length == 0) {
                        error++;
                        message = "File Tanda Tangan Penguji wajib di isi.";
                    }
                }
                if (error == 0) {
                    var password = $("#password-field").val();
                    var password = password.trim();
                    if (password.length == 0) {
                        error++;
                        message = "Password wajib di isi.";
                    }
                }
                if (error == 0) {
                    var password2 = $("#password-field2").val();
                    var password2 = password2.trim();
                    if (password2.length == 0) {
                        error++;
                        message = "Kofirmasi Password wajib di isi.";
                    }
                }
                if (error == 0) {
                    var gambar = $("#gambar").val();
                    var gambar = gambar.trim();
                    if (gambar.length == 0) {
                        error++;
                        message = "Foto wajib di isi.";
                    }
                }
                if (error == 0) {
                    $(".confirm").attr('disabled', 'disabled');
                    $.ajax({
                        method: 'POST',
                        beforeSend: function () {
                            $("#buka").hide();
                            $("#btn_loading").show();
                        },
                        url: '<?= base_url('save-akun'); ?>',
                        type: "post",
                        data: new FormData(this),
                        processData: false,
                        contentType: false,
                        cache: false,
                    }).done(function (data) {
                        var result = jQuery.parseJSON(data);
                        if (result.status == true) {
                            document.getElementById("form-daftar").reset();
                            $('img.img-thumbnail').attr("src", "http://localhost/men-perhub//assets/publik/img/images/tidak-ada.jpg");
                            $('img.img-thumbnail').attr("width", "150px");
                            $("#buka").show();
                            $("#btn_loading").hide();
                            toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                                "closeButton": true});
                        } else {
                            $("#buka").show();
                            $("#btn_loading").hide();
                            toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                                "closeButton": true});
                        }
                    });
                } else {
                    toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                }
            });

            $("#loadingImg").hide();
            $('#id_provinsi').change(function () {
                var id_provinsi = $(this).val();
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg").show();
                    },
                    url: "<?= base_url(); ?>ajax-list-kota",
                    method: "POST",
                    data: {id_provinsi: id_provinsi},
                    async: false,
                    dataType: 'json',
                    success: function (data) {
                        var html = '';
                        var i;
                        html += '<option>Silahkan pilih Kab / Kota</option>';
                        for (i = 0; i < data.length; i++) {
                            html += '<option value="' + data[i].id_kota + '" kode="' + data[i].kode + '">' + data[i].nama_kota + '</option>';
                        }
                        $('#id_kota').html(html);
                        $("#loadingImg").fadeOut();
                    }
                });
            });

            $("#loadingImg2").hide();
            $('#id_kota').change(function () {
                $("#loadingImg2").show();
                var kode = $("#id_kota option:selected").attr("kode");
                $('#kode_kota').val(kode);
                $('#show_kode_kota').val(kode);
                $("#loadingImg2").fadeOut();
            });

            $("#loadingImg3").hide();
            $('#id_jenis_jenjang').change(function () {
                $("#loadingImg3").show();
                var kode = $("#id_jenis_jenjang option:selected").attr("kode");
                $('#kode_jenjang').val(kode);
                $('#show_kode_jenjang').val(kode);
                $("#loadingImg3").fadeOut();
            });

            $("#loadingImg4").hide();
            $('#id_status_kepegawaian').change(function () {
                $("#loadingImg4").show();
                var id_status_kepegawaian = $(this).val();
                var kode = $("#id_status_kepegawaian option:selected").attr("kode");
                $('#kode_kepegawaian').val(kode);
                $('#show_kode_kepegawaian').val(kode);
                $("#loadingImg4").fadeOut();
            });
            $("#loadingImg5").hide();

            jQuery(document).ready(function () {
                jQuery('#nama_lengkap').keyup(function ()
                {
                    var str = jQuery('#nama_lengkap').val();
                    var spart = str.split(" ");
                    for (var i = 0; i < spart.length; i++)
                    {
                        var j = spart[i].charAt(0).toUpperCase();
                        spart[i] = j + spart[i].substr(1);
                    }
                    jQuery('#nama_lengkap').val(spart.join(" "));

                });
            });

            $(".select-jenis-jenjang").select2({
                placeholder: "Pilih Jenjang",
                allowClear: true
            });
            $(".select-status-kepegawaian").select2({
                placeholder: "Pilih Status Kepegawaian",
                allowClear: true
            });
            $(".pilih-reg").select2({
                placeholder: "Silahkan Pilih",
                allowClear: true
            });
            $(".select-jkel").select2({
                placeholder: "Pilih Jenis Kelamin",
                allowClear: true
            });

            $(function () {
                $('.menu-show').hide();
                $('#type').change(function () {
                    var type = $('#type').val();
                    if (type.length > 0) {
                        $('.menu-show').show();
                        if (type == 'belum-punya') {
                            document.getElementById("kode").disabled = true;
                            $('#kode').val("Auto Generate");
                        } else if (type == 'punya') {
                            document.getElementById("kode").disabled = false;
                            $('#kode').val("");
                        }
                    } else {
                        $('.menu-show').hide();
                    }
                });
            });

            $(".select-pegawai").select2({
                placeholder: "Pilih status pegawai",
                allowClear: true
            });
            $(".select-instansi").select2({
                placeholder: "Pilih Instansi",
                allowClear: true
            });
            $(".select-provinsi").select2({
                placeholder: "Pilih Provinsi",
                allowClear: true
            });
            $(".select-kota").select2({
                placeholder: "Pilih Provinsi Dahulu",
                allowClear: true
            });
            $(".tgl_lahir").datepicker({
                orientation: "left",
                autoclose: !0,
                format: 'dd-mm-yyyy'
            });

            // untuk show hide password
            $(".toggle-password").click(function () {
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });

            function capitalize(textboxid, str) {
                // string with alteast one character
                if (str && str.length >= 1)
                {
                    var firstChar = str.charAt(0);
                    var remainingStr = str.slice(1);
                    str = firstChar.toUpperCase() + remainingStr;
                }
                document.getElementById(textboxid).value = str;
            }

            function hanyaAngka(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            //untuk foto registrasi
            function foto_regis() {
                var fileInput = document.getElementById('gambar');
                var filePath = fileInput.value;
                var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
                if (!allowedExtensions.exec(filePath)) {
                    toastr.error('maaf masukan gambar dengan format .jpeg/.jpg/.png only', 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                    fileInput.value = '';
                    return false;
                } else {
                    //Image preview
                    if (fileInput.files && fileInput.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            document.getElementById('slider').innerHTML = '<img class="img-thumbnail" src="' + e.target.result + '" width="150" height="auto"/>';
                        };
                        reader.readAsDataURL(fileInput.files[0]);
                    }
                }
                var ukuran = document.getElementById("gambar");
                if (ukuran.files[0].size > 307200)  // validasi ukuran size file
                {
                    // swal("Peringatan", "File harus maksimal 5MB", "warning");
                    toastr.error('File harus maksimal 300 kb', 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                    ukuran.value = '';
//                    setTimeout(location.reload.bind(location), 500);
                    return false;
                }
            }

            function cekMandatoryPdf(variable) {
                var fileInput = document.getElementById(variable).value;
                if (fileInput != '')
                {
                    var checkfile = fileInput.toLowerCase();
                    if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                        // swal("Peringatan", "File harus format .pdf", "warning");
                        toastr.error('File harus format .pdf', 'Warning', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                        document.getElementById(variable).value = '';
                        return false;
                    }
                    var ukuran = document.getElementById(variable);
                    if (ukuran.files[0].size > 307200)  // validasi ukuran size file
                    {
                        // swal("Peringatan", "File harus maksimal 5MB", "warning");
                        toastr.error('File harus maksimal 300 kb', 'Warning', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                        ukuran.value = '';
                        return false;
                    }
                    return true;
                }
            }

            function cekemail(a) {
                re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                return re.test(a);
            }

            $(function () {
                $('.pencet').hide();
                $("#check-all").click(function () {
                    if ($(this).is(":checked")) {
                        $(".check-item").prop("checked", true);
                        $('.pencet').fadeIn("slow");
                    } else {
                        $('.pencet').fadeOut("slow");
                        $(".check-item").prop("checked", false);
                    }
                });
            });

            $(function () {
                $('.pencet').hide();
                $(".check-item").click(function () {
                    if ($(this).is(":checked")) {
                        $('.pencet').fadeIn("slow");
                    } else {
                        $('.pencet').fadeOut("slow");
                    }
                });
            });
        </script>
        <!--   Core JS Files   -->
        <script src="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="<?= base_url(); ?>assets/publik/registrasi/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assets/publik/registrasi/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assets/publik/registrasi/js/gsdk-bootstrap-wizard.js"></script>
        <script src="<?= base_url(); ?>assets/publik/registrasi/js/jquery.validate.min.js"></script>
    </body>
</html>