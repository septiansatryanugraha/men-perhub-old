<!-- CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Raleway:500" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins:300" rel="stylesheet">
<!-- end CSS -->

<div class="bagan">
    <header class="popupHeader">
        <h3><i class="fa fa-copy"></i>  Notifikasi <span>Terbaru</span></h3>
        <p>Informasi Terbaru dan Terupdate</p>
    </header>
    <section class="popupBody">          
        <table>              
            <tbody>

                <tr>
                    <td class="title">Judul</td>
                    <td></td>
                    <td>
                        <?= $history->judul; ?>
                    </td>
                </tr>

                <tr>
                    <td class="title">Koten</td>
                    <td></td>
                    <td>
                        <?= $history->konten; ?>
                    </td>
                </tr>

            </tbody>
        </table>       
    </section>

    <div class="notice">
        Tolong teliti dan dibaca baik - baik akan informasi data diatas, agar tidak kesalahan informasi Terimakasih !
    </div>
</div>

<script>
    function goBack() {
        window.history.back();
    }
</script>

<style type="text/css">
    .bagan{width: 800px; }



    h3{
        margin: 0px !important;
        font-family: 'Poppins', sans-serif;
        color: #000;
    }

    h3 i{
        font-size: 22.5px;
        margin-right: 3px;
        color: #d32f2f;
    }

    h3 span{
        color: #d32f2f;
        font-weight: bold;
    }

    p{
        margin: 4px 0px 4px 0px !important;
        font-family: 'Raleway', sans-serif;
        font-size: 13.5px !important;
        color: #212121;
    }

    table > tbody > tr > td{
        padding: 4px 15px 4px 0px !important;
        border-top: transparent;
        font-family: 'Raleway', sans-serif;
        vertical-align: top;
        font-size: 13.5px !important;
        color: #212121;
    }

    .donlod{color: #388E3C !important;}

    .donlod:hover, .donlod:focus, .donlod:active{color: #2E7D32 !important;}

    .popupBody{margin: 10px !important;}

    .title{
        color: #6D4C41;
        font-weight: bold;
    }

    .kembali{
        border-radius: 2px !important;
        padding: 10px 16.5px !important;
        font-family: 'Raleway', sans-serif !important;
        font-size: 13px;
        margin: 15px 0px 12px 0px;
        background-color: #424242 !important;
        color: #fff !important;
        border:transparent;
        position: absolute;
    }

    .kembali:hover,
    .kembali:focus,
    .kembali:active{background-color: #212121 !important;}

    .kembali i{
        margin-right: 10px;
        font-size: 11.5px;
    }

    .notice{
        font-weight: bold;
        margin-top: 15px;
        font-family: 'Raleway', sans-serif;
        font-size: 13.5px !important;
        color: #d32f2f;
    }
</style>