<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <title><?= $title . " - E-Kompetensi"; ?></title>
        <link rel="icon" href="<?= base_url() ?>/assets/publik/img/images/logo-icon.png">

        <link rel="stylesheet" href="<?= base_url(); ?>assets/publik/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/publik/sm/matrix-style.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/eksternal/font-awesome.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/sweetalert/sweetalert.css">
        <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
        <link rel="stylesheet" href="<?= base_url(); ?>assets/publik/sm/apaja.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/publik/sm/dataTables.semanticui.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/publik/sm/semantic.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/super-treadmill/super-treadmill.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/popup/jquery.fancybox.css" media="screen" />

        <link rel="stylesheet" href="<?= base_url(); ?>assets/toastr/toastr.css">
        <script src="<?= base_url(); ?>assets/toastr/toastr.js"></script>
        <link rel="stylesheet" href="<?= base_url(); ?>assets/slider/css/gaya.css" type="text/css"/>

        <script src="<?= base_url(); ?>assets/publik/js/jquery-3.3.1.js"></script>
        <script src="<?= base_url(); ?>assets/publik/sm/jquery.min.js"></script>
        <script src="<?= base_url(); ?>assets/slider/js/responsiveslides.min.js"></script>
        <link rel="stylesheet" href="<?= base_url(); ?>assets/animasi/animasi.css" type="text/css"/>  

        <?php if (isset($style)) echo $style; ?>
    </head>
    <body>