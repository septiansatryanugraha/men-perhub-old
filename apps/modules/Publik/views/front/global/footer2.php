<div class="ui black inverted vertical footer segment">
    <div class="ui center aligned container">
        <div class="ui inverted section divider"></div>
        <div class="ui horizontal inverted small divided link list">
            <a class="item" href="<?= base_url() ?>" target="_blank">&copy; E-Kompetensi - Kementerian Perhubungan <?= date("Y"); ?></a>
        </div>
    </div>
</div>

<script src="<?= base_url(); ?>assets/js/ajax.js"></script>
<script src="<?= base_url(); ?>assets/animasi/wow.min.js"></script>
<script src="<?= base_url(); ?>assets/animasi/perhub.js"></script>

</body>
</html>