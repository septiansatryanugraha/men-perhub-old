<div class="ui black inverted vertical footer segment">
    <div class="ui center aligned container">
        <div class="ui inverted section divider"></div>
        <div class="ui horizontal inverted small divided link list">
            <a class="item" href="<?= base_url() ?>" target="_blank">&copy; E-Kompetensi - Kementerian Perhubungan <?= date("Y"); ?></a>
        </div>
    </div>
</div>


<script src="<?= base_url(); ?>assets/js/ajax.js"></script>
<script src="<?= base_url(); ?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?= base_url(); ?>assets/publik/sm/semantic.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/super-treadmill/super-treadmill.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/popup/jquery.fancybox.js"></script> 
<script src="<?= base_url(); ?>assets/animasi/wow.min.js"></script>
<script src="<?= base_url(); ?>assets/animasi/perhub.js"></script>
<script src="<?= base_url(); ?>assets/publik/sm/main.js"></script>
<script src="<?= base_url(); ?>assets/publik/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/publik/js/dataTables.semanticui.min.js"></script>

</body>
</html>