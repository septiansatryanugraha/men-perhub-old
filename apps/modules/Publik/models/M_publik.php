<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_publik extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_table()
    {
        $table = "tbl_user";
        return $table;
    }

    function selek_slider()
    {
        $sql = "SELECT * FROM tbl_slider WHERE deleted_date IS NULL";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selek_kontak()
    {
        $sql = " select * from tbl_kontak WHERE deleted_date IS NULL";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function selek_history()
    {
        $sql = " select * from tbl_broadcast WHERE deleted_date IS NULL";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function selek_by_id($id)
    {
        $sql = "SELECT * FROM tbl_broadcast WHERE deleted_date IS NULL AND id_broadcast ='{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    function login($field1, $field2, $select = '*')
    {
        $table = $this->get_table();
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where($field1);
        $this->db->where($field2);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows === 0) {
            return FALSE;
        } else {
            return $query->result();
        }
    }

    function update_user($id, $data2)
    {
        $this->db->where('email', $id);
        $this->db->update('tbl_login', $data2);

        return TRUE;
    }

    function update_user2($id, $data2)
    {
        $this->db->where('email', $id);
        $this->db->update('tbl_user', $data2);

        return TRUE;
    }

    public function checkUsername($data)
    {
        $this->db->select('email');
        $this->db->from('tbl_user');
        $this->db->where('email', $data['email']);
        $this->db->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
}
