<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends MX_Controller
{
    private $allowed_img_types = 'gif|jpg|png|jpeg|JPG|PNG|JPEG|pdf';

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_publik');
        $this->load->model('M_provinsi');
        $this->load->model('M_kota');
        $this->load->model('M_jenis_jenjang');
        $this->load->model('M_status_kepegawaian');
        $this->load->model('M_generate_code');
        $this->load->model('M_kategori_status');
    }

    public function index()
    {

        if ($this->lib->login() != "") {
            redirect('logout-opd');
        } else {
            $this->load->model('M_publik');
            $data = [
                'posting' => $this->M_publik->selek_history(),
                'slider' => $this->M_publik->selek_slider(),
                'kontak' => $this->M_publik->selek_kontak(),
                'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                'script_captcha' => $this->recaptcha->getScriptTag(),
                'title' => "Dashboard",
                'style' => '<style type="text/css">body {background-color: #FFFFFF;}.ui.menu .item img.logo {margin-right: 1em;}.main.container {margin-top: 7em;}.wireframe {margin-top: 2em;}.ui.footer.segment{}</style>'
            ];

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('Home', $data);
            $this->load->view('front/global/footer', $data);
        }
    }

    public function info($id = 0, $backbtn = 0)
    {
        $data = [
            'history' => $this->M_publik->selek_by_id($id),
        ];
        $data["backbutton"] = $backbtn;
        $this->load->view('detail', $data);
    }

    public function login_user()
    {

        $this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean');
        $this->form_validation->set_rules('g-recaptcha-response', '<strong>Captcha</strong>', 'callback_getResponseCaptcha');
        $recaptcha = $this->input->post('g-recaptcha-response');
        $response = $this->recaptcha->verifyResponse($recaptcha);

        if ($this->form_validation->run() == TRUE || !isset($response['success']) ||
            $response['success'] == true) {
            $email = $this->input->post('email');
            $password = base64_encode($this->input->post('password'));
            $check = $this->M_publik->login(['email' => $email], ['password' => $password]);

            if ($check == TRUE) {
                foreach ($check as $user) {
                    if ($user->status == "Non aktif") {
                        $json['pesan'] = "Maaf akun belum aktif.";
                    } else {
                        $this->session->set_userdata([
                            'email' => $user->email,
                            'id' => $user->id_user,
                            'id_jenjang' => $user->id_jenjang,
                            'nama_lengkap' => $user->nama_lengkap,
                            'status_kepegawaian' => $user->status_kepegawaian,
                            'nama' => $user->nama,
                            'password' => $user->password
                        ]);

                        $data2 = [
                            'last_login_user' => date('Y-m-d H:i:s')
                        ];

                        $this->M_publik->update_user($email, $data2);
                        $this->M_publik->update_user2($email, $data2);
                        $URL_home = base_url('beranda');
                        $json['status'] = true;
                        $json['url_home'] = $URL_home;
                        $json['pesan'] = "Success Login.";
                    }
                }
            } else {
                $json['pesan'] = "Username / Password salah";
            }
        } elseif (isset($recaptcha)) {
            $json['pesan'] = "Captcha harus di isi";
        } else {
            $json['pesan'] = "Maaf tidak bisa login, silahkan cek user dan password anda";
        }

        echo json_encode($json);
    }

    public function logout()
    {
        if ($this->lib->login() != "") {
            $this->lib->logout();
            redirect('homepage');
        } else {
            redirect('homepage');
        }
    }

    public function Register()
    {
        if ($this->lib->login() != "") {
            redirect('');
        } else {
            $data = [
                'title' => 'Daftar akun - E-Kopetensi',
                'jkel' => $this->M_kategori_status->select([], ['Pria', 'Wanita']),
                'status_pegawai' => $this->M_kategori_status->select([], ['PNS', 'P3K', 'NON ASN']),
                'instansi' => $this->M_kategori_status->select([], ['Dishub Provinsi', 'Dishub Kabupaten', 'Dishub Kota', 'Ditjen Perhubungan Darat', 'Lembaga Diklat']),
                'provinsi' => $this->M_provinsi->select(),
                'jenisJenjang' => $this->M_jenis_jenjang->select(),
                'statusKepegawaian' => $this->M_status_kepegawaian->select(),
                'style' => '<style type="text/css">body {background-color: #DADADA;}.ui.two.column.centered.grid {margin-top: 1px;}</style>'
            ];
            $this->load->view('front/global/header', $data);
            $this->load->view('Register');
            $this->load->view('front/global/footer2', $data);
        }
    }

    public function prosesDaftar()
    {
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $this->db->trans_begin();

        $idProvinsi = $this->input->post('id_provinsi');
        $idKota = $this->input->post('id_kota');
        $idJenisJenjang = $this->input->post('id_jenis_jenjang');
        $idStatusKepegawaian = $this->input->post('id_status_kepegawaian');
        $kode = $this->input->post('kode');
        $folder = $this->input->post('folder');

        $type = $this->input->post('type');
        $jkel = $this->input->post('jkel');
        $statusPegawai = $this->input->post('status_pegawai');
        $namaLengkap = trim($this->input->post('nama_lengkap'));
        $tempatLahir = $this->input->post('tempat_lahir');
        $tglLahir = $this->input->post('tgl_lahir');
        $noHp = $this->input->post('no_hp');
        $asalInstansi = $this->input->post('asal_instansi');
        $email = $this->input->post('emails');
        $instansi = $this->input->post('instansi');
        $alamatInstansi = $this->input->post('alamat_instansi');
        $password = $this->input->post('password');

        if ($errCode == 0) {
            if (strlen($namaLengkap) == 0) {
                $errCode++;
                $errMessage = "Nama Lengkap wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($jkel) == 0) {
                $errCode++;
                $errMessage = "Jenis Kelamin wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($statusPegawai) == 0) {
                $errCode++;
                $errMessage = "Status Kepegawaian wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tempatLahir) == 0) {
                $errCode++;
                $errMessage = "Tempat Lahir wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglLahir) == 0) {
                $errCode++;
                $errMessage = "Tanggal Lahir wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($email) == 0) {
                $errCode++;
                $errMessage = "Email wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errCode++;
                $errMessage = "Format Email salah.";
            }
        }
        if ($errCode == 0) {
            $checkuser = $this->M_publik->checkUsername(['email' => $email]);
            if ($checkuser) {
                $errCode++;
                $errMessage = "Akun email sudah terdaftar.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noHp) == 0) {
                $errCode++;
                $errMessage = "Nomor HP wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($instansi) == 0) {
                $errCode++;
                $errMessage = "Instansi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($alamatInstansi) == 0) {
                $errCode++;
                $errMessage = "Alamat Instansi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($asalInstansi) == 0) {
                $errCode++;
                $errMessage = "Alamat Instansi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idProvinsi) == 0) {
                $errCode++;
                $errMessage = "Provinsi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkProvinsi = $this->M_provinsi->selectById($idProvinsi);
            if ($checkProvinsi == null) {
                $errCode++;
                $errMessage = "Provinsi tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKota) == 0) {
                $errCode++;
                $errMessage = "Kota wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKota = $this->M_kota->selectById($idKota);
            if ($checkKota == null) {
                $errCode++;
                $errMessage = "Kota tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idJenisJenjang) == 0) {
                $errCode++;
                $errMessage = "Jenjang wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkJenisJenjang = $this->M_jenis_jenjang->selectById($idJenisJenjang);
            if ($checkJenisJenjang == null) {
                $errCode++;
                $errMessage = "Jenjang tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idStatusKepegawaian) == 0) {
                $errCode++;
                $errMessage = "Status Kepegawaian wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkStatusKepegawaian = $this->M_status_kepegawaian->selectById($idStatusKepegawaian);
            if ($checkStatusKepegawaian == null) {
                $errCode++;
                $errMessage = "Status Kepegawaian tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($type) == 0) {
                $errCode++;
                $errMessage = '"Sudah Punya No Registrasi" wajib di isi.';
            }
        }
        if ($errCode == 0) {
            if ($type == 'belum-punya') {
                $kode = $this->M_generate_code->getNextUser();
            }
            if (strlen($kode) == 0) {
                $errCode++;
                $errMessage = "Kode wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($password) == 0) {
                $errCode++;
                $errMessage = "Jenjang wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $folder = ['date' => date('Ymd'), 'code' => md5($kode)];

            $path['link'] = "upload/foto/" . $folder['code'] . "/";
            $upath = './' . $path['link'];
            if (!file_exists($upath)) {
                mkdir($upath, 0777);
            }

            $config['upload_path'] = $upath;
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = '2048'; //maksimum besar file 2M
            $config['file_name'] = 'file_' . time() . '_' . $_FILES['photo']['name'];
            // $config['overwrite'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->do_upload('photo');
            $photo = $this->upload->data();

            $KodeKota = $checkKota->kode;
            $KodeJenisJenjang = $checkJenisJenjang->kode;
            $KodeStatusKepegawaian = $checkStatusKepegawaian->kode;
            $NoReg = $KodeKota . '.' . $KodeJenisJenjang . '.' . $KodeStatusKepegawaian . '.' . $kode;

            try {
                $data = [
                    'id_status_kepegawaian' => $idStatusKepegawaian,
                    'id_kota' => $idKota,
                    'id_jenis_jenjang' => $idJenisJenjang,
                    'nama_lengkap' => $namaLengkap,
                    'jkel' => $jkel,
                    'tempat_lahir' => $tempatLahir,
                    'tgl_lahir' => date('Y-m-d', strtotime($tglLahir)),
                    'nomor_hp' => $noHp,
                    'asal_instansi' => $asalInstansi,
                    'email' => $email,
                    'instansi' => $instansi,
                    'alamat_instansi' => $alamatInstansi,
                    'status_pegawai' => $statusPegawai,
                    'no_reg' => $NoReg,
                    'kode_kota' => $KodeKota,
                    'kode_jenjang' => $KodeJenisJenjang,
                    'kode_kepegawaian' => $KodeStatusKepegawaian,
                    'kode_otomatis' => $kode,
                    'folder' => $folder['date'] . '/' . $folder['code'],
                    'password' => base64_encode($password),
                    'photo' => $path['link'] . '' . $photo['file_name'],
                    'status' => 'Non aktif',
                    'created_date' => $datetime,
                    'created_by' => 'Regist',
                    'updated_date' => $datetime,
                    'updated_by' => 'Regist',
                    'tanggal' => $date,
                ];
                $result = $this->db->insert('tbl_user', $data);

                $data2 = [
                    'nama' => $namaLengkap,
                    'email' => $email,
                    'status' => 'Non Aktif',
                    'tipe' => 'user',
                    'password' => base64_encode($this->input->post("password")),
                ];
                $result = $this->db->insert('tbl_login', $data2);

                $this->doUploadOthersImages($folder);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    private function doUploadOthersImages($folder = [])
    {
        $upath = './upload/berkas_registrasi/' . $folder['date'] . '/';
        if (!file_exists($upath)) {
            mkdir($upath, 0777);
        }
        $upath = './upload/berkas_registrasi/' . $folder['date'] . '/' . $folder['code'] . '/';
        if (!file_exists($upath)) {
            mkdir($upath, 0777);
        }

        $this->load->library('upload');

        $files = $_FILES;
        $cpt = count($_FILES['others']['name']);
        for ($i = 0; $i < $cpt; $i++) {
            unset($_FILES);
            $_FILES['others']['name'] = $files['others']['name'][$i];
            $_FILES['others']['type'] = $files['others']['type'][$i];
            $_FILES['others']['tmp_name'] = $files['others']['tmp_name'][$i];
            $_FILES['others']['error'] = $files['others']['error'][$i];
            $_FILES['others']['size'] = $files['others']['size'][$i];

            $this->upload->initialize([
                'upload_path' => $upath,
                'allowed_types' => $this->allowed_img_types
            ]);
            $this->upload->do_upload('others');
        }
    }

    public function kontak_saran()
    {
        if ($this->lib->login() != "") {
            redirect('');
        } else {
            $data = [
                'title' => 'Kontak Saran - E-Kopetensi',
                'style' => '<style type="text/css">body {background-color: #DADADA;}.ui.two.column.centered.grid {margin-top: 1px;}</style>'
            ];
            $this->load->view('front/global/header', $data);
            $this->load->view('kontak_saran');
            $this->load->view('front/global/footer2', $data);
        }
    }

    public function sendInbox()
    {

        $date = date('Y-m-d H:i:s');
        $this->db->trans_begin();

        $data = [
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email2'),
            'no_hp' => $this->input->post('no_hp'),
            'subject' => $this->input->post('subject'),
            'message' => $this->input->post('message'),
            'created_date' => $date,
        ];

        $dataEmail = $this->input->post('email');

        $config = [
            'useragent' => 'Codeigniter',
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => 587,
            'smtp_crypto' => 'tls',
            'smtp_timeout' => 100,
            'smtp_user' => 'angkasamuda20@gmail.com',
            'smtp_pass' => 'agkasamuda2020',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        ];

        $this->load->library('email');
        $this->email->initialize($config);

        $this->email->from($dataEmail);
        $this->email->to('angkasamuda20@gmail.com');
        $this->email->subject('Aktivasi Akun - E-Kopetensi System');

        $this->email->subject('Pesan Masuk Dari Website E-Kopetensi');
        $message = $this->load->view('email', $data, TRUE);
        $this->email->message($message);

        $result = $this->db->insert('tbl_inbox', $data);
        $result = $this->email->send();

        if ($this->db->trans_status() === FALSE) {
            $out = ['status' => false, 'pesan' => 'Maaf pesan gagal terkirim!'];
        }

        if ($result > 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Pesan berhasil terkirim'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => 'Maaf pesan gagal terkirim!'];
        }
        echo json_encode($out);
    }

    function getListKota()
    {
        $idProvinsi = $this->input->post('id_provinsi');
        $data = $this->M_kota->select(['id_provinsi' => $idProvinsi]);

        echo json_encode($data);
    }
}
