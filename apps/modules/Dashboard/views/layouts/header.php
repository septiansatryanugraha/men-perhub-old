<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <title><?= $this->config->item('website_title'); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="icon" href="<?= base_url() ?>/assets/tambahan/gambar/logo-dishub.png">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?= theme(); ?>/global/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/bootstrap-summernote/summernote.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/tambahan.css">
        <link href="<?= theme(); ?>/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= theme(); ?>/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url('admin-lte/plugins/datepicker/datepicker3.css') ?>">
        <link href="<?= theme(); ?>/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <script src="<?= base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <link rel="stylesheet" href="<?= base_url(); ?>admin-lte/plugins/toastr/toastr.css">
        <script src="<?= base_url(); ?>admin-lte/plugins/toastr/toastr.js"></script>
        <style type="text/css">
            .dataTables_processing {
                position: fixed;
                top: 0px;
                left: 50%;
                width: -100px;
                margin-left: -125px;
                border: 1px solid #ddd;
                text-align: center;
                color: #999;
                font-size: 11px;
                padding: 2px 0;
            }
            .merah {
                color:red;
            }
            .thumb1 { 
                background: url(blah.jpg) 50% 50% no-repeat; /* 50% 50% centers image in div */
                width: 150px;
                height: 150px;
            }
            /* Important part */
            .modal-dialog {
                overflow-y: initial !important
            }
            /*.modal-body{
                height: 485px;
                overflow-y: auto;
                }*/
            .jarak-pop {
                margin-left: -10px;
            }
            .pdf {
                color: red;
            }
            @media only screen and (min-width: 800px) {
                .table-responsive {
                    overflow: hidden;
                }
            }
            .control-sidebar {
                .tab-content {
                    height: calc(100vh - 135px);
                    overflow-y: scroll;
                    overflow-x: hidden;
                }
            }
            @media (min-width: 768px) {
                .control-sidebar {
                    .tab-content {
                        height: calc(100vh - 85px);
                    }
                }
            }
            /* START TOOLTIP STYLES */
            [tooltip] {
                position: relative; /* opinion 1 */
            }
            /* Applies to all tooltips */
            [tooltip]::before,
            [tooltip]::after {
                text-transform: none; /* opinion 2 */
                font-size: .9em; /* opinion 3 */
                line-height: 1;
                user-select: none;
                pointer-events: none;
                position: absolute;
                display: none;
                opacity: 0;
            }
            [tooltip]::before {
                content: '';
                border: 5px solid transparent; /* opinion 4 */
                z-index: 1001; /* absurdity 1 */
            }
            [tooltip]::after {
                content: attr(tooltip); /* magic! */
                /* most of the rest of this is opinion */
                font-family: Helvetica, sans-serif;
                text-align: center;
                /* 
                  Let the content set the size of the tooltips 
                  but this will also keep them from being obnoxious
                */
                min-width: 3em;
                max-width: 21em;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
                padding: 1ch 1.5ch;
                border-radius: .3ch;
                box-shadow: 0 1em 2em -.5em rgba(0, 0, 0, 0.35);
                background: #333;
                color: #fff;
                z-index: 1000; /* absurdity 2 */
            }
            /* Make the tooltips respond to hover */
            [tooltip]:hover::before,
            [tooltip]:hover::after {
                display: block;
            }
            /* don't show empty tooltips */
            [tooltip='']::before,
            [tooltip='']::after {
                display: none !important;
            }
            /* FLOW: UP */
            [tooltip]:not([flow])::before,
            [tooltip][flow^="up"]::before {
                bottom: 150%;
                border-bottom-width: 0;
                border-top-color: #333;
            }
            [tooltip]:not([flow])::after,
            [tooltip][flow^="up"]::after {
                bottom: calc(150% + 5px);
            }
            [tooltip]:not([flow])::before,
            [tooltip]:not([flow])::after,
            [tooltip][flow^="up"]::before,
            [tooltip][flow^="up"]::after {
                left: 50%;
                transform: translate(-50%, -.5em);
            }
            /* KEYFRAMES */
            @keyframes tooltips-vert {
                to {
                    opacity: .9;
                    transform: translate(-50%, 0);
                }
            }
            @keyframes tooltips-horz {
                to {
                    opacity: .9;
                    transform: translate(0, -50%);
                }
            }
            /* FX All The Things */ 
            [tooltip]:not([flow]):hover::before,
            [tooltip]:not([flow]):hover::after,
            [tooltip][flow^="up"]:hover::before,
            [tooltip][flow^="up"]:hover::after,
            [tooltip][flow^="down"]:hover::before,
            [tooltip][flow^="down"]:hover::after {
                animation: tooltips-vert 300ms ease-out forwards;
            }
            [tooltip][flow^="left"]:hover::before,
            [tooltip][flow^="left"]:hover::after,
            [tooltip][flow^="right"]:hover::before,
            [tooltip][flow^="right"]:hover::after {
                animation: tooltips-horz 300ms ease-out forwards;
            }
            .jarak-logo-utama {
                margin-left: 45px;
                margin-top: -50px;
            }
            .jarak-logo {
                margin-top: -15px;
                margin-left: 55px;
            }
            .jarak-logo2 {
                margin-top: -17px;
                margin-left: 55px;
            }
            #spinner {
                position:fixed;
                width: 90px;
                height: 90px;
                display: inline-block;
                margin-top: 20%;
                opacity: 0.8;
                margin-left: 50%;
                background: black;
                padding: 10px;
                z-index: 9999;
                border-radius: 10px;
            }
            #spinner div {
                width: 6%;
                height: 16%;
                background: #FFF;
                position: absolute;
                left: 49%;
                top: 31%;
                opacity: 0;
                -webkit-border-radius: 50px;
                -webkit-box-shadow: 0 0 3px rgba(0,0,0,0.2);
                -webkit-animation: fade 1s linear infinite;
            }
            @-webkit-keyframes fade {
                from {opacity: 1;}
                to {opacity: 0.25;}
            }
            #spinner div.bar1 {
                -webkit-transform:rotate(0deg) translate(0, -130%);
                -webkit-animation-delay: 0s;
            }    
            #spinner div.bar2 {
                -webkit-transform:rotate(30deg) translate(0, -130%); 
                -webkit-animation-delay: -0.9167s;
            }
            #spinner div.bar3 {
                -webkit-transform:rotate(60deg) translate(0, -130%); 
                -webkit-animation-delay: -0.833s;
            }
            #spinner div.bar4 {
                -webkit-transform:rotate(90deg) translate(0, -130%); 
                -webkit-animation-delay: -0.7497s;
            }
            #spinner div.bar5 {
                -webkit-transform:rotate(120deg) translate(0, -130%); 
                -webkit-animation-delay: -0.667s;
            }
            #spinner div.bar6 {
                -webkit-transform:rotate(150deg) translate(0, -130%); 
                -webkit-animation-delay: -0.5837s;
            }
            #spinner div.bar7 {
                -webkit-transform:rotate(180deg) translate(0, -130%); 
                -webkit-animation-delay: -0.5s;
            }
            #spinner div.bar8 {
                -webkit-transform:rotate(210deg) translate(0, -130%); 
                -webkit-animation-delay: -0.4167s;
            }
            #spinner div.bar9 {
                -webkit-transform:rotate(240deg) translate(0, -130%); 
                -webkit-animation-delay: -0.333s;
            }
            #spinner div.bar10 {
                -webkit-transform:rotate(270deg) translate(0, -130%); 
                -webkit-animation-delay: -0.2497s;
            }
            #spinner div.bar11 {
                -webkit-transform:rotate(300deg) translate(0, -130%); 
                -webkit-animation-delay: -0.167s;
            }
            #spinner div.bar12 {
                -webkit-transform:rotate(330deg) translate(0, -130%); 
                -webkit-animation-delay: -0.0833s;
            }
            .tulisan {
                margin-top: 55px;
                margin-left: 4px;
                position: absolute;
                color:white;
                font-family: verdana;
                -webkit-box-shadow: 0 0 3px rgba(0,0,0,0.2);
                -webkit-animation: fade 1s linear infinite;
            }
        </style>
    <div id="spinner" style="display: none">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
        <div class="bar4"></div>
        <div class="bar5"></div>
        <div class="bar6"></div>
        <div class="bar7"></div>
        <div class="bar8"></div>
        <div class="bar9"></div>
        <div class="bar10"></div>
        <div class="bar11"></div>
        <div class="bar12"></div>
        <p class="tulisan">Loading...</p>
    </div>
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?= theme(); ?>/global/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="<?= theme(); ?>/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?= theme(); ?>/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="<?= theme(); ?>/global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?= theme(); ?>/assets/css/layout.css" rel="stylesheet" type="text/css"/>
    <link id="style_color" href="<?= theme(); ?>/assets/css/themes/darkblue.css" rel="stylesheet" type="text/css"/>
    <link href="<?= theme(); ?>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?= base_url(); ?>assets/icon-picker/fontawesome5/css/all.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/icon-picker/fontawesome-iconpicker.css">
    <script src="<?= theme(); ?>/global/plugins/jquery.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/sweetalert/sweetalert.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/super-treadmill/super-treadmill2.css" type="text/css" />
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a class="ajaxify" href="<?= site_url('Dashboard'); ?>">
                    <img src="<?= $this->config->item('app_logo') ?>" height="40px" alt="logo" class="logo-default"/>
                </a>
                <div class="menu-toggler sidebar-toggler"></div>
            </div>
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"></a>
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown dropdown-extended dropdown-inbox" data-toggle="tooltip" data-placement="bottom" title="Notifikasi Inbox">
                        <a href="javascript:;" class="dropdown-toggle hokya4" data-toggle="dropdown" >
                            <i class="icon-envelope-open"></i>
                            <span class="badge badge-default count4"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3>Notifikasi <span class="bold">Kontak Pesan Website</span> </h3>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller menu4" style="height: 275px;" data-handle-color="#637283"></ul>
                            </li>
                        </ul>
                    </li>

                    <?php
                    $this->load->model('M_sidebar');
                    $accessView1 = $this->M_sidebar->access('view', 'master-perpanjangan');
                    $accessViewPerpanjangan = $accessView1->menuview;
                    $accessView2 = $this->M_sidebar->access('view', 'master-diklat');
                    $accessViewDiklat = $accessView2 > menuview;
                    $accessView3 = $this->M_sidebar->access('view', 'master-jenjang');
                    $accessViewJenjang = $accessView3->menuview;
                    if ($accessViewPerpanjangan > 0) {

                        ?>
                        <li class="dropdown dropdown-extended dropdown-inbox" data-toggle="tooltip" data-placement="bottom" title="Notifikasi Perpanjangan">
                            <a href="javascript:;" class="dropdown-toggle hokya" data-toggle="dropdown" >
                                <i class="icon-bell"></i>
                                <span class="badge badge-default count1"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="external">
                                    <h3>Notifikasi <span class="bold">Pengajuan Perpanjangan</span> </h3>
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list scroller menu" style="height: 275px;" data-handle-color="#637283"></ul>
                                </li>
                            </ul>
                        </li>
                        <?php
                    }
                    if ($accessViewDiklat > 0) {

                        ?>
                        <li class="dropdown dropdown-extended dropdown-inbox" data-toggle="tooltip" data-placement="bottom" title="Notifikasi Pengajuan Diklat">
                            <a href="javascript:;" class="dropdown-toggle hokya2" data-toggle="dropdown">
                                <i class="icon-bell"></i>
                                <span class="badge badge-default count2"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="external">
                                    <h3>Notifikasi <span class="bold">Pengajuan Diklat PKB</span> </h3>
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list scroller menu2" style="height: 275px;" data-handle-color="#637283"></ul>
                                </li>
                            </ul>
                        </li>
                        <?php
                    }
                    if ($accessViewJenjang > 0) {

                        ?>
                        <li class="dropdown dropdown-extended dropdown-inbox" data-toggle="tooltip" data-placement="bottom" title="Notifikasi Kenaikan Jenjang">
                            <a href="javascript:;" class="dropdown-toggle hokya3" data-toggle="dropdown">
                                <i class="icon-bell"></i>
                                <span class="badge badge-default count3"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="external">
                                    <h3>Notifikasi <span class="bold">Pengajuan Kenaikan Jenjang</span> </h3>
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list scroller menu3" style="height: 275px;" data-handle-color="#637283"></ul>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" class="img-circle" src="<?= site_url(); ?>upload/user/<?= $this->session->userdata('foto') ?>">
                            <span class="username username-hide-on-mobile">
                                <?= $this->session->userdata('nama') ?></span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a class="ajaxify" href="<?= site_url('profile'); ?>"><i class="icon-user"></i> My Profile </a>
                            </li>           
                            <li class="divider"></li>           
                            <li>
                                <a href="<?= site_url('Default/Auth/logout'); ?>"><i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <!--  <div id ="loading2"></div> -->
            <div class="page-sidebar navbar-collapse collapse">
                <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                    <form class="sidebar-search  " action="#" method="POST">
                        <a href="javascript:;" class="remove">
                            <i class="icon-close"></i>
                        </a>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit">
                                    <i class="icon-magnifier"></i>
                                </a>
                            </span>
                        </div>
                    </form>
                    <?php
                    $this->load->model('M_sidebar');
                    $menu = $this->M_sidebar->left_menu();
                    if ($menu->num_rows() > 0) {
                        foreach ($menu->result() as $row) {
                            $menu_child = $this->M_sidebar->left_menu_child($row->id_menu);
                            $active = ($this->uri->segment(1) == $row->link) ? 'active' : '';
                            $open = ($this->uri->segment(1) == $row->link) ? 'open' : '';
                            $access = $this->M_sidebar->access('view', $row->kode_menu);

                            if ($access->menuview == 1) {
                                if ($menu_child->num_rows() > 0) {
                                    echo "
                        <li class = '" . $active . "'>
                        <a href='javascript:;'>
                        <i class='" . $row->icon . "'></i>
                        <span class='title'>" . $row->nama_menu . "</span>
                        <span class='selected'></span>
                        <span class='arrow " . $open . "'></span>
                        </a>
                        ";
                                    echo "<ul class='sub-menu'>";
                                    if ($menu_child->num_rows() > 0) {
                                        foreach ($menu_child->result() as $obj) {
                                            $access_child = $this->M_sidebar->access('view', $obj->kode_menu);
                                            if ($access_child->menuview == 1) {
                                                echo "
                              <li>
                              <a class='ajaxify' href='" . site_url($obj->link) . "'>
                              <i class='" . $obj->icon . "'></i>
                              " . $obj->nama_menu . "
                              </a>
                              </li>
                              ";
                                            }
                                        }
                                    }
                                    echo "</ul>";
                                    echo "
                        </li>
                        ";
                                } else {
                                    echo "
                        <li class = '" . $active . "'>
                        <a class='ajaxify' href='" . site_url($row->link) . "'>
                        <i class='" . $row->icon . "'></i>
                        <span class='title'>" . $row->nama_menu . "</span>
                        </a>
                        </li>
                        ";
                                }
                            }
                        }
                    }

                    ?>
                </ul>
                <!-- END SIDEBAR MENU -->
            </div>
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <script>
                $(document).ready(function () {
                    function load_unseen_notification(view = '') {
                        $.ajax({
                            url: "<?= base_url('notif-perpanjangan'); ?>",
                            method: "POST",
                            data: {view: view},
                            dataType: "json",
                            success: function (data) {
                                $('.menu').html(data.notification);
                                if (data.unseen_notification > 0) {
                                    $('.count1').html(data.unseen_notification);
                                }
                            }
                        });
                    }
                    load_unseen_notification();
                    $(document).on('click', '.hokya', function () {
                        $('.count1').html('');
                        load_unseen_notification('yes');
                    });
                    setInterval(function () {
                        load_unseen_notification();
                    }, 7000);
                });

                $(document).ready(function () {
                    function load_unseen_notification2(view = '') {
                        $.ajax({
                            url: "<?= base_url('notif-diklat'); ?>",
                            method: "POST",
                            data: {view: view},
                            dataType: "json",
                            success: function (data) {
                                $('.menu2').html(data.notification);
                                if (data.unseen_notification > 0) {
                                    $('.count2').html(data.unseen_notification);
                                }
                            }
                        });
                    }
                    load_unseen_notification2();
                    $(document).on('click', '.hokya2', function () {
                        $('.count2').html('');
                        load_unseen_notification2('yes');
                    });
                    setInterval(function () {
                        load_unseen_notification2();
                    }, 7000);
                });

                $(document).ready(function () {
                    function load_unseen_notification3(view = '') {
                        $.ajax({
                            url: "<?= base_url('notif-jenjang'); ?>",
                            method: "POST",
                            data: {view: view},
                            dataType: "json",
                            success: function (data) {
                                $('.menu3').html(data.notification);
                                if (data.unseen_notification > 0) {
                                    $('.count3').html(data.unseen_notification);
                                }
                            }
                        });
                    }
                    load_unseen_notification3();
                    $(document).on('click', '.hokya3', function () {
                        $('.count3').html('');
                        load_unseen_notification3('yes');
                    });
                    setInterval(function () {
                        load_unseen_notification3();
                    }, 7000);
                });

                $(document).ready(function () {
                    function load_unseen_notification4(view = '') {
                        $.ajax({
                            url: "<?= base_url('notif-inbox'); ?>",
                            method: "POST",
                            data: {view: view},
                            dataType: "json",
                            success: function (data) {
                                $('.menu4').html(data.notification);
                                if (data.unseen_notification > 0) {
                                    $('.count4').html(data.unseen_notification);
                                }
                            }
                        });
                    }
                    load_unseen_notification4();
                    $(document).on('click', '.hokya4', function () {
                        $('.count4').html('');
                        load_unseen_notification4('yes');
                    });
                    setInterval(function () {
                        load_unseen_notification4();
                    }, 7000);
                });

                $(document).ready(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            </script>



