<link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet'>
<?php $this->load->view('_heading/loader') ?>
<style type="text/css">
    .count {
        font-size: 35px;
    }
    .jam {
        margin-top: 10px;
        font-size: 12px;
    }
    .treadmill {
        overflow: hidden;
        /* border: 1px solid #26a69a;*/
        font-family: 'Raleway';
        background-color:white;
        padding:0 5px;
    }
    .treadmill-unit a:hover {
        text-decoration: none;
    }
    .treadmill-unit {
        overflow: hidden;
        position: relative;
    }
</style>
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="#">Home</a>            
            </li>
            <?= (isset($breadcrumb)) ? $breadcrumb : ''; ?>
        </ul>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                <div class="visual">
                    <i class="fa fa-file-zip-o"></i></div>
                <div class="details">
                    <div class="number">
                        <span class="count"><?= $total_perpanjangan ?></span>
                    </div>
                    <div class="desc"> Perpanjangan Sertifikasi </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                <div class="visual">
                    <i class="fa fa-file-zip-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span class="count"><?= $total_diklat ?></span></div>
                    <div class="desc"> Pengajuan Diklat PKB </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                <div class="visual"><i class="fa fa-file-zip-o"></i></div>
                <div class="details"><div class="number">
                        <span class="count"><?= $total_jenjang ?></span></div>
                    <div class="desc"> Pengajuan Kenaikan Jenjang </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                <div class="visual">
                    <i class="fa fa-users"></i>
                </div>
                <div class="details"><div class="number">
                        <span class="count"><?= $total_jenjang ?></span></div>
                    <div class="desc"> User Penguji </div>
                </div>
            </a>
        </div>
        <?php if ($accessView1 > 0) { ?>
            <div class="col-md-4">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-history"></i> Data Perpanjangan Terbaru
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="mytreadmill1" class="treadmill list"> 
                            <?php
                            if (!empty($perpanjangan)) {
                                foreach ($perpanjangan as $key) {
                                    echo "<div class='treadmill-unit'>";
                                    echo anchor('edit-perpanjangan/' . $key->id_perpanjangan, "
    <small class='pull-right jam'> &nbsp;" . date('d-m-Y', strtotime($key->created_date)) . "</i>&nbsp; </small> 
    <h5><i class='fa fa-info-circle'></i> " . $key->kode_pengajuan . "</h5> 
    <p> Pengajuan Perpanjangan : " . $key->jenis_perpanjangan . "</p>
    <p> Status <b>: " . $key->status_perpanjangan . "</b></p>
    <p> User : " . $key->created_by . "</p>", " class='klik ajaxify'");
                                    echo "<hr>";
                                    echo "</div>";
                                }

                                ?>
                            <?php } else { ?>
                                <?= 'Belum ada data Pengajuan' ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if ($accessView2 > 0) { ?>
            <div class="col-md-4">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-history"></i> Data Diklat PKB Terbaru
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="mytreadmill2" class="treadmill list"> 
                            <?php
                            if (!empty($diklat_pkb)) {
                                foreach ($diklat_pkb as $key) {
                                    echo "<div class='treadmill-unit'>";
                                    echo anchor('edit-diklat/' . $key->id_diklat, "
    <small class='pull-right jam'> &nbsp;" . date('d-m-Y', strtotime($key->created_date)) . "</i>&nbsp; </small>
    <h5><i class='fa fa-info-circle'></i> " . $key->kode_pengajuan . "</h5> 
    <p> Pengajuan Diklat : " . $key->jenis_diklat . "</p>
    <p> Status : <b>" . $key->status_pengajuan . "</b></p>
    <p> User : " . $key->created_by . "</p>", " class='klik ajaxify'");
                                    echo "<hr>";
                                    echo "</div>";
                                }

                                ?>
                            <?php } else { ?>
                                <?= 'Belum ada data Pengajuan' ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if ($accessView3 > 0) { ?>
            <div class="col-md-4">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-history"></i> Data Kenaikan Jenjang Terbaru
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="mytreadmill3" class="treadmill list"> 
                            <?php
                            if (!empty($jenjang)) {
                                foreach ($jenjang as $key) {
                                    echo "<div class='treadmill-unit'>";
                                    echo anchor('edit-jenjang/' . $key->id_jenjang, "
    <small class='pull-right jam'> &nbsp;" . date('d-m-Y', strtotime($key->created_date)) . "</i>&nbsp; </small>
    <h5><i class='fa fa-info-circle'></i> " . $key->kode_pengajuan . "</h5> 
    <p> Pengajuan Jenjang : " . $key->jenis_jenjang . "</p> 
    <p> Status : <b>" . $key->status_pengajuan . "</b></p>
    <p> User : " . $key->created_by . "</p>", " class='klik ajaxify'");
                                    echo "<hr>";
                                    echo "</div>";
                                }

                                ?>
                            <?php } else { ?>
                                <?= 'Belum ada data Pengajuan' ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTENT-->

<script type="text/javascript">
    $(document).ready(function () {
        $('#mytreadmill1').startTreadmill({
            runAfterPageLoad: true,
            direction: "down",
            speed: "slow",
            viewable: 5,
            pause: false
        });

        $('#mytreadmill2').startTreadmill({
            runAfterPageLoad: true,
            direction: "down",
            speed: "slow",
            viewable: 5,
            pause: false
        });

        $('#mytreadmill3').startTreadmill({
            runAfterPageLoad: true,
            direction: "down",
            speed: "slow",
            viewable: 5,
            pause: false
        });
    });

    // Animasi angka bergerak dashboard
    $('.count').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 1000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
</script>