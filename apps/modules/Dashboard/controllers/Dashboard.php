<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends AUTH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_total');
        $this->load->model('M_sidebar');
    }

    public function index()
    {
        $accessView1 = $this->M_sidebar->access('view', 'master-perpanjangan');
        $data['accessView1'] = $accessView1->menuview;
        $accessView2 = $this->M_sidebar->access('view', 'master-diklat');
        $data['accessView2'] = $accessView2->menuview;
        $accessView3 = $this->M_sidebar->access('view', 'master-jenjang');
        $data['accessView3'] = $accessView3->menuview;

        $data['title'] = 'Dashboard';
        $data['subtitle'] = '';
        $data['total_perpanjangan'] = $this->M_total->totalPerpanjangan();
        $data['total_diklat'] = $this->M_total->totalDiklat();
        $data['total_jenjang'] = $this->M_total->totalJenjang();
        $data['total_user'] = $this->M_total->totalUser();
        $data['perpanjangan'] = $this->M_total->getDataPerpanjangan();
        $data['diklat_pkb'] = $this->M_total->getDataDiklat();
        $data['jenjang'] = $this->M_total->getDataJenjang();

        $this->loadkonten('dashboard', $data);
    }

    public function notifPerpanjangan()
    {
        $view = $this->input->post('view');
        if ($view != '') {
            $updateStatus = $this->M_total->updateStatusPerpanjangan();
        }
        $selectNotif = $this->M_total->selectNotifPerpanjangan();

        if (!empty($selectNotif)) {
            foreach ($selectNotif as $data) {
                $output .= '
    	<li><a class="ajaxify klik" href="' . site_url() . 'master-perpanjangan">Kode :
        ' . $data->kode_pengajuan . '<span class="photo"><img src="' . base_url() . $data->photo . '" class="img-circle" alt=""> </span>
        <span class="subject">
        <span class="from">' . $data->user . '</span>
        </span>
        <span class="message">Melakukan pengajuan perpanjangan sertifikasi jenjang
        <b> ' . $data->jenis_perpanjangan . ' </b></span></a>
        </li>';
            }
        } else {
            $output .= '<li><a href="#"><i class="fa fa-remove text-red"></i> tidak ada notifikasi</a></li>';
        }

        $count = $this->M_total->totalCountPerpanjangan();
        $data = [
            'notification' => $output,
            'unseen_notification' => $count
        ];

        echo json_encode($data);
    }

    public function notifDiklat()
    {
        $view = $this->input->post('view');
        if ($view != '') {
            $updateStatus = $this->M_total->updateStatusDiklat();
        }
        $selectNotif = $this->M_total->selectNotifDiklat();

        if (!empty($selectNotif)) {
            foreach ($selectNotif as $data) {
                $output .= '
    	<li><a class="ajaxify" href="' . site_url() . 'master-diklat">Kode pengajuan :
        ' . $data->kode_pengajuan . '<span class="photo"><img src="' . base_url() . $data->photo . '" class="img-circle" alt=""> </span>
        <span class="subject">
        <span class="from">' . $data->user . '</span>
        </span>
        <span class="message">Melakukan pengajuan mengikuti jenis diklat
        <b> ' . $data->jenis_diklat . ' </b></span></a>
        </li>';
            }
        } else {
            $output .= '<li><a href="#"><i class="fa fa-remove text-red"></i> tidak ada notifikasi</a></li>';
        }

        $count = $this->M_total->totalCountDiklat();
        $data = [
            'notification' => $output,
            'unseen_notification' => $count
        ];

        echo json_encode($data);
    }

    public function notifJenjang()
    {
        $view = $this->input->post('view');
        if ($view != '') {
            $updateStatus = $this->M_total->updateStatusJenjang();
        }
        $selectNotif = $this->M_total->selectNotifJenjang();

        if (!empty($selectNotif)) {
            foreach ($selectNotif as $data) {
                $output .= '
    	<li><a class="ajaxify" href="' . site_url() . 'master-jenjang">Kode pengajuan :
        ' . $data->kode_pengajuan . '<span class="photo"><img src="' . base_url() . $data->photo . '" class="img-circle" alt=""> </span>
        <span class="subject">
        <span class="from">' . $data->user . '</span>
        </span>
        <span class="message">Melakukan pengajuan mengikuti kenaikan jenjang 
        <b> ' . $data->jenis_jenjang . ' </b></span></a>
        </li>';
            }
        } else {
            $output .= '<li><a href="#"><i class="fa fa-remove text-red"></i> tidak ada notifikasi</a>
                  		</li>';
        }

        $count = $this->M_total->totalCountJenjang();
        $data = [
            'notification' => $output,
            'unseen_notification' => $count
        ];

        echo json_encode($data);
    }

    public function notifInbox()
    {
        $view = $this->input->post('view');
        if ($view != '') {
            $updateStatus = $this->M_total->updateStatusInbox();
        }
        $selectNotif = $this->M_total->selectNotifInbox();

        if (!empty($selectNotif)) {
            foreach ($selectNotif as $data) {
                $output .= '
    	<li><a class="ajaxify" href="' . site_url() . 'inbox">
       
        <span class="from"><b>' . $data->email . '</b></span><br>
        <span class="from"><b>' . $data->subject . '</b></span><br>
        <span class="from"> ' . $data->message . '</span></a>
        </li>';
            }
        } else {
            $output .= '<li><a href="#"><i class="fa fa-remove text-red"></i> tidak ada notifikasi</a></li>';
        }

        $count = $this->M_total->totalCountInbox();
        $data = [
            'notification' => $output,
            'unseen_notification' => $count
        ];

        echo json_encode($data);
    }
}
