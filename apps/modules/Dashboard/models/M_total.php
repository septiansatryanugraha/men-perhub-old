<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_total extends CI_Model
{

    public function totalPerpanjangan()
    {
        $this->db->from('tbl_perpanjangan');
        $this->db->where('deleted_date IS NULL');
        $data = $this->db->get();

        return $data->num_rows();
    }

    public function totalDiklat()
    {
        $this->db->from('tbl_diklat');
        $this->db->where('deleted_date IS NULL');
        $data = $this->db->get();

        return $data->num_rows();
    }

    public function totalJenjang()
    {
        $this->db->from('tbl_kenaikan_jenjang');
        $this->db->where('deleted_date IS NULL');
        $data = $this->db->get();

        return $data->num_rows();
    }

    public function totalUser()
    {
        $this->db->from('tbl_user');
        $this->db->where('deleted_date IS NULL');
        $data = $this->db->get();

        return $data->num_rows();
    }

    function getDataPerpanjangan()
    {
        $sql = "SELECT * FROM tbl_perpanjangan WHERE deleted_date IS NULL ORDER BY id_perpanjangan DESC";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function getDataDiklat()
    {
        $sql = "SELECT * FROM tbl_diklat WHERE deleted_date IS NULL ORDER BY id_diklat DESC";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function getDataJenjang()
    {
        $sql = "SELECT * FROM tbl_kenaikan_jenjang WHERE deleted_date IS NULL ORDER BY id_jenjang DESC";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function updateStatusPerpanjangan()
    {
        $updateQuery = "UPDATE tbl_perpanjangan SET status_baca=1 WHERE status_baca=0 ";
        $this->db->query($updateQuery);

        return $this->db->affected_rows();
    }

    function selectNotifPerpanjangan()
    {
        $sql = "SELECT tbl_user.photo as photo,tbl_user.nama_lengkap as user,kode_pengajuan,jenis_jenjang.jenjang as jenis_perpanjangan FROM `tbl_perpanjangan`
                LEFT JOIN tbl_user ON tbl_perpanjangan.`id_user` = tbl_user.`id_user`
                LEFT JOIN jenis_jenjang ON tbl_perpanjangan.`id_jenis_jenjang` = jenis_jenjang.`id`
                WHERE tbl_perpanjangan.deleted_date IS NULL
                ORDER BY id_perpanjangan DESC LIMIT 5";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function totalCountPerpanjangan()
    {
        $this->db->from('tbl_perpanjangan');
        $this->db->where('deleted_date IS NULL');
        $this->db->where('status_baca', 0);
        $data = $this->db->get();

        return $data->num_rows();
    }

    function updateStatusDiklat()
    {
        $updateQuery = "UPDATE tbl_diklat SET status_baca=1 WHERE status_baca=0 ";
        $this->db->query($updateQuery);

        return $this->db->affected_rows();
    }

    function selectNotifDiklat()
    {
        $sql = "SELECT tbl_user.photo as photo,tbl_user.nama_lengkap as user,kode_pengajuan,tbl_kategori_diklat.kategori_diklat as jenis_diklat FROM `tbl_diklat`
                LEFT JOIN tbl_user ON tbl_diklat.`id_user` = tbl_user.`id_user`
                LEFT JOIN tbl_kategori_diklat ON tbl_diklat.`id_kategori_diklat` = tbl_kategori_diklat.`id_kategori_diklat`
                WHERE tbl_diklat.deleted_date IS NULL
                ORDER BY id_diklat DESC LIMIT 5";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function totalCountDiklat()
    {
        $this->db->from('tbl_diklat');
        $this->db->where('deleted_date IS NULL');
        $this->db->where('status_baca', 0);
        $data = $this->db->get();

        return $data->num_rows();
    }

    function updateStatusJenjang()
    {
        $updateQuery = "UPDATE tbl_kenaikan_jenjang SET status_baca=1 WHERE status_baca=0 ";
        $this->db->query($updateQuery);

        return $this->db->affected_rows();
    }

    function selectNotifJenjang()
    {
        $sql = "SELECT tbl_user.photo as photo,tbl_user.nama_lengkap as user,kode_pengajuan,jenis_jenjang.jenjang as jenis_jenjang FROM `tbl_kenaikan_jenjang`
                LEFT JOIN tbl_user ON tbl_kenaikan_jenjang.`id_user` = tbl_user.`id_user`
                LEFT JOIN jenis_jenjang ON tbl_kenaikan_jenjang.`id_jenis_jenjang` = jenis_jenjang.`id`
                WHERE tbl_kenaikan_jenjang.deleted_date IS NULL
                ORDER BY id_jenjang DESC LIMIT 5";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function totalCountJenjang()
    {
        $this->db->from('tbl_kenaikan_jenjang');
        $this->db->where('deleted_date IS NULL');
        $this->db->where('status_baca', 0);
        $data = $this->db->get();

        return $data->num_rows();
    }

    function updateStatusInbox()
    {
        $updateQuery = "UPDATE tbl_inbox SET status_baca=1 WHERE status_baca=0 ";
        $this->db->query($updateQuery);

        return $this->db->affected_rows();
    }

    function selectNotifInbox()
    {
        $sql = "SELECT * FROM tbl_inbox WHERE deleted_date IS NULL ORDER BY id_inbox DESC LIMIT 5 ";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function totalCountInbox()
    {
        $this->db->from('tbl_inbox');
        $this->db->where('deleted_date IS NULL');
        $this->db->where('status_baca', 0);
        $data = $this->db->get();

        return $data->num_rows();
    }
}
