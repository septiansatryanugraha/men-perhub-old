<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_history extends CI_Model {

	const __tableName = 'tbl_history';
    const __tableName2 = 'tbl_user';
    const __tableId2 = 'id_user';
 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
 

    function getData($isAjaxList = 0, $filter = array()) {
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];

        $sql = "SELECT * FROM " . self::__tableName . " WHERE 1=1";
        if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
            $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
            $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
            $sql .= " AND created_date >= '{$tanggalAwal}' AND created_date <= '{$tanggalAkhir}'";
        } 

        if ($isAjaxList > 0) {
            $sql .= " ORDER BY id_history DESC";
        }

        $data = $this->db->query($sql);
        return $data->result();
    }

     public function select_user($id) {
        $sql = "SELECT * FROM " . self::__tableName2 . " WHERE " . self::__tableId2 . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }



	
}
