<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenjang extends CI_Model
{
    const __tableName = 'tbl_kenaikan_jenjang';
    const __tableId = 'id_jenjang';
    const __tableName2 = 'tbl_user';
    const __tableId2 = 'id_user';
    const __tableName3 = 'tbl_history';
    const __tableId3 = 'id_history';
    const __tableId4 = 'kode_pengajuan';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getData($isAjaxList = 0, $filter = [], $whereIn = [])
    {
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];

        $sql = "SELECT " . self::__tableName . ".*
                , jenis_jenjang.jenjang as jenis_jenjang
                , tbl_user.kode_otomatis as kode_otomatis_pemohon
                , tbl_user.nama_lengkap as nama_pemohon
                , tbl_user.no_reg as no_reg_pemohon
                , tbl_user.tempat_lahir as tempat_lahir_pemohon
                , tbl_user.tgl_lahir as tgl_lahir_pemohon
                , tbl_user.asal_instansi as asal_instansi_pemohon
                , tbl_status_kepegawaian.kode as kode_status_kepegawaian_pemohon
                , tbl_status_kepegawaian.nama as status_kepegawaian_pemohon
                , kota.kode as kode_kota_pemohon
                , kota.nama_kota as kota_pemohon
                FROM " . self::__tableName . "
                LEFT JOIN jenis_jenjang ON jenis_jenjang.id = " . self::__tableName . ".id_jenis_jenjang
                LEFT JOIN tbl_user ON tbl_user.id_user = " . self::__tableName . ".id_user
                LEFT JOIN tbl_status_kepegawaian ON tbl_status_kepegawaian.id = tbl_user.id_status_kepegawaian
                LEFT JOIN kota ON kota.id_kota = tbl_user.id_kota
                WHERE " . self::__tableName . ".deleted_date IS NULL";
        if (!empty($whereIn)) {
            $sql .= " AND " . self::__tableName . ".status_pengajuan IN (";
            foreach ($whereIn as $key => $value) {
                if ($key > 0) {
                    $sql .= ",";
                }
                $sql .= "'{$value}'";
            }
            $sql .= ")";
        }
        if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
            $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
            $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
            $sql .= " AND " . self::__tableName . ".created_date >= '{$tanggalAwal}' AND " . self::__tableName . ".created_date <= '{$tanggalAkhir}'";
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".id_jenjang DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id, $whereIn = [])
    {
        $sql = "SELECT " . self::__tableName . ".*
                , jenis_jenjang.jenjang as jenis_jenjang
                , tbl_user.kode_otomatis as kode_otomatis_pemohon
                , tbl_user.nama_lengkap as nama_pemohon
                , tbl_user.no_reg as no_reg_pemohon
                , tbl_user.tempat_lahir as tempat_lahir_pemohon
                , tbl_user.tgl_lahir as tgl_lahir_pemohon
                , tbl_user.asal_instansi as asal_instansi_pemohon
                , tbl_status_kepegawaian.kode as kode_status_kepegawaian_pemohon
                , tbl_status_kepegawaian.nama as status_kepegawaian_pemohon
                , kota.kode as kode_kota_pemohon
                , kota.nama_kota as kota_pemohon
                FROM " . self::__tableName . "
                LEFT JOIN jenis_jenjang ON jenis_jenjang.id = " . self::__tableName . ".id_jenis_jenjang
                LEFT JOIN tbl_user ON tbl_user.id_user = " . self::__tableName . ".id_user
                LEFT JOIN tbl_status_kepegawaian ON tbl_status_kepegawaian.id = tbl_user.id_status_kepegawaian
                LEFT JOIN kota ON kota.id_kota = tbl_user.id_kota
                WHERE " . self::__tableName . ".deleted_date IS NULL";
        if (!empty($whereIn)) {
            $sql .= " AND " . self::__tableName . ".status_pengajuan IN (";
            foreach ($whereIn as $key => $value) {
                if ($key > 0) {
                    $sql .= ",";
                }
                $sql .= "'{$value}'";
            }
            $sql .= ")";
        }
        $sql .= " AND " . self::__tableName . "." . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function selectHistoryJenjang($where = [])
    {
        $sql = "SELECT tbl_history_jenjang.*
                , jenis_jenjang.jenjang as jenis_jenjang
                FROM tbl_history_jenjang
                LEFT JOIN jenis_jenjang ON jenis_jenjang.id = tbl_history_jenjang.id_jenis_jenjang
                WHERE tbl_history_jenjang.deleted_date IS NULL";
        if (!empty($where)) {
            foreach ($where as $key => $value) {
                $sql .= " AND tbl_history_jenjang.{$key} = '{$value}' ";
            }
        }
        $data = $this->db->query($sql);

        return $data;
    }
}
