<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_surat_perintah_bayar extends CI_Model
{
    const __tableName = 'tbl_surat_perintah_bayar';
    const __tableId = 'id_surat';

    function get_data()
    {
        $this->db->from(self::__tableName);
        $this->db->where('deleted_date IS NULL');
        $this->db->order_by(self::__tableId);
        $data = $this->db->get();

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . "
                WHERE deleted_date IS NULL AND " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }
}
