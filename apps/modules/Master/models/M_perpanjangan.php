<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_perpanjangan extends CI_Model
{
    const __tableName = 'tbl_perpanjangan';
    const __tableId = 'id_perpanjangan';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getData($isAjaxList = 0, $filter = [], $whereIn = [])
    {
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];

        $sql = "SELECT " . self::__tableName . ".*
                , jenis_jenjang.jenjang as jenis_perpanjangan
                , tbl_user.nama_lengkap as nama_pemohon
                , tbl_user.no_reg as no_reg_pemohon
                , tbl_user.tempat_lahir as tempat_lahir_pemohon
                , tbl_user.tgl_lahir as tgl_lahir_pemohon
                , tbl_user.asal_instansi as asal_instansi_pemohon
                , kota.nama_kota as kota_pemohon
                FROM " . self::__tableName . "
                LEFT JOIN jenis_jenjang ON jenis_jenjang.id = " . self::__tableName . ".id_jenis_jenjang
                LEFT JOIN tbl_user ON tbl_user.id_user = " . self::__tableName . ".id_user
                LEFT JOIN kota ON kota.id_kota = tbl_user.id_kota
                WHERE " . self::__tableName . ".deleted_date IS NULL";
        if (!empty($whereIn)) {
            $sql .= " AND " . self::__tableName . ".status_perpanjangan IN (";
            foreach ($whereIn as $key => $value) {
                if ($key > 0) {
                    $sql .= ",";
                }
                $sql .= "'{$value}'";
            }
            $sql .= ")";
        }
        if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
            $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
            $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
            $sql .= " AND " . self::__tableName . ".created_date >= '{$tanggalAwal}' AND " . self::__tableName . ".created_date <= '{$tanggalAkhir}'";
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".id_perpanjangan DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id, $whereIn = [])
    {
        $sql = "SELECT " . self::__tableName . ".*
                , jenis_jenjang.jenjang as jenis_perpanjangan
                , tbl_user.nama_lengkap as nama_pemohon
                , tbl_user.no_reg as no_reg_pemohon
                , tbl_user.tempat_lahir as tempat_lahir_pemohon
                , tbl_user.tgl_lahir as tgl_lahir_pemohon
                , tbl_user.asal_instansi as asal_instansi_pemohon
                , kota.nama_kota as kota_pemohon
                FROM " . self::__tableName . "
                LEFT JOIN jenis_jenjang ON jenis_jenjang.id = " . self::__tableName . ".id_jenis_jenjang
                LEFT JOIN tbl_user ON tbl_user.id_user = " . self::__tableName . ".id_user
                LEFT JOIN kota ON kota.id_kota = tbl_user.id_kota
                WHERE " . self::__tableName . ".deleted_date IS NULL";
        if (!empty($whereIn)) {
            $sql .= " AND " . self::__tableName . ".status_perpanjangan in (";
            foreach ($whereIn as $key => $value) {
                if ($key > 0) {
                    $sql .= ",";
                }
                $sql .= "'{$value}'";
            }
            $sql .= ")";
        }
        $sql .= " AND " . self::__tableName . "." . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }
}
