<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat_perintah_bayar extends AUTH_Controller
{
    const __tableName = 'tbl_surat_perintah_bayar';
    const __tableId = 'id_surat';
    const __folder = 'v_surat_perintah_bayar/';
    const __kode_menu = 'master-surat-bayar';
    const __title = 'Surat Perintah Bayar';
    const __model = 'M_surat_perintah_bayar';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
        $this->load->model('M_user');
        $this->load->model('M_utilities');
    }

    public function index()
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $this->loadkonten('' . self::__folder . 'home', $data);
        }
    }

    public function ajaxList()
    {
        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_surat_perintah_bayar->get_data();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->subjek;
            $row[] = $brand->no_surat;
            $row[] = $brand->tipe_surat;
            $row[] = date('d-m-Y', strtotime($brand->tgl_surat));
            //add html for action

            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-surat-bayar') . "/" . $brand->id_surat . "' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-surat-bayar' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_surat . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">Data " . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['jenis_jenjang'] = $this->M_utilities->selectJenjang();
            $data['jenis_diklat'] = $this->M_utilities->selectDiklat();
            $this->loadkonten(self::__folder . 'tambah', $data);
        }
    }

    public function prosesAdd()
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $tipeSurat = $this->input->post('tipe_surat');
        $idJenisJenjang = $this->input->post('id_jenis_jenjang');
        $idKategoriDiklat = $this->input->post('id_kategori_diklat');
        $subjek = $this->input->post('subjek');
        $kepada = $this->input->post('kepada');
        $perihal = $this->input->post('perihal');
        $totalPeserta = $this->input->post('total_peserta');
        $noSurat = $this->input->post('no_surat');
        $tglSurat = $this->input->post('tgl_surat');
        $noBa = $this->input->post('no_ba');
        $tglBa = $this->input->post('tgl_ba');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tipeSurat) == 0) {
                $errCode++;
                $errMessage = "Jenis Surat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if ($tipeSurat == 'Surat Panggilan Jenjang') {
                if (strlen($idJenisJenjang) == 0) {
                    $errCode++;
                    $errMessage = "Jenis Jenjang wajib di isi.";
                }
            }
            if ($tipeSurat == 'Surat Panggilan Diklat') {
                if (strlen($idKategoriDiklat) == 0) {
                    $errCode++;
                    $errMessage = "Jenis Jenjang wajib di isi.";
                }
            }
        }
        if ($errCode == 0) {
            if (strlen($subjek) == 0) {
                $errCode++;
                $errMessage = "Subjek wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($kepada) == 0) {
                $errCode++;
                $errMessage = "Kepada wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noSurat) == 0) {
                $errCode++;
                $errMessage = "No Surat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglSurat) == 0) {
                $errCode++;
                $errMessage = "Tanggal Surat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noBa) == 0) {
                $errCode++;
                $errMessage = "No BA wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglBa) == 0) {
                $errCode++;
                $errMessage = "Tanggal BA wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'tipe_surat' => $tipeSurat,
                    'subjek' => $subjek,
                    'kepada' => $kepada,
                    'perihal' => $perihal,
                    'total_peserta' => $totalPeserta,
                    'no_surat' => $noSurat,
                    'tgl_surat' => date('Y-m-d', strtotime($tglSurat)),
                    'no_ba' => $noBa,
                    'tgl_ba' => date('Y-m-d', strtotime($tglBa)),
                    'created_by' => $username,
                    'created_date' => $datetime,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];

                if ($tipeSurat == 'Surat Panggilan Jenjang') {
                    $data = array_merge($data, [
                        'id_jenis_jenjang' => $idJenisJenjang,
                        'id_kategori_diklat' => null,
                    ]);
                }
                if ($tipeSurat == 'Surat Panggilan Diklat') {
                    $data = array_merge($data, [
                        'id_jenis_jenjang' => null,
                        'id_kategori_diklat' => $idKategoriDiklat,
                    ]);
                }
                $result = $this->db->insert(self::__tableName, $data);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">Data " . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_surat_perintah_bayar->selectById($id);
            if ($brand != null) {
                $data['brand'] = $brand;
                $data['jenis_jenjang'] = $this->M_utilities->selectJenjang();
                $data['jenis_diklat'] = $this->M_utilities->selectDiklat();
                $data['exportPdf'] = "<a href=" . base_url('export-surat-bayar') . "/" . $brand->id_surat . " class='btn default btn-sm klik'><i class='fa fa-file-pdf-o'></i> Export PDF</a>";
                $this->loadkonten(self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $tipeSurat = $this->input->post('tipe_surat');
        $idJenisJenjang = $this->input->post('id_jenis_jenjang');
        $idKategoriDiklat = $this->input->post('id_kategori_diklat');
        $subjek = $this->input->post('subjek');
        $kepada = $this->input->post('kepada');
        $perihal = $this->input->post('perihal');
        $totalPeserta = $this->input->post('total_peserta');
        $noSurat = $this->input->post('no_surat');
        $tglSurat = $this->input->post('tgl_surat');
        $noBa = $this->input->post('no_ba');
        $tglBa = $this->input->post('tgl_ba');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_surat_perintah_bayar->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tipeSurat) == 0) {
                $errCode++;
                $errMessage = "Jenis Surat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if ($tipeSurat == 'Surat Panggilan Jenjang') {
                if (strlen($idJenisJenjang) == 0) {
                    $errCode++;
                    $errMessage = "Jenis Jenjang wajib di isi.";
                }
            }
            if ($tipeSurat == 'Surat Panggilan Diklat') {
                if (strlen($idKategoriDiklat) == 0) {
                    $errCode++;
                    $errMessage = "Jenis Jenjang wajib di isi.";
                }
            }
        }
        if ($errCode == 0) {
            if (strlen($subjek) == 0) {
                $errCode++;
                $errMessage = "Subjek wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($kepada) == 0) {
                $errCode++;
                $errMessage = "Kepada wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noSurat) == 0) {
                $errCode++;
                $errMessage = "No Surat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglSurat) == 0) {
                $errCode++;
                $errMessage = "Tanggal Surat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noBa) == 0) {
                $errCode++;
                $errMessage = "No BA wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglBa) == 0) {
                $errCode++;
                $errMessage = "Tanggal BA wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'tipe_surat' => $tipeSurat,
                    'subjek' => $subjek,
                    'kepada' => $kepada,
                    'perihal' => $perihal,
                    'total_peserta' => $totalPeserta,
                    'no_surat' => $noSurat,
                    'tgl_surat' => date('Y-m-d', strtotime($tglSurat)),
                    'no_ba' => $noBa,
                    'tgl_ba' => date('Y-m-d', strtotime($tglBa)),
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];

                if ($tipeSurat == 'Surat Panggilan Jenjang') {
                    $data = array_merge($data, [
                        'id_jenis_jenjang' => $idJenisJenjang,
                        'id_kategori_diklat' => null,
                    ]);
                }
                if ($tipeSurat == 'Surat Panggilan Diklat') {
                    $data = array_merge($data, [
                        'id_jenis_jenjang' => null,
                        'id_kategori_diklat' => $idKategoriDiklat,
                    ]);
                }
                $result = $this->db->update(self::__tableName, $data, [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function suratBayar($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['title'] = self::__title;
        $data['judul'] = self::__title;
        $data['page'] = self::__title;
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_surat_perintah_bayar->selectById($id);
            if ($brand != null) {
                $view = "";
                $data['datamaster'] = $brand;
                if ($brand->tipe_surat == 'Surat Panggilan Diklat') {
                    $view = 'surat_perintah_bayar_diklat';
                    $data['jenis_diklat'] = $this->M_utilities->selectDiklat();
                } elseif ($brand->tipe_surat == 'Surat Panggilan Jenjang') {
                    $view = 'surat_perintah_bayar_jenjang_pdf';
                    $data['jenis_jenjang'] = $this->M_utilities->selectJenjang();
                } else {
                    echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
                }
                $this->load->view(self::__folder . $view, $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function exportSuratBayar($id)
    {
        /* ini harus ada boss */
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        $data['title'] = self::__title;
        $data['userdata'] = $this->userdata;
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_surat_perintah_bayar->selectById($id);
            if ($brand != null) {
                $view = "";
                $data['datamaster'] = $brand;
                $judulSurat = $brand->subjek . " " . date("d-m-Y h:i");
                if ($brand->tipe_surat == 'Surat Panggilan Diklat') {
                    $view = 'surat_perintah_bayar_diklat_pdf';
                    $data['jenis_diklat'] = $this->M_utilities->selectDiklat();
                } elseif ($brand->tipe_surat == 'Surat Panggilan Jenjang') {
                    $view = 'surat_perintah_bayar_jenjang_pdf';
                    $data['jenis_jenjang'] = $this->M_utilities->selectJenjang();
                } else {
                    echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
                }
                $this->pdf->load_view(self::__folder . $view, $data);
//                $this->pdf->render();
//                $this->pdf->stream($judulSurat . '.pdf');
                $this->pdf->output();
                die();
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesDelete()
    {
        $errCode = 0;
        $errMessage = "";

        $id = $_POST[self::__tableId];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_surat_perintah_bayar->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $result = $this->db->update(self::__tableName, ['deleted_date' => date('Y-m-d H:i:s')], [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di hapus'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
