<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_user extends AUTH_Controller
{
    const __tableName = 'tbl_user';
    const __tableName2 = 'tbl_login';
    const __tableId = 'id_user';
    const __tableIdname = 'name';
    const __folder = 'v_user/';
    const __kode_menu = 'master-user';
    const __title = 'Master User ';
    const __model = 'M_user';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
        $this->load->model('M_provinsi');
        $this->load->model('M_kota');
        $this->load->model('M_utilities');
    }

    public function index()
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $this->loadkonten(self::__folder . 'home', $data);
        }
    }

    public function ajaxList()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = [
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        ];

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_user->getData(1, $filter);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $status = '<span class="badge bg-green">Non Aktif</span">';
            if ($brand->status == 'Aktif') {
                $status = '<span class="badge bg-red">Aktif</span">';
            }
            if ($brand->photo == '') {
                $gambar = '<img class="img-thumbnail" src="' . base_url() . 'upload/foto/no-user.jpg" width="100" /></center>';
            } else {
                $gambar = '<a href="' . $brand->photo . '" target="_blank"><img class="img-thumbnail" src="' . base_url() . $brand->photo . '"   width="90" /></a>';
            }

            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $gambar;
            $row[] = $brand->nama_lengkap;
            $row[] = $brand->email;
            $row[] = $brand->instansi;
            $row[] = $status;

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                if ($brand->status != 'Aktif') {
                    $action .= "    <li><a href='" . base_url('edit-catatan-user') . "/" . $brand->id_user . "' class='klik ajaxify'><i class='fa fa-send'></i> Ubah Catatan</a></li>";
                }
                $action .= "    <li><a href='" . base_url('histori-user') . "/" . $brand->id_user . "' class='klik ajaxify'><i class='fa fa-history'></i> Histori Pengajuan</a></li>";
                $action .= "    <li><a href='" . base_url('edit-master-user') . "/" . $brand->id_user . "' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-master-user' data-toggle='tooltip' data-placement='top' data-id='" . $brand->email . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function history($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">Data " . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['title'] = self::__title;
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $user = $this->M_user->selectById($id);
            if ($user != null) {
                $data['brand'] = $user;
                $data['ValData'] = $this->M_user->selectHistoryDiklat($user->id_user);
                $data['ValData2'] = $this->M_user->selectHistoryJenjang($user->id_user);
                $this->loadkonten(self::__folder . 'histori', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function download()
    {
        if ($this->input->post('berkas')) {
            $berkas = $this->input->post('berkas');
            foreach ($berkas as $res) {
                $this->zip->read_file($res);
            }
            $this->zip->download('file-berkas' . time() . '.zip');
        }
    }

    public function Edit($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">Data " . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_user->selectById($id);
            if ($brand != null) {
                $data['brand'] = $brand;
                $data['jkel'] = $this->M_utilities->selectKategoriStatus([], ['Pria', 'Wanita']);
                $data['status_pegawai'] = $this->M_utilities->selectKategoriStatus([], ['PNS', 'P3K', 'NON ASN']);
                $data['instansi'] = $this->M_utilities->selectKategoriStatus([], ['Dishub Provinsi', 'Dishub Kabupaten', 'Dishub Kota', 'Ditjen Perhubungan Darat', 'Lembaga Diklat']);
                $data['status'] = $this->M_utilities->selectStatusGrup([], ['Non aktif', 'Aktif']);
                $data['provinsi'] = $this->M_provinsi->select();
                $data['kota'] = $this->M_kota->select();
                $data["berkas"] = glob('upload/berkas_registrasi/' . $brand->folder . "/*");
                $this->loadkonten('' . self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $idProvinsi = $this->input->post('id_provinsi');
        $idKota = $this->input->post('id_kota');
        $jkel = $this->input->post('jkel');
        $statusPegawai = $this->input->post('status_pegawai');
        $namaLengkap = trim($this->input->post('nama_lengkap'));
        $tglLahir = $this->input->post('tgl_lahir');
        $tempatLahir = $this->input->post('tempat_lahir');
        $nomorHp = $this->input->post('nomor_hp');
        $instansi = $this->input->post('instansi');
        $alamatInstansi = $this->input->post('alamat_instansi');

        $email = trim($this->input->post('email'));
        $statusAccount = $this->input->post('status');
        $password = $this->input->post('password');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_user->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($jkel) == 0) {
                $errCode++;
                $errMessage = "Jenis Kelamin wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($statusPegawai) == 0) {
                $errCode++;
                $errMessage = "Status Kepegawaian wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tempatLahir) == 0) {
                $errCode++;
                $errMessage = "Tempat Lahir wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglLahir) == 0) {
                $errCode++;
                $errMessage = "Tanggal Lahir wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($nomorHp) == 0) {
                $errCode++;
                $errMessage = "Nomor HP wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($statusAccount) == 0) {
                $errCode++;
                $errMessage = "Status User wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($instansi) == 0) {
                $errCode++;
                $errMessage = "Instansi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($alamatInstansi) == 0) {
                $errCode++;
                $errMessage = "Alamat Instansi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'id_kota' => $idKota,
                    'jkel' => $jkel,
                    'status_pegawai' => $statusPegawai,
                    'tempat_lahir' => $tempatLahir,
                    'tgl_lahir' => date('Y-m-d', strtotime($tglLahir)),
                    'nomor_hp' => $nomorHp,
                    'status' => $statusAccount,
                    'instansi' => $instansi,
                    'alamat_instansi' => $alamatInstansi,
//                    'no_reg' => $this->input->post('no_reg'),
                    'ip_address' => $this->input->ip_address(),
                    'user_agent' => $this->input->user_agent(),
                    'updated_by' => $username,
                    'updated_date' => $datetime
                ];
                if (strlen($password) > 0) {
                    $data = array_merge($data, [
                        'password' => base64_encode($password),
                    ]);
                }
                $result = $this->db->update(self::__tableName, $data, [self::__tableId => $id]);

                $data2 = [
                    'email' => $this->input->post('email'),
                    'status' => $this->input->post('status'),
                    'tipe' => 'user',
                    'password' => base64_encode($password),
                ];
                $result = $this->db->update(self::__tableName2, $data2, ['email' => $email]);

                if ($statusAccount == 'Aktif') {
                    $web = "http://localhost/men-perhub/";
                    $link_web = '<a href="' . $web . '">Disini</a>';

                    $config = [
                        'useragent' => 'Codeigniter',
                        'protocol' => 'smtp',
                        'smtp_host' => 'smtp.gmail.com',
                        'smtp_port' => 587,
                        'smtp_crypto' => 'tls',
                        'smtp_timeout' => 100,
                        'smtp_user' => 'angkasamuda20@gmail.com',
                        'smtp_pass' => 'agkasamuda2020',
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'newline' => "\r\n"
                    ];

                    $this->load->library('email');
                    $this->email->initialize($config);

                    $this->email->from('angkasamuda20@gmail.com');
                    $this->email->to($this->input->post('email'));
                    $this->email->subject('Aktivasi Akun - E-Kopetensi System');
                    $message = 'Terima kasih telah melakukan pendaftaran, Akun anda sekarang sudah aktif silahkan login di Website E-Kopetensi
			<br><br><br>
			Best regards,<br><br><br>
			<b><font color="black">Admin E-Kopetensi</font></b><br>
			Kementerian Perhubungan Direktorat Jendral Perhubungan Darat';
                    $this->email->message($message);
                    $this->email->send();
                }
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function EditCatatan($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">Data " . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $user = $this->M_user->selectById($id);
            if ($user != null) {
                if ($user->status != 'Aktif') {
                    $data['brand'] = $user;
                    $this->loadkonten('' . self::__folder . 'update-catatan', $data);
                } else {
                    echo "<script>alert('" . self::__title . " sudah aktif.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
                }
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdateCatatan($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $subject = $this->input->post('subject');
        $catatan = $this->input->post('catatan');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_user->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkValid->status == 'Aktif') {
                $errCode++;
                $errMessage = self::__title . " sudah aktif.";
            }
        }
        if ($errCode == 0) {
            if (strlen($subject) == 0) {
                $errCode++;
                $errMessage = "Subjek wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($catatan) == 0) {
                $errCode++;
                $errMessage = "Catatan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'subject' => $subject,
                    'catatan' => $catatan,
                    'updated_date' => $datetime,
                    'updated_by' => $username,
                ];
                $result = $this->db->update(self::__tableName, $data, [self::__tableId => $id]);

                $config = ['useragent' => 'Codeigniter',
                    'protocol' => 'smtp',
                    'smtp_host' => 'smtp.gmail.com',
                    'smtp_port' => 587,
                    'smtp_crypto' => 'tls',
                    'smtp_timeout' => 100,
                    'smtp_user' => 'angkasamuda20@gmail.com',
                    'smtp_pass' => 'agkasamuda2020',
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'newline' => "\r\n"
                ];

                $this->load->library('email');
                $this->email->initialize($config);

                $this->email->from('angkasamuda20@gmail.com');
                $this->email->to($checkValid->email);

                $this->email->subject($subject);
                $message = '' . $catatan . '
			<br><br><br>
			Best regards,<br><br><br>
			<b><font color="black">Admin E-Kopetensi</font></b><br>
			Kementerian Perhubungan Direktorat Jendral Perhubungan Darat';
                $this->email->message($message);
                $this->email->send();
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $token = $this->input->post('email');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($token) == 0) {
                $errCode++;
                $errMessage = "Email does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_user->selectById2($token);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->load->helper("file");

                //hapus unlink file photo
                if (file_exists($checkValid->photo)) {
                    unlink($checkValid->photo);
                }

                $path = 'upload/berkas_registrasi/' . $checkValid->folder;
                delete_files($path, true, false, 1);

                $result = $this->db->update(self::__tableName, ['photo' => null, 'deleted_date' => date('Y-m-d H:i:s')], ['email' => $token]);
                $result = $this->db->update(self::__tableName2, ['deleted_date' => date('Y-m-d H:i:s')], ['email' => $token]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di hapus'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
