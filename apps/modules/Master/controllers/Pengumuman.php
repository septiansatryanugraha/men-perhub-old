<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengumuman extends AUTH_Controller
{
    const __tableName = 'tbl_pengumuman_email';
    const __tableId = 'id_pengumuman';
    const __folder = 'v_pengumuman/';
    const __kode_menu = 'pengumuman';
    const __title = 'Pengumuman ';
    const __model = 'M_pengumuman';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
        $this->load->model('M_utilities');
    }

    public function index()
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $this->loadkonten('' . self::__folder . 'home', $data);
        }
    }

    public function ajaxList()
    {
        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);

        $list = $this->M_pengumuman->getData();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->judul;
            $row[] = $brand->konten;

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-pengumuman') . "/" . $brand->id_pengumuman . "' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-pengumuman' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_pengumuman . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">Data " . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $this->loadkonten(self::__folder . 'tambah', $data);
        }
    }

    public function prosesAdd()
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $tipeSurat = $this->input->post('tipe_surat');
        $judul = $this->input->post('judul');
        $konten = $this->input->post('konten');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tipeSurat) == 0) {
                $errCode++;
                $errMessage = "Jenis Surat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($judul) == 0) {
                $errCode++;
                $errMessage = "Judul wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($konten) == 0) {
                $errCode++;
                $errMessage = "Konten wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $config['upload_path'] = "./upload/pengumuman/";
                $config['allowed_types'] = 'pdf';
                $config['max_size'] = '5048'; //maksimum besar file 2M
                $config['encrypt_name'] = TRUE;
                $config['overwrite'] = TRUE;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload("file_surat")) {
                    $path['link'] = "upload/pengumuman/";
                    $imageData = $this->upload->data();

                    $data = [
                        'tipe_surat' => $tipeSurat,
                        'judul' => $judul,
                        'konten' => $konten,
                        'nama_file_surat' => $imageData['file_name'],
                        'file_surat' => $path['link'] . '' . $imageData['file_name'],
                        'created_by' => $username,
                        'created_date' => $date,
                        'created_by' => $username,
                        'created_date' => $datetime,
                        'updated_by' => $username,
                        'updated_date' => $datetime,
                    ];
                    $result = $this->db->insert(self::__tableName, $data);

                    $data = [];
                    if ($tipeSurat == 'Surat Panggilan Jenjang') {
                        $data = $this->M_utilities->selectEmailJenjang();
                    }
                    if ($tipeSurat == 'Surat Panggilan Diklat') {
                        $data = $this->M_utilities->selectEmailDiklat();
                    }

                    if (!empty($data)) {
                        $dataEmail = [];
                        foreach ($data as $key => $value) {
                            $dataEmail[] = $value->email;
                        }

                        $config = array(
                            'useragent' => 'Codeigniter',
                            'protocol' => 'smtp',
                            'smtp_host' => 'smtp.gmail.com',
                            'smtp_port' => 587,
                            'smtp_crypto' => 'tls',
                            'smtp_timeout' => 100,
                            'smtp_user' => 'angkasamuda20@gmail.com',
                            'smtp_pass' => 'agkasamuda2020',
                            'mailtype' => 'html',
                            'charset' => 'utf-8',
                            'newline' => "\r\n"
                        );

                        $this->load->library('email');
                        $this->email->initialize($config);

                        $this->email->from('angkasamuda20@gmail.com');
                        $this->email->to($dataEmail);

                        $this->email->subject($judul);
                        $message = '' . $konten . '
                <br><br><br>
                Best regards,<br><br><br>
                <b><font color="black">Admin E-Kopetensi</font></b><br>
                Kementerian Perhubungan Direktorat Jendral Perhubungan Darat';
                        $this->email->message($message);
                        $this->email->attach($imageData['full_path']);
                        $this->email->send();
                    }
                }
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">Data " . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_pengumuman->selectById($id);
            if ($brand != null) {
                $data['brand'] = $brand;
                $this->loadkonten(self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $tipeSurat = $this->input->post('tipe_surat');
        $judul = $this->input->post('judul');
        $konten = $this->input->post('konten');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_pengumuman->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tipeSurat) == 0) {
                $errCode++;
                $errMessage = "Jenis Surat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($judul) == 0) {
                $errCode++;
                $errMessage = "Judul wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($konten) == 0) {
                $errCode++;
                $errMessage = "Konten wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'tipe_surat' => $tipeSurat,
                    'judul' => $judul,
                    'konten' => $konten,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $errCode = 0;
        $errMessage = "";

        $id = $_POST[self::__tableId];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_pengumuman->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->load->helper("file");

                //hapus unlink file surat
                if (file_exists($checkValid->file_surat)) {
                    unlink($checkValid->file_surat);
                }

                $result = $this->db->update(self::__tableName, ['file_surat' => null, 'deleted_date' => date('Y-m-d H:i:s')], [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di hapus'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
