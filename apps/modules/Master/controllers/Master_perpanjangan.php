<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_perpanjangan extends AUTH_Controller
{
    const __tableName = 'tbl_perpanjangan';
    const __tableName2 = 'tbl_history';
    const __tableId = 'id_perpanjangan';
    const __folder = 'v_perpanjangan/';
    const __kode_menu = 'master-perpanjangan';
    const __kode_menu2 = 'master-perpanjangan-validasi';
    const __kode_menu3 = 'master-perpanjangan-approve';
    const __title = 'Master Perpanjangan Kompetensi ';
    const __model = 'M_perpanjangan';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
        $this->load->model('M_utilities');
    }

    public function index()
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $this->loadkonten('' . self::__folder . 'home', $data);
        }
    }

    public function ajaxList()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = [
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        ];

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_perpanjangan->getData(1, $filter, ['Pending', 'Proses Verifikasi']);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $status = '<span class="badge bg-red">Status Pending</span>';
            if ($brand->status_perpanjangan == 'Proses Verifikasi') {
                $status = '<span class="badge bg-blue">Proses Verifikasi</span>';
            } else if ($brand->status_perpanjangan == 'Approve') {
                $status = '<span class="badge bg-green">Approve</span>';
            }

            if ($brand->updated_by == NULL) {
                $proses = "Belum ada yang memproses";
            } else {
                $proses = $brand->updated_by;
            }

            if ($brand->no_ba == NULL) {
                $NoBa = '<span class="badge bg-red">Belum Ada</span>';
            } else {
                $NoBa = $brand->no_ba;
            }

            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->kode_pengajuan;
            $row[] = $brand->nama_pemohon;
            $row[] = $brand->email;
            $row[] = $brand->jenis_perpanjangan;
            $row[] = $status . '<br>' . $brand->status . '</br>' . date('d-m-Y', strtotime($brand->updated_date)) . '<br>Perpanjangan Pengajuan di Proses oleh : <b>' . $proses . '</b>';

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($brand->status_perpanjangan != 'Approve' && $accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-perpanjangan') . "/" . $brand->id_perpanjangan . "' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-perpanjangan' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_perpanjangan . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data " . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_perpanjangan->selectById($id, ['Pending', 'Proses Verifikasi']);
            if ($brand != null) {
                $data['brand'] = $brand;
                $data['status'] = $this->M_utilities->selectStatusGrup([], ['Proses Verifikasi']);
                $data["berkas"] = glob('upload/berkas_perpanjangan/' . $brand->folder . "/*");
                $this->loadkonten(self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $statusPerpanjangan = $this->input->post('status_perpanjangan');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_perpanjangan->selectById($id, ['Pending', 'Proses Verifikasi']);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($statusPerpanjangan) == 0) {
                $errCode++;
                $errMessage = "Status Perpanjangan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'keterangan_status' => 'Pengajuan user <b>' . $checkValid->nama_pemohon . '</b> untuk proses perpanjangan Kompetensi tingkat ' . $checkValid->jenis_perpanjangan . ' Sedang di ' . $statusPerpanjangan . ' oleh admin sistem E-Kompetensi',
                    'status_perpanjangan' => $statusPerpanjangan,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                $result = $this->db->update(self::__tableName, $data, [self::__tableId => $id]);

                $data2 = [
                    'kode_pengajuan' => $checkValid->kode_pengajuan,
                    'id_user' => $checkValid->id_user,
                    'keterangan_status' => 'Pengajuan user <b>' . $checkValid->nama_pemohon . '</b> untuk proses perpanjangan Kompetensi tingkat ' . $checkValid->jenis_perpanjangan . ' Sedang di ' . $statusPerpanjangan . ' oleh admin sistem E-Kompetensi',
                    'status' => $statusPerpanjangan,
                    'created_by' => 'System',
                    'created_date' => $datetime,
                ];
                $result = $this->db->insert(self::__tableName2, $data2);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function homeValidasi()
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-perpanjangan-validasi';
        $data['title'] = 'Validasi Perpanjangan';
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $this->loadkonten('' . self::__folder . 'home_validasi', $data);
        }
    }

    public function ajaxValidasi()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = [
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        ];

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu2);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu2);
        $list = $this->M_perpanjangan->getData(1, $filter, ['Proses Verifikasi', 'Proses Validasi', 'Belum Validasi']);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $status = '<span class="badge bg-blue">Proses Verifikasi</span>';
            if ($brand->status_perpanjangan == 'Proses Validasi') {
                $status = '<span class="badge bg-blue">Proses Validasi</span>';
            } elseif ($brand->status_perpanjangan == 'Belum Validasi') {
                $status = '<span class="badge bg-red">Belum Validasi</span>';
            }

            if ($brand->updated_by == NULL) {
                $proses = "Belum ada yang memproses";
            } else {
                $proses = $brand->updated_by;
            }

            if ($brand->no_ba == NULL) {
                $NoBa = '<span class="badge bg-red">Belum Ada</span>';
            } else {
                $NoBa = $brand->no_ba;
            }

            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->kode_pengajuan;
            $row[] = $brand->nama_pemohon;
            $row[] = $brand->email;
            $row[] = $brand->jenis_perpanjangan;
            $row[] = $status . '<br> ' . $brand->status . '</br>' . date('d-m-Y', strtotime($brand->updated_date)) . '<br>Perpanjangan Pengajuan di Proses oleh : <b>' . $proses . '</b>';

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($brand->status_perpanjangan != 'Approve' && $accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-perpanjangan-validasi') . "/" . $brand->id_perpanjangan . "' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-perpanjangan' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_perpanjangan . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function editValidasi($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-perpanjangan-validasi';
        $data['title'] = 'Validasi Perpanjangan';
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data " . $data['title'] . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_perpanjangan->selectById($id, ['Proses Verifikasi', 'Proses Validasi', 'Belum Validasi']);
            if ($brand != null) {
                $data['brand'] = $brand;
                $data['status'] = $this->M_utilities->selectStatusGrup([], ['Belum Validasi', 'Proses Validasi']);
                $data["berkas"] = glob('upload/berkas_perpanjangan/' . $brand->folder . "/*");
                $this->loadkonten(self::__folder . 'update_validasi', $data);
            } else {
                echo "<script>alert('Data " . $data['title'] . " tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function prosesUpdateValidasi($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $noReg = $this->input->post('no_reg');
        $noBa = $this->input->post('no_ba');
        $tglBa = $this->input->post('tgl_ba');
        $noSertifikat = $this->input->post('no_sertifikat');
        $statusPerpanjangan = $this->input->post('status_perpanjangan');
        $catatan = $this->input->post('catatan');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_perpanjangan->selectById($id, ['Proses Verifikasi', 'Proses Validasi', 'Belum Validasi']);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noReg) == 0) {
                $errCode++;
                $errMessage = "No registrasi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noBa) == 0) {
                $errCode++;
                $errMessage = "No Berita Acara wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglBa) == 0) {
                $errCode++;
                $errMessage = "Tanggal Berita Acara wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noSertifikat) == 0) {
                $errCode++;
                $errMessage = "Tanggal Sertifikat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($statusPerpanjangan) == 0) {
                $errCode++;
                $errMessage = "Status Perpanjangan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                if ($statusPerpanjangan == 'Proses Validasi') {
                    $keteranganStatus = 'Pengajuan user <b>' . $checkValid->nama_pemohon . '</b> untuk proses perpanjangan Kompetensi tingkat ' . $checkValid->jenis_perpanjangan . ' sedang di ' . $statusPerpanjangan . ' oleh admin sistem Kompetensi';
                } else {
                    $statusPerpanjangan = 'Pengajuan user <b>' . $checkValid->nama_pemohon . '</b> untuk proses perpanjangan Kompetensi tingkat ' . $checkValid->jenis_perpanjangan . ' ' . $statusPerpanjangan . ' oleh admin sistem E-Kompetensi .karena ada beberapa dokumen yang belum memenuhi syarat.';
                }

                $data = [
                    'no_reg' => $noReg,
                    'no_ba' => $noBa,
                    'tgl_ba' => date('Y-m-d', strtotime($tglBa)),
                    'no_sertifikat' => $noSertifikat,
                    'catatan' => $catatan,
                    'keterangan_status' => $keteranganStatus,
                    'status_perpanjangan' => $statusPerpanjangan,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                $this->db->update(self::__tableName, $data, [self::__tableId => $id]);

                $data2 = [
                    'kode_pengajuan' => $checkValid->kode_pengajuan,
                    'id_user' => $checkValid->id_user,
                    'keterangan_status' => $keteranganStatus,
                    'status' => $statusPerpanjangan,
                    'created_by' => 'System',
                    'created_date' => $datetime,
                ];
                $this->db->insert(self::__tableName2, $data2);

                $this->db->update('tbl_user', ['no_reg' => $noReg], ['id_user' => $checkValid->id_user]);

                if ($statusPerpanjangan != 'Proses Validasi') {
                    $config = [
                        'useragent' => 'Codeigniter',
                        'protocol' => 'smtp',
                        'smtp_host' => 'smtp.gmail.com',
                        'smtp_port' => 587,
                        'smtp_crypto' => 'tls',
                        'smtp_timeout' => 100,
                        'smtp_user' => 'angkasamuda20@gmail.com',
                        'smtp_pass' => 'agkasamuda2020',
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'newline' => "\r\n"
                    ];

                    $this->load->library('email');
                    $this->email->initialize($config);

                    $this->email->from('angkasamuda20@gmail.com');
                    $this->email->to($checkValid->email);

                    $this->email->subject('Notifikasi Pengajuan Kompetensi');
                    $message = '' . $catatan . '
				<br><br><br>
				Best regards,<br><br><br>
				<b><font color="black">Admin E-Kopetensi</font></b><br>
				Kementerian Perhubungan Direktorat Jendral Perhubungan Darat';
                    $this->email->message($message);
                    $this->email->send();
                }
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function homeApprove()
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-perpanjangan-approve';
        $data['title'] = 'Approve Perpanjangan';
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $this->loadkonten('' . self::__folder . 'home_approve', $data);
        }
    }

    public function ajaxApprove()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = [
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        ];

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_perpanjangan->getData(1, $filter, ['Proses Validasi', 'Approve']);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            if ($brand->status_perpanjangan == 'Proses Validasi') {
                $status = '<span class="badge bg-blue">Proses Validasi</span>';
            } else if ($brand->status_perpanjangan == 'Approve') {
                $status = '<span class="badge bg-green">Approve</span>';
            }

            if ($brand->updated_by == NULL) {
                $proses = "Belum ada yang memproses";
            } else {
                $proses = $brand->updated_by;
            }

            if ($brand->no_ba == NULL) {
                $NoBa = '<span class="badge bg-red">Belum Ada</span>';
            } else {
                $NoBa = $brand->no_ba;
            }

            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->kode_pengajuan;
            $row[] = $brand->nama_pemohon;
            $row[] = $brand->email;
            $row[] = $brand->jenis_perpanjangan;
            $row[] = $status . '<br>' . $brand->status . '</br>' . date('d-m-Y', strtotime($brand->updated_date)) . '<br>Perpanjangan Pengajuan di Proses oleh : <b>' . $proses . '</b>';

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($brand->status_pembayaran == 'Lunas') {
                $action .= "    <li><a href='" . base_url('cetak-perpanjangan') . "/" . $brand->id_perpanjangan . "' class='klik ajaxify' target='__blank'><i class='fa fa-print'></i> Print</a></li>";
            } else {
                $action .= "    <li><a href='" . base_url('edit-perpanjangan-approve') . "/" . $brand->id_perpanjangan . "' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>";
                if ($brand->status_perpanjangan == 'Approve') {
                    $action .= "    <li><a href='" . base_url('edit-file-pnbp') . "/" . $brand->id_perpanjangan . "' class='klik ajaxify'><i class='fa fa-file'></i> Ubah PNBP</a></li>";
                    $action .= "    <li><a href='" . base_url('upload-pnbp') . "/" . $brand->id_perpanjangan . "' class='klik ajaxify'><i class='fa fa-upload'></i> Upload PNBP</a></li>";
                }
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-perpanjangan' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_perpanjangan . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function editApprove($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-perpanjangan-approve';
        $data['title'] = 'Approve Perpanjangan';
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data " . $data['title'] . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_perpanjangan->selectById($id, ['Proses Validasi', 'Approve']);
            if ($brand != null) {
                $data['brand'] = $brand;
                $data['status'] = $this->M_utilities->selectStatusGrup([], ['Approve']);
                $data["berkas"] = glob('upload/berkas_perpanjangan/' . $brand->folder . "/*");
                $this->loadkonten(self::__folder . 'update_approve', $data);
            } else {
                echo "<script>alert('Data " . $data['title'] . " tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function prosesUpdateApprove($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $noReg = $this->input->post('no_reg');
        $noBa = $this->input->post('no_ba');
        $tglBa = $this->input->post('tgl_ba');
        $noSertifikat = $this->input->post('no_sertifikat');
        $statusPerpanjangan = $this->input->post('status_perpanjangan');
        $catatan = $this->input->post('catatan');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_perpanjangan->selectById($id, ['Proses Validasi', 'Approve']);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noReg) == 0) {
                $errCode++;
                $errMessage = "No registrasi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noBa) == 0) {
                $errCode++;
                $errMessage = "No Berita Acara wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglBa) == 0) {
                $errCode++;
                $errMessage = "Tanggal Berita Acara wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noSertifikat) == 0) {
                $errCode++;
                $errMessage = "Tanggal Sertifikat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($statusPerpanjangan) == 0) {
                $errCode++;
                $errMessage = "Status Perpanjangan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'no_reg' => $noReg,
                    'no_ba' => $noBa,
                    'tgl_ba' => date('Y-m-d', strtotime($tglBa)),
                    'no_sertifikat' => $noSertifikat,
                    'catatan' => $catatan,
                    'keterangan_status' => 'Pengajuan user <b>' . $checkValid->nama_pemohon . '</b> untuk proses perpanjangan Kompetensi tingkat ' . $checkValid->jenis_perpanjangan . ' telah di ' . $statusPerpanjangan . ' oleh admin sistem Kompetensi.dan akan segera mendapatkan pembayaran Biling PNBP di email anda.',
                    'status_perpanjangan' => $statusPerpanjangan,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                $this->db->update(self::__tableName, $data, [self::__tableId => $id]);

                $data2 = [
                    'kode_pengajuan' => $checkValid->kode_pengajuan,
                    'id_user' => $checkValid->id_user,
                    'keterangan_status' => 'Pengajuan user <b>' . $checkValid->nama_pemohon . '</b> untuk proses perpanjangan Kompetensi tingkat ' . $checkValid->jenis_perpanjangan . ' telah di ' . $statusPerpanjangan . ' oleh admin sistem Kompetensi.dan akan segera mendapatkan pembayaran Biling PNBP di email anda.',
                    'status' => $statusPerpanjangan,
                    'created_by' => 'System',
                    'created_date' => $datetime,
                ];
                $this->db->insert(self::__tableName2, $data2);

                $this->db->update('tbl_user', ['no_reg' => $noReg], ['id_user' => $checkValid->id_user]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function editPnbp($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-perpanjangan-approve';
        $data['title'] = 'Data PNBP';
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data Approve Perpanjangan</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_perpanjangan->selectById($id, ['Approve']);
            if ($brand != null) {
                $data['brand'] = $brand;
                $data['exportPdf'] = "<a href=" . base_url('eksport-surat-pnbp-perpanjangan') . "/" . $brand->id_perpanjangan . " class='btn default btn-sm klik'><i class='fa fa-file-pdf-o'></i> Export PDF</a>";
                $this->loadkonten(self::__folder . 'update_pnbp', $data);
            } else {
                echo "<script>alert('Data Approve Perpanjangan tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function prosesUpdatePnbp($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $noSurat = $this->input->post('no_surat');
        $tglSurat = $this->input->post('tgl_surat');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_perpanjangan->selectById($id, ['Approve']);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noSurat) == 0) {
                $errCode++;
                $errMessage = "No Surat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglSurat) == 0) {
                $errCode++;
                $errMessage = "Tanggal Surat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'no_surat' => $noSurat,
                    'tgl_surat' => date('Y-m-d', strtotime($tglSurat)),
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                $this->db->update(self::__tableName, $data, [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function uploadPnbp($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-perpanjangan-approve';
        $data['title'] = 'Data PNBP';
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data Approve Perpanjangan</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_perpanjangan->selectById($id, ['Approve']);
            if ($brand != null) {
                $data['brand'] = $brand;
                $this->loadkonten(self::__folder . 'upload_pnbp', $data);
            } else {
                echo "<script>alert('Data Approve Perpanjangan tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function prosesUpload($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_perpanjangan->selectById($id, ['Approve']);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $path['link'] = "upload/berkas_pembayaran/" . date('Ymd') . "/";

                $config['upload_path'] = "./" . $path['link'];
                $config['allowed_types'] = 'pdf';
                $config['max_size'] = '2048'; //maksimum besar file 2M
                $config['encrypt_name'] = TRUE;
                $config['overwrite'] = TRUE;

                $this->load->library('upload', $config);

                $this->upload->do_upload('file_pnbp');
                $dataFilePnbp = $this->upload->data();

                $this->upload->do_upload('file_perintah');
                $dataFilePerintah = $this->upload->data();

                $data = [
                    'nama_file_pnbp' => $dataFilePnbp['file_name'],
                    'file_pnbp' => $path['link'] . '' . $dataFilePnbp['file_name'],
                    'nama_file_perintah' => $dataFilePerintah['file_name'],
                    'file_perintah' => $path['link'] . '' . $dataFilePerintah['file_name'],
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                $this->db->update(self::__tableName, $data, [self::__tableId => $id]);

                $data2 = [
                    'kode_pengajuan' => $checkValid->kode_pengajuan,
                    'id_user' => $checkValid->id_user,
                    'email' => $checkValid->email,
                    'status' => 'Belum Lunas',
                    'keterangan' => 'Pembayaran pengajuan perpanjangan Sertifikasi Komptensi' . $checkValid->jenis_perpanjangan . '',
                    'nama_file_pnbp' => $dataFilePnbp['file_name'],
                    'file_pnbp' => $path['link'] . '' . $dataFilePnbp['file_name'],
                    'nama_file_perintah' => $dataFilePerintah['file_name'],
                    'file_perintah' => $path['link'] . '' . $dataFilePerintah['file_name'],
                    'created_by' => 'System',
                    'created_date' => $datetime,
                ];
                $result = $this->db->insert('tbl_bukti_pembayaran', $data2);

                $AttachFilePnbp = $path['link'] . '' . $dataFilePnbp['file_name'];
                $AttachFilePerintah = $path['link'] . '' . $dataFilePerintah['file_name'];
                $DataSend = [$AttachFilePerintah, $AttachFilePnbp];

                $configEmail = [
                    'useragent' => 'Codeigniter',
                    'protocol' => 'smtp',
                    'smtp_host' => 'smtp.gmail.com',
                    'smtp_port' => 587,
                    'smtp_crypto' => 'tls',
                    'smtp_timeout' => 100,
                    'smtp_user' => 'angkasamuda20@gmail.com',
                    'smtp_pass' => 'agkasamuda2020',
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'newline' => "\r\n"
                ];

                $this->load->library('email');
                $this->email->initialize($configEmail);

                $this->email->from('angkasamuda20@gmail.com');
                $this->email->to($checkValid->email);

                $this->email->subject('Notifikasi Informasi Pembayaran Biling PNBP');
                $message = '
			<h3 align="center">Info Pembayaran Billing PNBP</h3>
			<center><table border="1" width="50%" cellpadding="5">
			<tr>
			<td width="30%">No Kode Pengajuan</td>
			<td width="70%">' . $this->input->post("kode_pengajuan") . '</td>
			</tr>

			<tr>
			<td width="30%">Keterangan</td>
			<td width="40%">Informasi tagihan pembayaran Biling PNBP</td>
			</tr>

			</table></center>';

                $this->email->message($message);
                foreach ($DataSend as $InputMail) {
                    $this->email->attach($InputMail);
                }
                $this->email->send();
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function suratPnbpPdf($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-perpanjangan-approve';
        $data['title'] = self::__title;
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_perpanjangan->selectById($id, ['Approve']);
            if ($brand != null) {
                $data['datamaster'] = $brand;
                $judulSurat = "Surat Perintah Bayar PNBP " . $brand->created_by . " " . date("d-m-Y h:i") . "";
                $this->pdf->load_view('v_perpanjangan/surat_pnbp_pdf', $data);
                $this->pdf->render();
                $this->pdf->stream('' . $judulSurat . '.pdf');
            } else {
                echo "<script>alert('Data Perpanjangan tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function suratPnbp($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-perpanjangan-approve';
        $data['title'] = self::__title;
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_perpanjangan->selectById($id, ['Approve']);
            if ($brand != null) {
                $data['datamaster'] = $brand;
                $this->load->view('' . self::__folder . 'surat_pnbp', $data);
            } else {
                echo "<script>alert('Data Perpanjangan tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function Cetak($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-perpanjangan-approve';
        $data['title'] = self::__title;
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_perpanjangan->selectById($id, ['Approve']);
            if ($brand != null) {
                $data['datamaster'] = $brand;
                $this->load->view('' . self::__folder . 'print', $data);
            } else {
                echo "<script>alert('Data Perpanjangan tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function download()
    {
        if ($this->input->post('berkas')) {
            $berkas = $this->input->post('berkas');
            foreach ($berkas as $res) {
                $this->zip->read_file($res);
            }
            $this->zip->download('berkas-perpanjangan' . time() . '.zip');
        }
    }

    public function prosesDelete()
    {
        $errCode = 0;
        $errMessage = "";

        $id = $_POST[self::__tableId];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_perpanjangan->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->load->helper("file");
                delete_files('upload/berkas_perpanjangan/' . $checkValid->folder, true, false, 1);

                $result = $this->db->update(self::__tableName, ['deleted_date' => date('Y-m-d H:i:s')], [self::__tableId => $id]);
                $result = $this->db->update(self::__tableName2, ['deleted_date' => date('Y-m-d H:i:s')], ['kode_pengajuan' => $checkValid->kode_pengajuan]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di hapus'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
