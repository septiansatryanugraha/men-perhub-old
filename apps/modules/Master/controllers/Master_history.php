<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_history extends AUTH_Controller {

	const __tableName = 'tbl_history';
	const __kode_menu = 'histori';
	const  __folder   = 'v_history/';
	const __title     = 'Histori Data Pengajuan';
	const __model     = 'M_history' ;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(self::__model);
		$this->load->model('M_sidebar');
	}
	
	public function loadkonten($page, $data) {
		
		$data['userdata'] 	= $this->userdata;
		$ajax = ($this->input->post('status_link') == "ajax" ? true : false);
		if (!$ajax) { 
			$this->load->view('Dashboard/layouts/header', $data);
		}
		$this->load->view($page, $data);
		if (!$ajax) $this->load->view('Dashboard/layouts/footer', $data);
	}

	public function index()
	{
		$accessAdd = $this->M_sidebar->access('add',self::__kode_menu);
        $data['accessAdd']  = $accessAdd->menuview;
        $data['title'] 		= self::__title;
		$data['userdata'] 	= $this->userdata; 
		$data['breadcrumb'] = '	<li><i class="fa fa-angle-right"></i><a class="ajaxify" href="#">History</a></li>';
		
		$this->loadkonten(''.self::__folder.'home',$data);
	}

	
	public function histori_list()
	{
		$tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = array(
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        );
        
		$accessEdit = $this->M_sidebar->access('edit',self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del',self::__kode_menu);
		$list = $this->M_history->getData(1,$filter);

		$data = array();
		$no = $_POST['start'];
		foreach ($list as $brand) {

			$idUser = $brand->id_user;
            $dataUser = $this->M_history->select_user($idUser);
            $namaUser = $dataUser->nama_lengkap;

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $brand->kode_pengajuan;
			$row[] = $namaUser;
			$row[] = $brand->status;
			$row[] = ''.$brand->keterangan_status.'<br><b>'.date('d-m-Y H:i:s', strtotime($brand->created_date)).'</b>';

			$row[] = $buttonEdit . '  ' . $buttonDel;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	
	

}
