<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Informasi extends AUTH_Controller
{
    const __tableName = 'tbl_broadcast';
    const __tableId = 'id_broadcast';
    const __folder = 'v_informasi/';
    const __kode_menu = 'informasi';
    const __title = 'Informasi ';
    const __model = 'M_informasi';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
        $this->load->model('M_utilities');
    }

    public function index()
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $this->loadkonten(self::__folder . 'home', $data);
        }
    }

    public function ajaxList()
    {
        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);

        $list = $this->M_informasi->getData();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->judul;
            $row[] = $brand->konten;

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-informasi') . "/" . $brand->id_broadcast . "' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-informasi' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_broadcast . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function Add()
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">Data " . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $this->loadkonten(self::__folder . 'tambah', $data);
        }
    }

    public function prosesAdd()
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $judul = $this->input->post('judul');
        $konten = $this->input->post('konten');
        $isSendEmail = $this->input->post('is_send_email');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($judul) == 0) {
                $errCode++;
                $errMessage = "Judul wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($konten) == 0) {
                $errCode++;
                $errMessage = "Konten wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($isSendEmail) == 0) {
                $errCode++;
                $errMessage = "Kirim Email wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'judul' => $judul,
                    'konten' => $konten,
                    'created_by' => $username,
                    'created_date' => $datetime,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                $result = $this->db->insert(self::__tableName, $data);

                if ($isSendEmail > 0) {
                    $data = $this->M_utilities->selectEmail();
                    foreach ($data as $key => $value) {
                        $dataEmail[] = $value->email;
                    }

                    $judul = $this->input->post('judul');
                    $konten = $this->input->post('konten');
                    $web = "http://localhost/men-perhub/";
                    $link_web = '<a href="' . $web . '">Disini</a>';

                    $config = [
                        'useragent' => 'Codeigniter',
                        'protocol' => 'smtp',
                        'smtp_host' => 'smtp.gmail.com',
                        'smtp_port' => 587,
                        'smtp_crypto' => 'tls',
                        'smtp_timeout' => 100,
                        'smtp_user' => 'angkasamuda20@gmail.com',
                        'smtp_pass' => 'agkasamuda2020',
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'newline' => "\r\n"
                    ];

                    $this->load->library('email');
                    $this->email->initialize($config);

                    $this->email->from('angkasamuda20@gmail.com');
                    $this->email->to($dataEmail);

                    $this->email->subject($judul);
                    $message = '' . $konten . '
                <br><br><br>
                Best regards,<br><br><br>
                <b><font color="black">Admin E-Kopetensi</font></b><br>';
                    $this->email->message($message);
                    $this->email->send();
                }
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">Data " . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_informasi->selectById($id);
            if ($brand != null) {
                $data['brand'] = $brand;
                $this->loadkonten(self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $judul = $this->input->post('judul');
        $konten = $this->input->post('konten');
        $isSendEmail = $this->input->post('is_send_email');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_informasi->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($judul) == 0) {
                $errCode++;
                $errMessage = "Judul wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($konten) == 0) {
                $errCode++;
                $errMessage = "Konten wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($isSendEmail) == 0) {
                $errCode++;
                $errMessage = "Kirim Email wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'judul' => $judul,
                    'konten' => $konten,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                $result = $this->db->update(self::__tableName, $data, [self::__tableId => $id]);

                if ($isSendEmail > 0) {
                    $data = $this->M_utilities->selectEmail();
                    foreach ($data as $key => $value) {
                        $dataEmail[] = $value->email;
                    }

                    $judul = $this->input->post('judul');
                    $konten = $this->input->post('konten');
                    $web = "http://localhost/men-perhub/";
                    $link_web = '<a href="' . $web . '">Disini</a>';

                    $config = [
                        'useragent' => 'Codeigniter',
                        'protocol' => 'smtp',
                        'smtp_host' => 'smtp.gmail.com',
                        'smtp_port' => 587,
                        'smtp_crypto' => 'tls',
                        'smtp_timeout' => 100,
                        'smtp_user' => 'angkasamuda20@gmail.com',
                        'smtp_pass' => 'agkasamuda2020',
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'newline' => "\r\n"
                    ];

                    $this->load->library('email');
                    $this->email->initialize($config);

                    $this->email->from('angkasamuda20@gmail.com');
                    $this->email->to($dataEmail);

                    $this->email->subject($judul);
                    $message = '' . $konten . '
                <br><br><br>
                Best regards,<br><br><br>
                <b><font color="black">Admin E-Kopetensi</font></b><br>';
                    $this->email->message($message);
                    $this->email->send();
                }
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $errCode = 0;
        $errMessage = "";

        $id = $_POST[self::__tableId];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_informasi->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $result = $this->db->update(self::__tableName, ['deleted_date' => date('Y-m-d H:i:s')], [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di hapus'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
