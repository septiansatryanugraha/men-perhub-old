<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_jenjang extends AUTH_Controller
{
    const __tableName = 'tbl_kenaikan_jenjang';
    const __tableName2 = 'tbl_history';
    const __tableId = 'id_jenjang';
    const __folder = 'v_jenjang/';
    const __kode_menu = 'master-jenjang';
    const __kode_menu2 = 'master-jenjang-validasi';
    const __kode_menu3 = 'master-jenjang-approve';
    const __title = 'Master Kenaikan Jenjang ';
    const __title3 = 'Master Kenaikan Jenjang';
    const __model = 'M_jenjang';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
        $this->load->model('M_utilities');
    }

    public function index()
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $this->loadkonten('' . self::__folder . 'home', $data);
        }
    }

    public function ajaxList()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = [
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        ];

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_jenjang->getData(1, $filter, ['Pending', 'Proses Verifikasi']);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $status = '<span class="badge bg-red">Status Pending</span>';
            if ($brand->status_pengajuan == 'Proses Verifikasi') {
                $status = '<span class="badge bg-blue">Proses Verifikasi</span>';
            } else if ($brand->status_pengajuan == 'Approve') {
                $status = '<span class="badge bg-green">Approve</span>';
            }

            if ($brand->updated_by == NULL) {
                $proses = "Belum ada yang memproses";
            } else {
                $proses = $brand->updated_by;
            }

            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->kode_pengajuan;
            $row[] = $brand->nama_pemohon;
            $row[] = $brand->email;
            $row[] = $brand->status_kepegawaian_pemohon;
            $row[] = $status . '<br> ' . $brand->status . '</br>' . date('d-m-Y', strtotime($brand->updated_date)) . '<br> Pengajuan Kenaikan Jenjang di Proses oleh : <b>' . $proses . '</b>';

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($brand->status_pengajuan != 'Approve' && $accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-jenjang') . "/" . $brand->id_jenjang . "' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-jenjang' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_jenjang . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function Edit($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data " . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_jenjang->selectById($id, ['Pending', 'Proses Verifikasi']);
            if ($brand != null) {
                $data['brand'] = $brand;
                $data['status'] = $this->M_utilities->selectStatusGrup([], ['Proses Verifikasi']);
                $data["berkas"] = glob('upload/berkas_peningkatan_jenjang/' . $brand->folder . "/*");
                $this->loadkonten(self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $statusPengajuan = $this->input->post('status_pengajuan');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_jenjang->selectById($id, ['Pending', 'Proses Verifikasi']);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($statusPengajuan) == 0) {
                $errCode++;
                $errMessage = "Status Pengajuan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'keterangan_status' => 'Pengajuan user <b>' . $checkValid->nama_pemohon . '</b> untuk proses Pengajuan Kenaikan Jenjang ' . $checkValid->jenis_jenjang . ' Sedang di ' . $statusPengajuan . ' oleh admin sistem E-Kompetensi',
                    'status_pengajuan' => $statusPengajuan,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                $result = $this->db->update(self::__tableName, $data, [self::__tableId => $id]);

                $data2 = [
                    'kode_pengajuan' => $checkValid->kode_pengajuan,
                    'id_user' => $checkValid->id_user,
                    'keterangan_status' => 'Pengajuan user <b>' . $checkValid->nama_pemohon . '</b> untuk proses Pengajuan Kenaikan Jenjang ' . $checkValid->jenis_jenjang . ' Sedang di ' . $statusPengajuan . ' oleh admin sistem E-Kompetensi',
                    'status' => $statusPengajuan,
                    'created_by' => 'System',
                    'created_date' => $datetime,
                ];
                $result = $this->db->insert(self::__tableName2, $data2);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function homeValidasi()
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-jenjang-validasi';
        $data['title'] = "Validasi Kenaikan Jenjang";
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $this->loadkonten('' . self::__folder . 'home_validasi', $data);
        }
    }

    public function ajaxValidasi()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = [
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        ];

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu2);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu2);
        $list = $this->M_jenjang->getData(1, $filter, ['Proses Verifikasi', 'Proses Validasi', 'Belum Validasi']);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $status = '<span class="badge bg-blue">Proses Verifikasi</span>';
            if ($brand->status_pengajuan == 'Proses Validasi') {
                $status = '<span class="badge bg-blue">Proses Validasi</span>';
            } else if ($brand->status_pengajuan == 'Belum Validasi') {
                $status = '<span class="badge bg-red">Belum Validasi</span>';
            }

            if ($brand->updated_by == NULL) {
                $proses = "Belum ada yang memproses";
            } else {
                $proses = $brand->updated_by;
            }

            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->kode_pengajuan;
            $row[] = $brand->nama_pemohon;
            $row[] = $brand->email;
            $row[] = $brand->status_kepegawaian_pemohon;
            $row[] = $status . '<br> ' . $brand->status . '</br>' . date('d-m-Y', strtotime($brand->updated_date)) . '<br>Pengajuan Kenaikan Jenjang di Proses oleh : <b>' . $proses . '</b>';

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($brand->status_pengajuan != 'Approve' && $accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-jenjang-validasi') . "/" . $brand->id_jenjang . "' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-jenjang' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_jenjang . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function editValidasi($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-jenjang-validasi';
        $data['title'] = "Validasi Kenaikan Jenjang";
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data " . $data['title'] . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_jenjang->selectById($id, ['Proses Verifikasi', 'Proses Validasi', 'Belum Validasi']);
            if ($brand != null) {
                $data['brand'] = $brand;
                $data['status'] = $this->M_utilities->selectStatusGrup([], ['Belum Validasi', 'Proses Validasi']);
                $data["berkas"] = glob('upload/berkas_peningkatan_jenjang/' . $brand->folder . "/*");
                $this->loadkonten(self::__folder . 'update_validasi', $data);
            } else {
                echo "<script>alert('Data " . $data['title'] . " tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function prosesUpdateValidasi($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $noReg = $this->input->post('no_reg');
        $noBa = $this->input->post('no_ba');
        $tglBa = $this->input->post('tgl_ba');
        $noSertifikat = $this->input->post('no_sertifikat');
        $statusPengajuan = $this->input->post('status_pengajuan');
        $catatan = $this->input->post('catatan');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_jenjang->selectById($id, ['Proses Verifikasi', 'Proses Validasi', 'Belum Validasi']);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noReg) == 0) {
                $errCode++;
                $errMessage = "No registrasi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noBa) == 0) {
                $errCode++;
                $errMessage = "No Berita Acara wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglBa) == 0) {
                $errCode++;
                $errMessage = "Tanggal Berita Acara wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noSertifikat) == 0) {
                $errCode++;
                $errMessage = "Tanggal Sertifikat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($statusPengajuan) == 0) {
                $errCode++;
                $errMessage = "Status Pengajuan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                if ($statusPengajuan == 'Proses Validasi') {
                    $keteranganStatus = 'Pengajuan user <b>' . $checkValid->nama_pemohon . '</b> untuk proses Kenaikan Jenjang  ' . $checkValid->jenis_jenjang . ' sedang di ' . $statusPengajuan . ' oleh admin sistem Kompetensi';
                } else {
                    $keteranganStatus = 'Pengajuan user <b>' . $checkValid->nama_pemohon . '</b> untuk proses Kenaikan Jenjang ' . $checkValid->jenis_jenjang . ' ' . $statusPengajuan . ' oleh admin sistem E-Kompetensi .karena ada beberapa dokumen yang belum memenuhi syarat.';
                }

                $data = [
                    'no_reg' => $noReg,
                    'no_ba' => $noBa,
                    'tgl_ba' => date('Y-m-d', strtotime($tglBa)),
                    'no_sertifikat' => $noSertifikat,
                    'catatan' => $catatan,
                    'keterangan_status' => $keteranganStatus,
                    'status_pengajuan' => $statusPengajuan,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                $this->db->update(self::__tableName, $data, [self::__tableId => $id]);

                $data2 = [
                    'kode_pengajuan' => $checkValid->kode_pengajuan,
                    'id_user' => $checkValid->id_user,
                    'keterangan_status' => $keteranganStatus,
                    'status' => $statusPengajuan,
                    'created_by' => 'System',
                    'created_date' => $datetime,
                ];
                $this->db->insert(self::__tableName2, $data2);

                $this->db->update('tbl_user', ['no_reg' => $noReg], ['id_user' => $checkValid->id_user]);

                if ($statusPengajuan != 'Proses Validasi') {
                    $config = [
                        'useragent' => 'Codeigniter',
                        'protocol' => 'smtp',
                        'smtp_host' => 'smtp.gmail.com',
                        'smtp_port' => 587,
                        'smtp_crypto' => 'tls',
                        'smtp_timeout' => 100,
                        'smtp_user' => 'angkasamuda20@gmail.com',
                        'smtp_pass' => 'agkasamuda2020',
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'newline' => "\r\n"
                    ];

                    $this->load->library('email');
                    $this->email->initialize($config);

                    $this->email->from('angkasamuda20@gmail.com');
                    $this->email->to($checkValid->email);

                    $this->email->subject('Notifikasi Pengajuan Diklat PKB');
                    $message = '' . $catatan . '
				<br><br><br>
				Best regards,<br><br><br>
				<b><font color="black">Admin E-Kopetensi</font></b><br>
				Kementerian Perhubungan Direktorat Jendral Perhubungan Darat';
                    $this->email->message($message);
                    $this->email->send();
                }
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function homeApprove()
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-jenjang-approve';
        $data['title'] = "Approve Kenaikan Jenjang";
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $this->loadkonten('' . self::__folder . 'home_approve', $data);
        }
    }

    public function ajaxApprove()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = [
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        ];

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu3);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu3);
        $list = $this->M_jenjang->getData(1, $filter, ['Proses Validasi', 'Lolos']);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $status = '<span class="badge bg-blue">Proses Validasi</span>';
            if ($brand->status_pengajuan == 'Lolos') {
                $status = '<span class="badge bg-green">Lolos Validasi</span>';
            }

            $statusKelulusan = '<span class="badge bg-blue">Belum Diketahui</span>';
            if ($brand->status_kelulusan == 'Lulus') {
                $statusKelulusan = '<span class="badge bg-green">Lulus</span> dengan nilai ' . $brand->nilai . ' ';
            } else if ($brand->status_kelulusan == 'Tidak Lulus') {
                $statusKelulusan = '<span class="badge bg-red">Tidak Lulus</span> dengan nilai ' . $brand->nilai . ' ';
            }

            if ($brand->updated_by == NULL) {
                $proses = "Belum ada yang memproses";
            } else {
                $proses = $brand->updated_by;
            }

            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->kode_pengajuan;
            $row[] = $brand->nama_pemohon . '<br><br> Status kelulusan : ' . $statusKelulusan . '';
            $row[] = $brand->email;
            $row[] = $brand->status_kepegawaian_pemohon;
            $row[] = $status . '<br> ' . $brand->status . '</br>' . date('d-m-Y', strtotime($brand->updated_date)) . '<br>Pengajuan Kenaikan Jenjang di Proses oleh : <b>' . $proses . '</b>';

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-jenjang-approve') . "/" . $brand->id_jenjang . "' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($brand->status_kelulusan == 'Lulus') {
                if ($accessEdit->menuview > 0) {
                    $action .= "    <li><a href='" . base_url('view-nilai-jenjang') . "/" . $brand->id_jenjang . "' class='klik ajaxify'><i class='fa fa-eye'></i> Lihat Nilai</a></li>";
                    $action .= "    <li><a href='" . base_url('cetak-sertifikat-kenaikan') . "/" . $brand->id_jenjang . "' class='klik ajaxify' target='__blank'><i class='fa fa-print'></i> Print</a></li>";
                }
            } else {
                if ($brand->status_pengajuan == 'Lolos') {
                    $action .= "    <li><a href='" . base_url('edit-nilai-jenjang') . "/" . $brand->id_jenjang . "' class='klik ajaxify'><i class='fa fa-tags'></i> Ubah Nilai Diklat</a></li>";
                }
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-jenjang' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_jenjang . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function editApprove($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-jenjang-approve';
        $data['title'] = "Approve Kenaikan Jenjang";
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data " . $data['title'] . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_jenjang->selectById($id, ['Proses Validasi', 'Lolos']);
            if ($brand != null) {
                $data['brand'] = $brand;
                $data['status'] = $this->M_utilities->selectStatusGrup([], ['Lolos']);
                $data["berkas"] = glob('upload/berkas_peningkatan_jenjang/' . $brand->folder . "/*");
                $this->loadkonten(self::__folder . 'update_approve', $data);
            } else {
                echo "<script>alert('Data " . $data['title'] . " tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function prosesUpdateApprove($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $noReg = $this->input->post('no_reg');
        $noBa = $this->input->post('no_ba');
        $tglBa = $this->input->post('tgl_ba');
        $noSertifikat = $this->input->post('no_sertifikat');
        $statusPengajuan = $this->input->post('status_pengajuan');
        $catatan = $this->input->post('catatan');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_jenjang->selectById($id, ['Proses Validasi', 'Lolos']);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noReg) == 0) {
                $errCode++;
                $errMessage = "No registrasi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noBa) == 0) {
                $errCode++;
                $errMessage = "No Berita Acara wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglBa) == 0) {
                $errCode++;
                $errMessage = "Tanggal Berita Acara wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noSertifikat) == 0) {
                $errCode++;
                $errMessage = "Tanggal Sertifikat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($statusPengajuan) == 0) {
                $errCode++;
                $errMessage = "Status Pengajuan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'no_reg' => $noReg,
                    'no_ba' => $noBa,
                    'tgl_ba' => date('Y-m-d', strtotime($tglBa)),
                    'no_sertifikat' => $noSertifikat,
                    'catatan' => $catatan,
                    'keterangan_status' => 'user <b>' . $checkValid->nama_pemohon . '</b> untuk proses Pengajuan Kenaikan Jenjang ' . $checkValid->jenis_jenjang . ' telah di nyatakan ' . $statusPengajuan . ' oleh admin sistem Kompetensi.dan akan dimasukan ke daftar list calon peserta uji kompetensi.',
                    'status_pengajuan' => $statusPengajuan,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                $this->db->update(self::__tableName, $data, [self::__tableId => $id]);

                $data2 = [
                    'kode_pengajuan' => $checkValid->kode_pengajuan,
                    'id_user' => $checkValid->id_user,
                    'keterangan_status' => 'user <b>' . $checkValid->nama_pemohon . '</b> untuk proses Pengajuan Kenaikan Jenjang ' . $checkValid->jenis_jenjang . ' telah di nyatakan ' . $statusPengajuan . ' oleh admin sistem Kompetensi.dan akan dimasukan ke daftar list calon peserta uji kompetensi.',
                    'status' => $statusPengajuan,
                    'created_by' => 'System',
                    'created_date' => $datetime,
                ];
                $this->db->insert(self::__tableName2, $data2);

                $this->db->update('tbl_user', ['no_reg' => $noReg], ['id_user' => $checkValid->id_user]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function editNilai($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-jenjang-approve';
        $data['title'] = "Nilai Kenaikan Jenjang";
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data " . $data['title'] . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_jenjang->selectById($id, ['Lolos']);
            if ($brand != null) {
                $data['brand'] = $brand;
                $data['ValData'] = $this->M_jenjang->selectHistoryJenjang(['id_user' => $brand->id_user])->result();
                $this->loadkonten(self::__folder . 'edit_nilai', $data);
            } else {
                echo "<script>alert('Data " . $data['title'] . " tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function prosesUpdateNilai($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";
        $jenisJenjang = "";
        $KodeJenjang = "";
        $IdJenjang = "";

        $ne1 = $this->input->post('ne1');
        $na1 = $this->input->post('na1');
        $ne2 = $this->input->post('ne2');
        $na2 = $this->input->post('na2');
        $ne3 = $this->input->post('ne3');
        $na3 = $this->input->post('na3');
        $nilai = $this->input->post('nilai');
        $statusKelulusan = $this->input->post('status_kelulusan');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_jenjang->selectById($id, ['Lolos']);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($ne1) == 0) {
                $errCode++;
                $errMessage = "Nilai wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($ne2) == 0) {
                $errCode++;
                $errMessage = "Nilai wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($ne3) == 0) {
                $errCode++;
                $errMessage = "Nilai wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'nilai' => $nilai,
                    'status_kelulusan' => $statusKelulusan,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                if ($statusKelulusan == 'Lulus') {
                    $jenisJenjang = $checkValid->jenis_jenjang;
                    if ($jenisJenjang == 'Penguji Pemula') {
                        $KodeJenjang = 'PP2';
                        $IdJenjang = '2';
                    } elseif ($jenisJenjang == 'Penguji Tingkat Dua') {
                        $KodeJenjang = 'PT2';
                        $IdJenjang = '4';
                    } elseif ($jenisJenjang == 'Penguji Tingkat Empat') {
                        $KodeJenjang = 'PT4';
                        $IdJenjang = '6';
                    } elseif ($jenisJenjang == 'Master Penguji') {
                        $KodeJenjang = 'PMT';
                        $IdJenjang = '8';
                    }
                }
                $result = $this->db->update(self::__tableName, $data, [self::__tableId => $id]);

                $data2 = [
                    'id_user' => $checkValid->id_user,
                    'kode_pengajuan' => $checkValid->kode_pengajuan,
                    'id_jenis_jenjang' => $checkValid->id_jenis_jenjang,
                    'ne1' => $ne1,
                    'na1' => $na1,
                    'ne2' => $ne2,
                    'na2' => $na2,
                    'ne3' => $ne3,
                    'na3' => $na3,
                    'kumulatif' => $nilai,
                    'status' => $statusKelulusan,
                    'created_by' => 'System',
                    'created_date' => $datetime,
                ];
                $result = $this->db->insert('tbl_history_jenjang', $data2);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function lihatNilai($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-jenjang-approve';
        $data['title'] = 'Nilai Kenaikan Jenjang';
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data " . $data['title'] . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_jenjang->selectById($id, ['Lolos']);
            if ($brand != null) {
                $data['brand'] = $this->M_jenjang->selectHistoryJenjang(['kode_pengajuan' => $brand->kode_pengajuan])->row();
                $this->loadkonten(self::__folder . 'lihat_nilai', $data);
            } else {
                echo "<script>alert('Data " . $data['title'] . " tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function Cetak($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-jenjang-approve';
        $data['title'] = 'Nilai Kenaikan Jenjang';
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data " . $data['title'] . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_jenjang->selectById($id, ['Lolos']);
            if ($brand != null) {
                $data['datamaster'] = $brand;
                $this->load->view(self::__folder . 'print', $data);
            } else {
                echo "<script>alert('Data " . $data['title'] . " tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function download()
    {
        if ($this->input->post('berkas')) {
            $berkas = $this->input->post('berkas');
            foreach ($berkas as $res) {
                $this->zip->read_file($res);
            }
            $this->zip->download('berkas-jenjang' . time() . '.zip');
        }
    }

    public function prosesDelete()
    {
        $errCode = 0;
        $errMessage = "";

        $id = $_POST[self::__tableId];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_jenjang->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->load->helper("file");
                delete_files('upload/berkas_peningkatan_jenjang/' . $checkValid->folder, true, false, 1);

                $result = $this->db->update(self::__tableName, ['deleted_date' => date('Y-m-d H:i:s')], [self::__tableId => $id]);
                $result = $this->db->update(self::__tableName2, ['deleted_date' => date('Y-m-d H:i:s')], ['kode_pengajuan' => $checkValid->kode_pengajuan]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di hapus'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
