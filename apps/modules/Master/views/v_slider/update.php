<style type="text/css">
    #btn_loading {
        display: none;
    }
    .space {
        margin-top: 8px;
    }
</style>
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        Ubah <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i>Ubah <?= $title ?></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-9"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-pencil-square"></i> Data <?= $title ?></div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <form class="form-horizontal" id="form-update" method="POST" enctype="multipart/form-data">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Judul </label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="judul" id="judul" aria-describedby="sizing-addon2" value="<?= $brand->judul; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Deskripsi Singkat</label>
                                <div class="col-sm-6">
                                    <textarea id="summernote" name="deskripsi"><?= $brand->deskripsi; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div id="slider">
                                    <img class="img-thumbnail" src='<?= (strlen($brand->gambar) > 0) ? base_url('slider') . '/../' . $brand->gambar : base_url() . '/assets/tambahan/gambar/tidak-ada.png'; ?>'>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputFoto" class="col-sm-2 control-label">Gambar</label>
                                <div class="col-sm-6">
                                    <input type="file" class="form-control" name="gambar" id="gambar"onchange="return CheckSlide()"/></div>
                                <div class="col-sm-2 space">
                                    <small class="label pull-center bg-red">max 1 Mb </small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 control-label">Urutan</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" name="urutan" placeholder="Urutan slider" id="judul" value="<?= $brand->urutan; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-2">
                                    <select name="status" class="form-control select-status" id="status" aria-describedby="sizing-addon2">
                                        <?php foreach ($datastatus as $status) { ?>
                                            <option value="<?= $status->nama; ?>"<?php
                                            if ($status->nama == $brand->status) {
                                                echo "selected='selected'";
                                            }

                                            ?>><?= $status->nama; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <div id="buka">
                                        <button type="submit" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                                        <a href="<?= base_url($page); ?>" type="button" class="btn red ajaxify klik">Kembali</a>
                                    </div>
                                    <div id="btn_loading">
                                        <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>             
                </div>
            </div>
        </div> 
    </div>
</div> 

<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var error = 0;
        var message = "";

        if (error == 0) {
            var judul = $("#judul").val();
            var judul = judul.trim();
            if (judul.length == 0) {
                error++;
                message = "Judul wajib di isi.";
            }
        }
        if (error == 0) {
            var konten = $("#summernote").val();
            var konten = konten.trim();
            if (konten.length == 0) {
                error++;
                message = "Konten wajib di isi.";
            }
        }
        if (error == 0) {
            var status = $("#status").val();
            var status = status.trim();
            if (status.length == 0) {
                error++;
                message = "Status slider harus di isi.";
            }
        }
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?= base_url('update-slider') . '/' . $brand->id; ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                $("#buka").show();
                $("#btn_loading").hide();
                if (result.status == true) {
                    setTimeout("window.location='<?= base_url($page); ?>'", 500);
                    toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                } else {
                    setTimeout(location.reload.bind(location), 500);
                    toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                }
            })
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });

    //untuk live gambar ajax slider 
    function CheckSlide() {
        var fileInput = document.getElementById('gambar');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        if (!allowedExtensions.exec(filePath)) {
            toastr.error('maaf masukan gambar dengan format .jpeg/.jpg/.png/.gif only.', 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            fileInput.value = '';
            return false;
        } else {
            //Image preview
            if (fileInput.files && fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById('slider').innerHTML = '<img src="' + e.target.result + '" width="150" height="auto"/>';
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        }

        // validasi ukuran size file
        var ukuran = document.getElementById("gambar");
        if (ukuran.files[0].size > 1007200) {
            toastr.error('File harus maksimal 1 MB', 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            ukuran.value = '';
//            setTimeout(location.reload.bind(location), 500);
            return false;
        }
    }

    $(document).ready(function () {
        $('#summernote').summernote({
            height: 150
        });
    });

    $(function () {
        $(".select-status").select2({
            placeholder: " -- pilih status -- "
        });
    });
</script>