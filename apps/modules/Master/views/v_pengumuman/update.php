<style type="text/css">
    #btn_loading {
        display: none;
    }
</style>
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        Ubah <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i>Ubah <?= $title ?></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12"> 

            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-pencil-square"></i> Data <?= $title ?></div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <form class="form-horizontal" id="form-update" method="POST">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Jenis Surat </label>
                                <div class="col-sm-4">
                                    <select name="tipe_surat" id="tipe_surat" class="form-control select-tipe-surat" aria-describedby="sizing-addon2">
                                        <option></option>
                                        <option value="Surat Panggilan Diklat" <?= ($brand->tipe_surat == 'Surat Panggilan Diklat') ? "selected" : ""; ?>>Diklat PKB</option>
                                        <option value="Surat Panggilan Jenjang" <?= ($brand->tipe_surat == 'Surat Panggilan Jenjang') ? "selected" : ""; ?>>Kenaikan Jenjang</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Judul Informasi </label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="judul" id="judul" aria-describedby="sizing-addon2" value="<?= $brand->judul; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Konten Informasi</label>
                                <div class="col-sm-6">
                                    <textarea id="konten" name="konten" class="summernote"><?= $brand->konten; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <div id="buka">
                                        <button type="submit" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                                        <a href="<?= base_url($page); ?>" type="button" class="btn red ajaxify klik">Kembali</a>
                                    </div>
                                    <div id="btn_loading">
                                        <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>              
    </div>
</div>

<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-update').submit(function (e) {
        var error = 0;
        var message = "";

        if (error == 0) {
            var tipe_surat = $("#tipe_surat").val();
            var tipe_surat = tipe_surat.trim();
            if (tipe_surat.length == 0) {
                error++;
                message = "Jenis Surat wajib di isi.";
            }
        }
        if (error == 0) {
            var judul = $("#judul").val();
            var judul = judul.trim();
            if (judul.length == 0) {
                error++;
                message = "Judul wajib di isi.";
            }

        }
        if (error == 0) {
            var konten = $("#konten").val();
            var konten = konten.trim();
            if (konten.length == 0) {
                error++;
                message = "Konten wajib di isi.";
            }
        }
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?= base_url('update-pengumuman') . '/' . $brand->id_pengumuman; ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    setTimeout("window.location='<?= base_url($page); ?>'", 500);
                    toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                }
            })
            e.preventDefault();
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });

    $(document).ready(function () {
        $('.summernote').summernote({
            height: 200
        });
    });

    $(function () {
        $(".select-tipe-surat").select2({
            placeholder: " -- pilih -- "
        });
    });
</script>