<div class="page-content">
  
  
  <!-- BEGIN PAGE HEADER-->
  <h3 class="page-title">
    <?= (isset($title))?$title:''; ?>
    <small><?= (isset($subtitle))?$subtitle:''; ?></small>
  </h3>
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a class="ajaxify" href="<?= base_url() ?>Dashboard">Home</a>            
      </li>
      <?= (isset($breadcrumb))?$breadcrumb:''; ?>
    </ul>
    
  </div>
  
  <!--<br><br><br>-->

  <!-- /.box-header -->
  
  <div class="row">
    <div class="col-md-12">
      <div class="portlet box green">
        <div class="portlet-title">
          <div class="caption">
            <i class="fa fa-user"></i> History Pengajuan
          </div>
        </div>
        <div class="portlet-body">
         
         <div class="table-responsive">
           <table id="table" class="table table-striped table-bordered table-hover">
             <thead>
               <tr>
                 <th>#</th>
                 <th>Kode Pengajuan</th>
                 <th>Nama Pengaju</th>
                 <th>Status</th>
                 <th>Keterangan Status</th>
               </tr>
             </thead>
             <tbody>
             </tbody>
           </table>
           
         </div>
       </div>
     </div>
   </div>
   

 </div>
</div>




<script type="text/javascript">
    // untuk datepicker
    $(function () {
     $(".datepicker").datepicker({
       orientation: "left",
       autoclose: !0,
       format: 'dd-mm-yyyy'
     })
   });

    //untuk load data table ajax  
    var save_method; //for save method string
    var table;

    $(document).ready(function () {
      reloadTable();
    });

    function reloadTable() {
      var tanggal_awal = $("#tanggal_awal").val();
      var tanggal_akhir = $("#tanggal_akhir").val();

      table = $('#table').DataTable({
            "processing": true, //Feature control the processing indicator.
             // "scrollX": true,
            "order": [], //Initial no order.
            oLanguage: {
              "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
              "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
              "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
              "sInfoFiltered": "(difilter dari _MAX_ total data)",
              "sEmptyTable": "Data tidak ada di server",
              "sInfoPostFix": "",
              "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
              "sPaginationType": "simple_numbers",
              "sUrl": "",
              "oPaginate": {
                "sFirst": "Pertama",
                "sPrevious": "Sebelumnya",
                "sNext": "Selanjutnya",
                "sLast": "Terakhir"
              }
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
              "url": "<?= site_url('Master/Master_history/histori_list') ?>",
              "type": "POST",
              data: {tanggal_awal: tanggal_awal, tanggal_akhir: tanggal_akhir},
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                  },
                  ],
                });
    }

    $('#search-button').click(function () {
      $('.search-form').toggle();
      return false;
    });

    $("#button_filter").click(function () {
      table.destroy();
      reloadTable();
    });

  </script>






