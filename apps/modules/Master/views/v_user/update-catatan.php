<style type="text/css">
    #btn_loading {
        display: none;
    }
</style>
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        Ubah Catatan User
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i>Ubah Catatan User</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-user"></i> Ubah Catatan User</div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <form class="form-horizontal" id="form-update"  method="POST">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Subjek </label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="subject" aria-describedby="sizing-addon2" value="<?= $brand->subject; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">catatan</label>
                                <div class="col-sm-6">
                                    <textarea id="summernote" name="catatan"><?= $brand->catatan; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <div id="buka">
                                        <button type="submit" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                                        <a href="<?= base_url('master-user'); ?>" type="button" class="btn red ajaxify klik">Kembali</a>
                                    </div>
                                    <div id="btn_loading">
                                        <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>              
    </div>
</div>

<script type="text/javascript">
    $('#form-update').submit(function (e) {
        e.preventDefault();
        $.ajax({
            method: 'POST',
            beforeSend: function () {
                $("#buka").hide();
                $("#btn_loading").show();
            },
            url: '<?= base_url('update-catatan-user') . '/' . $brand->id_user; ?>',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
        }).done(function (data) {
            var result = jQuery.parseJSON(data);
            if (result.status == true) {
                $("#buka").show();
                $("#btn_loading").hide();
                setTimeout("window.location='<?= base_url('master-user'); ?>'", 500);
                toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
            } else {
                $("#buka").show();
                $("#btn_loading").hide();
                toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
            }
        })
    });

    function cekemail(a) {
        re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(a);
    }

    $(document).ready(function () {
        $('#summernote').summernote({
            height: 200
        });
    });
</script>