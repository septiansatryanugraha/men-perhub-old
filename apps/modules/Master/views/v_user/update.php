<style>
    .field-icon {
        float: left;
        margin-left: 93%;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }
    #btn_loading {
        display: none;
    }
    #loadingImg {
        margin-left: 27%;
    }
    /* The container */
    .container2 {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 22px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    /* Hide the browser's default checkbox */
    .container2 input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }
    /* Create a custom checkbox */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px;
        width: 20px;
        border-radius: 3px;
        background-color: #d4d2d2;
    }
    /* On mouse-over, add a grey background color */
    .container:hover input ~ .checkmark {
        background-color: #ccc;
    }
    /* When the checkbox is checked, add a blue background */
    .container2 input:checked ~ .checkmark {
        background-color: #2196F3;
    }
    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }
    /* Show the checkmark when checked */
    .container2 input:checked ~ .checkmark:after {
        display: block;
    }
    /* Style the checkmark/indicator */
    .container2 .checkmark:after {
        left: 7px;
        top: 4px;
        width: 5px;
        height: 11px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
</style>

<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i>Ubah <?= $title ?></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-7"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i> Data User
                    </div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <form class="form-horizontal" id="form-update" method="POST">
                        <input type="hidden"  name="id_user" value="<?= $brand->id_user; ?>">
                        <input type="hidden"  name="email" value="<?= $brand->email; ?>">
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="form-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static"><?= $brand->nama_lengkap; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Tempat Lahir</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir" id="tempat_lahir" onkeyup="javascript:capitalize(this.id, this.value);" value="<?= $brand->tempat_lahir; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Lahir</label>
                                <div class="col-sm-4">
                                    <input type="text" id="tgl_lahir" class="form-control tgl_lahir" name="tgl_lahir" placeholder="Tanggal Lahir" value="<?= date('d-m-Y', strtotime($brand->tgl_lahir)); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Jenis Kelamin</label>
                                <div class="col-sm-4">
                                    <select name="jkel" class="form-control select-jkel" id="jkel">
                                        <?php foreach ($jkel as $data) { ?>
                                            <option value="<?= $data->nama_kategori ?>" <?= ($data->nama_kategori == $brand->jkel) ? "selected" : ""; ?>>
                                                <?= $data->nama_kategori; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Status Kepegawaian</label>
                                <div class="col-sm-4">
                                    <select name="status_pegawai" class="form-control select-pegawai" id="status_pegawai">
                                        <?php foreach ($status_pegawai as $data) { ?>
                                            <option value="<?= $data->nama_kategori ?>" <?= ($data->nama_kategori == $brand->status_pegawai) ? "selected" : ""; ?>>
                                                <?= $data->nama_kategori; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Instansi</label>
                                <div class="col-sm-5">
                                    <select name="instansi" class="form-control select-jkel" id="instansi">
                                        <?php foreach ($instansi as $data) { ?>
                                            <option value="<?= $data->nama_kategori ?>" <?= ($data->nama_kategori == $brand->instansi) ? "selected" : ""; ?>>
                                                <?= $data->nama_kategori; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Alamat Instansi</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="alamat_instansi" placeholder="Alamat instansi" id="alamat_instansi" value="<?= $brand->alamat_instansi; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Provinsi</label>
                                <div class="col-sm-6">
                                    <select name="id_provinsi" id="id_provinsi" class="form-control select-provinsi" aria-describedby="sizing-addon2">
                                        <?php foreach ($provinsi as $data) { ?>
                                            <option value="<?= $data->id_provinsi; ?>" <?= ($data->id_provinsi == $brand->id_provinsi) ? "selected" : ""; ?>>
                                                <?= $data->nama_provinsi; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <br><br>
                                <div id="loadingImg">
                                    <img src="<?= base_url() . 'assets/' ?>publik/img/images/loading-bubble.gif">
                                    <p><font face="raleway" size="2" color="red">Tunggu sebentar...</font></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Kab/Kota</label>
                                <div class="col-sm-6">
                                    <select name="id_kota" id="id_kota" class="form-control select-kota" aria-describedby="sizing-addon2">
                                        <?php foreach ($kota as $data) { ?>
                                            <option value="<?= $data->id_kota; ?>" <?= ($data->id_kota == $brand->id_kota) ? "selected" : ""; ?>>
                                                <?= $data->nama_kota; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">No Registrasi</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="no_reg" placeholder="No Registrasi" id="alamat_instansi" value="<?= $brand->no_reg; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">No Handphone</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="nomor_hp" placeholder="No Handphone" id="nomor_hp" onkeypress="return hanyaAngka(event)" value="<?= $brand->nomor_hp; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" placeholder="email" name="email" id="email" aria-describedby="sizing-addon2" value="<?= $brand->email; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Password</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" placeholder="password" id="password-field" name="password" aria-describedby="sizing-addon2" value="<?= base64_decode($brand->password); ?>"><span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Status User</label>
                                <div class="col-sm-5">
                                    <select name="status" class="form-control select-jkel" id="status">
                                        <?php foreach ($status as $data) { ?>
                                            <option value="<?= $data->nama ?>" <?= ($data->nama == $brand->status) ? "selected" : ""; ?>>
                                                <?= $data->nama; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Foto</label>
                                <div class="col-sm-5">
                                    <div id="foto">
                                        <?php if ($brand->photo != null) { ?>
                                            <img class="img-thumbnail" src='<?= base_url(); ?><?= $brand->photo; ?>' width="150px">
                                        <?php } else { ?>
                                            <img class="img-thumbnail" src="<?= base_url(); ?>/upload/no-image.jpg" width="150px" />
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>     
                        </div>
                        <!-- /.box-body -->
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <div id="buka">
                                        <button type="submit" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                                        <a href="<?= site_url($page); ?>" type="button" class="btn red ajaxify klik">Kembali</a>
                                    </div>
                                    <div id="btn_loading">
                                        <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>             
                </div>
            </div>
        </div> 
        <div class="col-md-5"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i> Kelengkapan Dokumen
                    </div>
                </div>
                <div class="portlet-body form">
                    <?php if ($brand->folder != NULL) { ?>
                        <form method="post" action="<?= base_url(); ?>Master/Master_user/download" enctype="multipart/form-data">
                            <div class="form-body">
                                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                    <tr>  
                                        <th width="20px"><label class="container2">
                                                <input type="checkbox" id="check-all"><span class="checkmark"></span></label></th>
                                        <th>File</th>
                                    </tr>
                                    <?php
                                    foreach ($berkas as $res) {
                                        $kalimat = $res;
                                        $string = substr($kalimat, 36);
                                        echo '<tr>
                  <td><label class="container2"><input type="checkbox" name="berkas[]" class="check-item" value="' . $kalimat . '"><span class="checkmark"></span></label></th></td>
                  <td><font face="raleway" size="2" color="red"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>&nbsp;' . $string . '</font></td>
                  </tr>';
                                    }

                                    ?>
                                    <br>
                                </table>
                            </div>
                            <br/>
                            <div class="form-actions">
                                <div class="pencet">
                                    <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-download"></i> Download</button>
                                </div>
                            </div>
                        </form>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#loadingImg").hide();
        $('#id_provinsi').change(function () {
            var id_provinsi = $(this).val();
            $.ajax({
                beforeSend: function () {
                    $("#loadingImg").show();
                },
                url: "<?= base_url(); ?>ajax-list-kota",
                method: "POST",
                data: {id_provinsi: id_provinsi},
                async: false,
                dataType: 'json',
                success: function (data) {
                    var html = '';
                    var i;
                    html += '<option>Silahkan pilih Kab / Kota</option>';
                    for (i = 0; i < data.length; i++) {
                        html += '<option value="' + data[i].id_kota + '" kode="' + data[i].kode + '">' + data[i].nama_kota + '</option>';
                    }
                    $('#id_kota').html(html);
                    $("#loadingImg").fadeOut();
                }
            });
        });
    });

    $('#form-update').submit(function (e) {
        e.preventDefault();
        var error = 0;
        var message = "";

        if (error == 0) {
            var tempat_lahir = $("#tempat_lahir").val();
            var tempat_lahir = tempat_lahir.trim();
            if (tempat_lahir.length == 0) {
                error++;
                message = "Tempat Lahir wajib di isi.";
            }
        }
        if (error == 0) {
            var tgl_lahir = $("#tgl_lahir").val();
            var tgl_lahir = tgl_lahir.trim();
            if (tgl_lahir.length == 0) {
                error++;
                message = "Tanggal Lahir wajib di isi.";
            }
        }
        if (error == 0) {
            var jkel = $("#jkel").val();
            var jkel = jkel.trim();
            if (jkel.length == 0) {
                error++;
                message = "Jenis Kelamin wajib di isi.";
            }
        }
        if (error == 0) {
            var status_pegawai = $("#status_pegawai").val();
            var status_pegawai = status_pegawai.trim();
            if (status_pegawai.length == 0) {
                error++;
                message = "Status Kepegawaian wajib di isi.";
            }
        }
        if (error == 0) {
            var instansi = $("#instansi").val();
            var instansi = instansi.trim();
            if (instansi.length == 0) {
                error++;
                message = "Instansi wajib di isi.";
            }
        }
        if (error == 0) {
            var nomor_hp = $("#nomor_hp").val();
            var nomor_hp = nomor_hp.trim();
            if (nomor_hp.length == 0) {
                error++;
                message = "No Handphone wajib di isi.";
            }
        }
        if (error == 0) {
            var password = $("#password-field").val();
            var password = password.trim();
            if (password.length == 0) {
                error++;
                message = "Password wajib di isi.";
            } else if (!cekpassword(password)) {
                error++;
                message = "Password tidak boleh ada spasi !! ";
            }
        }
        if (error == 0) {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?= base_url('update-master-user') . '/' . $brand->id_user; ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    setTimeout("window.location='<?= base_url($page); ?>'", 500);
                    toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                }
            });
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });

    // untuk show hide password
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });

    function capitalize(textboxid, str) {
        // string with alteast one character
        if (str && str.length >= 1) {
            var firstChar = str.charAt(0);
            var remainingStr = str.slice(1);
            str = firstChar.toUpperCase() + remainingStr;
        }
        document.getElementById(textboxid).value = str;
    }

    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    //Proses Controller logic ajax
    function cekemail(a) {
        re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(a);
    }

    function cekpassword(a) {
        re2 = /^\S{3,}$/;
        return re2.test(a);
    }

    $(function () {
        $('.pencet').hide();
        $("#check-all").click(function () {
            if ($(this).is(":checked")) {
                $(".check-item").prop("checked", true);
                $('.pencet').fadeIn("slow");
            } else {
                $('.pencet').fadeOut("slow");
                $(".check-item").prop("checked", false);
            }
        });

        $('.pencet').hide();
        $(".check-item").click(function () {
            if ($(this).is(":checked")) {
                $('.pencet').fadeIn("slow");
            } else {
                $('.pencet').fadeOut("slow");
            }
        });

        $(".select-provinsi").select2({
            placeholder: " -- Pilih Provinsi -- "
        });
        $(".select-kota").select2({
            placeholder: " -- Pilih Kab/Kota -- "
        });
        $(".select-jkel").select2({
            placeholder: " -- Pilih jenis kelamin -- "
        });
        $(".select-pegawai").select2({
            placeholder: " -- Pilih Status Kepegawaian -- "
        });

        // untuk datepicker
        $(".tgl_lahir").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });
</script>