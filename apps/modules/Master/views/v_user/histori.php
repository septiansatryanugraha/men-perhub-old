<div class="page-content">
    <style type="text/css">
        #slider{
            margin-left: -2%;
        }
        #btn_loading {
            display: none;
        }
        #btn_loading2 {
            display: none;
        }
        .space {
            margin-top: 8px;
        }
        .table-ok {
            font-family: "arial", serif;
            font-size: 14px;
            margin-bottom: 20px;
        }
        .table-ok, th, td {
            padding: 10px 0px 0px 30px;
            margin-left: -20px;
            text-align: left;
        }
    </style>
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        Histori Pengajuan
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i>Histori Pengajuan</li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-history"></i> Histori Pengajuan</div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#diklat" data-toggle="tab">Pengajuan Diklat PKB</a></li>
                                        <li><a href="#jenjang" data-toggle="tab">Pengajuan Kenaikan Jejang</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="active tab-pane" id="diklat">
                                            <div class="portlet-body form"> 
                                                <div class="form-body">
                                                    <br>
                                                    <?php if (!empty($ValData)) { ?>
                                                        <?php foreach ($ValData as $res) { ?>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <table class="table-ok">
                                                                        <tr><td>Kode Pengajuan</td><td>:</td><td><?= $res->kode_pengajuan ?></td></tr>
                                                                        <tr><td>Jenis Jenjang</td><td>:</td><td><b><?= $res->jenis_diklat ?></b></td></tr>
                                                                        <tr><td>Status</td><td>:</td><td><?= $res->status ?></td></tr>
                                                                        <tr><td>Tanggal</td><td>:</td><td><?= date_indo(date($res->created_date)) ?></td></tr>
                                                                    </table><hr>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <table class="table-ok">
                                                                        <tr><td>CAT ( 30% )</td><td>:</td><td><b><?= $res->ne1; ?></b></td><td><b><?= $res->na1; ?></b></td></tr>
                                                                        <tr><td>Lisan ( 30% )</td><td>:</td><td><b><?= $res->ne2; ?></b></td><td><b><?= $res->na2; ?></b></td></tr>
                                                                        <tr><td>Praktek ( 40% )</td><td>:</td><td><b><?= $res->ne3; ?></b></td><td><b><?= $res->na3; ?></b></td></tr>
                                                                        <tr><td>Nilai</td><td>:</td><td><b><?= $res->kumulatif ?></b></td></tr>
                                                                    </table><hr>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div> 
                                                </div>           
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="jenjang">
                                            <div class="portlet-body form"> 
                                                <div class="form-body">
                                                    <br>
                                                    <?php if (!empty($ValData2)) { ?>
                                                        <?php foreach ($ValData2 as $res) { ?>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <table class="table-ok">
                                                                        <tr><td>Kode Pengajuan</td><td>:</td><td><?= $res->kode_pengajuan ?></td></tr>
                                                                        <tr><td>Jenis Diklat</td><td>:</td><td><b><?= $res->jenis_jenjang ?></b></td></tr>
                                                                        <tr><td>Status</td><td>:</td><td><?= $res->status ?></td></tr>
                                                                        <tr><td>Tanggal</td><td>:</td><td><?= date_indo(date($res->created_date)) ?></td></tr>
                                                                    </table><hr>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <table class="table-ok">
                                                                        <tr><td>CAT ( 30% )</td><td>:</td><td><b><?= $res->ne1; ?></b></td><td><b><?= $res->na1; ?></b></td></tr>
                                                                        <tr><td>Lisan ( 30% )</td><td>:</td><td><b><?= $res->ne2; ?></b></td><td><b><?= $res->na2; ?></b></td></tr>
                                                                        <tr><td>Praktek ( 40% )</td><td>:</td><td><b><?= $res->ne3; ?></b></td><td><b><?= $res->na3; ?></b></td></tr>
                                                                        <tr><td>Nilai</td><td>:</td><td><b><?= $res->kumulatif ?></b></td></tr>
                                                                    </table><hr>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>