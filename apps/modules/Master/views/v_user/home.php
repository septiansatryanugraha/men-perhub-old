<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        Data <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <li><i class="fa fa-angle-right"></i>Data <?= $title ?></li>
        </ul>
    </div>
    <!-- /.box-header -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-list"></i> Data <?= $title ?></div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Photo</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Instansi</th>
                                    <th>Status</th>
                                    <th style="width:125px;">Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    // untuk datepicker
    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });

    //untuk load data table ajax  
    var save_method; //for save method string
    var table;

    $(document).ready(function () {
        reloadTable();
    });

    function reloadTable() {
        var tanggal_awal = $("#tanggal_awal").val();
        var tanggal_akhir = $("#tanggal_akhir").val();

        table = $('#table').DataTable({
            "processing": true, //Feature control the processing indicator.
            "order": [], //Initial no order.
            oLanguage: {
                "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Data tidak ada di server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?= base_url('ajax-master-user') ?>",
                "type": "POST",
                data: {tanggal_awal: tanggal_awal, tanggal_akhir: tanggal_akhir},
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
        });
    }

    $('#search-button').click(function () {
        $('.search-form').toggle();
        return false;
    });

    $("#button_filter").click(function () {
        table.destroy();
        reloadTable();
    });

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    $(document).on("click", ".hapus-master-user", function () {
        var email = $(this).attr("data-id");
        swal({
            title: "Hapus Data?",
            text: "Yakin anda akan menghapus data ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Hapus",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: "POST",
                url: "<?= base_url('hapus-master-user'); ?>",
                data: "email=" + email,
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        $("tr[data-id='" + email + "']").fadeOut("fast", function () {
                            $(this).remove();
                        });
                        swal("Success", result.pesan, "success");
                    } else {
                        swal("Warning", result.pesan, "warning");
                    }
                    reload_table();
                }
            });
        });
    });
</script>