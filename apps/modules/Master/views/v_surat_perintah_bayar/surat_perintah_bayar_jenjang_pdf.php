<html>
    <style>
        body {
            /*background: rgb(204,204,204); */
            font-family: "Bookman Old Style";
        }
        /*        page {
                    background: white;
                    display: block;
                    margin: 0 auto;
                    margin-bottom: 0.5cm;
                    box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
        
                }
                page[size="A4"] {  
                    width: 21cm;
                    height: 29.7cm; 
                }*/
        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }
        }
        .container {
            max-width: 68em;
            margin: 0 auto;
            padding-left: 6%;
            padding-top: 1%;
            width: 100%;
        }
        .kop {
            margin-top: -20px;
        }
        .header-kanan-1 {
            font-size: 14px;
            margin-bottom: 10px;
        }
        .header-kanan-2 {
            font-size: 14px;
        }
        .header-kanan-3 {
            font-size: 14px;
            margin-top: -10px;
        }
        #box1 {
            width:656px;
            margin-left:70px;
        }
        .margin-judul-3 {
            margin-top:10px;
            font-size: 16px
        }
        .no_sertifikat {
            padding-top:20px;
            font-size: 12px
        }
        .text1 {
            margin-top:10px;
            font-size: 14px
        }
        .text2 {
            margin-top:-16px;
            margin-left: 390px;
            font-size: 14px
        }
        .text3 {
            margin-top:-17px;
            margin-left: 600px;
            font-size: 14px
        }
        .text4 {
            margin-top:12px;
            font-size: 14px;
            line-height:1.5em;
            max-width: 45em;
            margin: 0 auto;
            margin-left: 4%;
            padding-right: 5%;
            padding-top: 1%;
            width: 100%;
        }
        .text5 {
            margin-top:10px;
            font-size: 14px;
            line-height:1.7em;
            max-width: 44em;
            margin: 0 auto;
            margin-left: 7%;
            padding-right: 5%;
            padding-top: 1%;
            width: 100%;
        }
        .kotak {
            width: 4.3em;
            height: 6.3em;
            margin-top: 60px;
            margin-left:250px;
            position: absolute;
            z-index: 7;
            border: solid 1px black;
        }
        .kotak p {
            padding-top: 30px;
            padding-left: 15px;
        }
        .gap1 {
            margin-top: 0px;
        }
        .spasi {
            margin-top: 10px;
        }
        .margin-nama {
            margin-top:27px;
            padding-left:325px;
            text-transform: uppercase;
            font-size:18px;
        }
        .margin-tgl {
            margin-top:10px;
            padding-left:325px;
            text-transform: uppercase;
            font-size:18px;
        }
        #kontenkedua {
            margin-top: -15px;
        }
        #kontenkedua h2 {
            margin: 0;
            padding: 0;
            text-align: center;
        }
        #kontenkedua li {
            list-style-type:  none;
            float: left;
            width: 45%;
            padding-left: 9%;
            padding-right: 2%;
            line-height: 1em;
        }
        #kontenkedua:after {
            display: block;
            content: "";
            clear: both;
        }  
        #kontenkedua2 {
            margin-top: 5px;
        }
        #kontenkedua2 h2 {
            margin: 0;
            padding: 0;
            text-align: center;
        }
        #kontenkedua2 li {
            list-style-type:  none;
            float: left;
            width: 50%;
            padding-left: -2%;
            margin-bottom: 2%;
            margin-right: 0%;
            line-height: 1em;
        }
        #kontenkedua2:after {
            display: block;
            content: "";
            clear: both;
        } 
        .margin-judul-kapol {
            margin-top:10px;
            padding-left:320px;
            font-size:14px; 
        }
        .margin-judul-kapol2 {
            margin-top:5px;
            padding-left:350px;
            font-size:14px; 
        }
        .margin-nama-kapol {
            margin-top:110px;
            padding-left:423px;
            font-size:14px; 
            text-decoration: underline;
        }
        .margin-nama-kapol2 {
            margin-top:5px;
            padding-left:400px;
            font-size:14px; 
        }
        .margin-nama-kapol3 {
            margin-top:5px;
            padding-left:412px;
            font-size:14px; 
        }
        .ttd {
            margin-left: 450px;
            position: absolute;
            z-index: 99;
            margin-top: 10px;
        }
        .cap {
            margin-top: -60px;
            margin-left: 380px;
            position: absolute;
            z-index: 9;
        }
        .table-ok {
            font-family: "Bookman Old Style";
            font-size: 14px;
        }
        .table-ok, th, td {
            padding: 5px 0px 0px 30px;
            margin-left: -100px;
            text-align: left;
        }
        .kontenkiribawah {
            position: absolute;
            z-index: 9;
            margin-top: 120px;
        }
        .text-kontenkiribawah {
            margin-top:2px;
            font-size: 13px;
            line-height:1em;
            max-width: 40em;
            margin: 0 auto;
            margin-left: 4%;
            padding-right: 5%;
            padding-top: 1%;
            width: 100%;
        }
        .tulisan-kanan-1 {
            font-size: 14px;
            margin-bottom: 10px;
        }
        .tulisan-kanan-2 {
            font-size: 14px;
            margin-bottom: 10px;
        }
        .tulisan-kanan-3 {
            font-size: 14px;
            margin-bottom: 10px;
        }
    </style>
    <div class="container">
        <img class="kop" src="<?= site_url(); ?>assets/tambahan/gambar/kop-menhub.png" width="680px">
        <section id="kontenkedua">
            <ul>
                <li>
                    <table class="table-ok">
                        <tr><td>Nomor</td><td>:</td><td><?= $datamaster->no_surat ?></td></tr>
                        <tr><td>Klasifikasi</td><td>:</td><td>Penting / Segera</td></tr>
                        <tr><td>Lampiran</td><td>:</td><td>1 (satu) lembar</td></tr>
                        <tr><td>Perihal</td><td>:</td><td><?= $datamaster->perihal ?></td></tr>
                    </table>
                </li>
                <li>
                    <p class="header-kanan-1">Jakarta,  <?= date_indo(date($datamaster->tgl_surat)); ?> </p>
                    <p class="header-kanan-2">Kepada :</p>
                    <p class="header-kanan-3"><?= $datamaster->kepada ?></p>
                    <p class="header-kanan-3">di TEMPAT.</p>  
                </li>
            </ul>     
        </section>
        <div class="gap1"></div>
        <p align="justify" class="text4">1.Berdasarkan Berita Acara Hasil Uji Kompetensi Tenaga Penguji Berkala Kendaraan Bermotor Nomor:  : <?= $datamaster->no_surat ?> Tanggal  <?= date_indo(date($datamaster->tgl_surat)); ?> Perihal Penawaran  Peserta Diklat Penguji Kendaraan Bermotor dan Hasil Seleksi Peserta dengan Berita Acara Nomor: <?= $datamaster->no_ba ?> Tanggal <?= date_indo(date($datamaster->tgl_ba)); ?> telah dilaksanakan Uji Kompetensi Tenaga Penguji Berkala Kendaraan Bermotor jenjang <?= $datamaster->jenis_jenjang ?> dengan jumlah peserta sebanyak <?= $datamaster->total_peserta ?> oleh Tim Penilai Uji Kompetensi dengan hasil peserta yang dinyatakan lulus (berkompeten) sebagaimana terlampir. Sehubungan dengan hal tersebut, bersama ini kami sampaikan hal-hal sebagai berikut:  </p>
        <p align="justify" class="text5">a. Telah diterbitkan Sertifikat Kompetensi Penguji Berkala Kendaraan Bermotor atas nama sebagaimana daftar terlampir.
        <p align="justify" class="text5">b. Berdasarkan Peraturan Pemerintah Nomor 15 Tahun 2016 Tentang Jenis dan Tarif atas Jenis Penerimaan Negara Bukan Pajak (PNBP) Yang Berlaku Pada Kementerian Perhubungan, pemberian Sertifikasi Kompetensi Penguji  Berkala Kendaraan Bermotor dan Tanda Kualifikasi Teknis dikenakan tarif dengan rincian tagihan sebagaimana terlampir</p>
        <p align="justify" class="text5">c.	Selanjutnya pembayaran dapat dilakukan pada bank yang ditunjuk oleh Kementerian Keuangan di wilayah Saudara dengan menunjukkan kode billing dan memperhatikan batas waktu pembayaran.</p>
        <p align="justify" class="text5">d.	Setelah melakukan pembayaran dimohon segera melakukan konfirmasi ke Direktorat Sarana Transportasi Jalan untuk pemberian Sertifikasi Kompetensi Penguji  Berkala Kendaraan Bermotor beserta Tanda Kualifikasi Teknis.</p>
        <p class="text4">2.	Demikian atas perhatian atas dan kerjasamanya diucapkan terima kasih.</p>
        <div class="spasi"></div>
        <div class="margin-judul-kapol"><b>a.n DIREKTUR JENDERAL PERHUBUNGAN DARAT</b></div>
        <div class="margin-judul-kapol2"><b>DIREKTUR SARANA TRANSPORTASI JALAN</b></div>
        <img class="ttd" src="<?= site_url(); ?>assets/tambahan/gambar/ttd.png" width="150px">
        <img class="cap" src="<?= site_url(); ?>assets/tambahan/gambar/cap-kemenhub.png" width="220px">
        <div class="margin-nama-kapol"><b>PANDU YUNIANTO</b></div>
        <div class="margin-nama-kapol2"><b>Pembina Utama Madya - IV/d</b></div>
        <div class="margin-nama-kapol3"><b>NIP. 19650606 198803 1 001</b></div>
        <section id="kontenkedua2">
            <ul>
                <li><table class="table-ok"></table></li>
                <li>
                    <p class="header-kanan-1">Lampiran Surat Direktur Jenderal Perhubungan Darat </p>
                    <p class="header-kanan-2">Nomor &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp; <?= $datamaster->no_surat ?></p>
                    <p class="header-kanan-3">Tanggal &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp; <?= $datamaster->tgl_surat ?></p>
                </li>
            </ul>     
        </section>
        <h5><center>DAFTAR NAMA PESERTA DIKLAT/PENINGKATAN JENJANG KOMPETENSI YANG DINYATAKAN LULUS UJI KOMPETENSI (KOMPETEN)</center></h5>
        <table border="1" width="100%">
            <thead>
                <tr>
                    <th width="30px">No.</th>
                    <th align="center">Nama</th>
                    <th align="center">Asal Instansi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $dataakses = $this->M_utilities->selekUserDiklat($datamaster->id_jenis_jenjang);
                if (!empty($dataakses)) {
                    $no = 1;
                    foreach ($dataakses as $data1) {
                        $dataUser = $this->M_user->selectById($data1->id_user);

                        ?>
                        <tr>
                            <td><?= $no ?></td>
                            <td><?= $data1->created_by ?></td>
                            <td><?= $dataUser->asal_instansi ?></td>
                        </tr>
                        <?php
                        $no++;
                    }
                } else {

                    ?>
                    <tr><td colspan='3'><center>Belum ada pengajuan</center></td></tr>
            <?php } ?>
            </tbody>
        </table>
        <br>
    </div>
</html>