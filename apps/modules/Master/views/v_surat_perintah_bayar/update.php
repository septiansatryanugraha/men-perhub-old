<style type="text/css">
    #btn_loading {
        display: none;
    }
</style>
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        Ubah <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i>Ubah <?= $title ?></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-pencil-square"></i> Data <?= $title ?></div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                        <?= $exportPdf ?>
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <form class="form-horizontal" id="form-update" method="POST">
                        <input type="hidden" name="id_surat" value="<?= $brand->id_surat; ?>">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Subjek </label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" placeholder="Subjek" name="subjek" id="subjek" aria-describedby="sizing-addon2" value="<?= $brand->subjek; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Kepada </label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" placeholder="Subjek" name="kepada" id="kepada" aria-describedby="sizing-addon2" value="<?= $brand->kepada; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Perihal</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" placeholder="Perihal Surat " name="perihal" id="perihal" aria-describedby="sizing-addon2" value="<?= $brand->perihal; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Jumlah Peserta</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" placeholder="Jumlah Peserta " name="total_peserta" id="total_peserta" aria-describedby="sizing-addon2" value="<?= $brand->total_peserta; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Jenis Surat </label>
                                <div class="col-sm-4">
                                    <select name="tipe_surat" id="tipe_surat" class="form-control select-tipe-surat" aria-describedby="sizing-addon2">
                                        <option></option>
                                        <option value="Surat Panggilan Diklat" <?= ($brand->tipe_surat == 'Surat Panggilan Diklat') ? "selected" : ""; ?>>Diklat PKB</option>
                                        <option value="Surat Panggilan Jenjang" <?= ($brand->tipe_surat == 'Surat Panggilan Jenjang') ? "selected" : ""; ?>>Kenaikan Jenjang</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group div-jenjang">
                                <label class="col-sm-2 control-label">Jenis Jenjang</label>
                                <div class="col-sm-4">
                                    <select name="id_jenis_jenjang" class="form-control select-jenjang" id="id_jenis_jenjang" aria-describedby="sizing-addon2">
                                        <option></option>
                                        <?php foreach ($jenis_jenjang as $data) { ?>
                                            <option value="<?= $data->id ?>" <?= ($data->id == $brand->id_jenis_jenjang) ? "selected" : ""; ?>><?= $data->jenjang; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group div-diklat">
                                <label class="col-sm-2 control-label">Jenis Jenjang</label>
                                <div class="col-sm-4">
                                    <select name="id_kategori_diklat" class="form-control select-jenjang" id="id_kategori_diklat" aria-describedby="sizing-addon2">
                                        <option></option>
                                        <?php foreach ($jenis_diklat as $data) { ?>
                                            <option value="<?= $data->id_kategori_diklat ?>" <?= ($data->id_kategori_diklat == $brand->id_kategori_diklat) ? "selected" : ""; ?>><?= $data->kategori_diklat; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Nomor Surat </label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" placeholder="Nomor surat" name="no_surat" id="no_surat" aria-describedby="sizing-addon2" value="<?= $brand->no_surat; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Surat</label>
                                <div class="col-sm-3">
                                    <input type="text" id="tgl_surat" class="form-control tgl_surat" name="tgl_surat" placeholder="Tanggal Surat" value="<?= date('d-m-Y', strtotime($brand->tgl_surat)); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Nomor Berita Acara </label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" placeholder="Nomor berita acara" name="no_ba" id="no_ba" aria-describedby="sizing-addon2" value="<?= $brand->no_ba; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Berita Acara</label>
                                <div class="col-sm-3">
                                    <input type="text" id="tgl_ba" class="form-control tgl_surat" name="tgl_ba" placeholder="Tanggal Surat" value="<?= date('d-m-Y', strtotime($brand->tgl_ba)); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <div id="buka">
                                        <button type="submit" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                                        <a href="<?= base_url($page); ?>" type="button" class="btn red ajaxify klik">Kembali</a>
                                    </div>
                                    <div id="btn_loading">
                                        <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>              
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $(".select-tipe-surat").select2({
            placeholder: " -- Pilih-- "
        });
        $(".select-jenjang").select2({
            placeholder: " -- Pilih jenis jenjang -- "
        });
        // untuk datepicker
        $(".tgl_surat").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
        tipe_surat();
        $('#tipe_surat').change(function () {
            tipe_surat();
        });
    });

    function tipe_surat() {
        $('.div-diklat').hide();
        $('.div-jenjang').hide();
        var tipe_surat = $('#tipe_surat').val();
        if (tipe_surat.length > 0) {
            if (tipe_surat == 'Surat Panggilan Diklat') {
                $('.div-diklat').show();
                $('.div-jenjang').hide();
            } else if (tipe_surat == 'Surat Panggilan Jenjang') {
                $('.div-diklat').hide();
                $('.div-jenjang').show();
            } else {
                $('.div-diklat').hide();
                $('.div-jenjang').hide();
            }
        } else {
            $('.div-diklat').hide();
            $('.div-jenjang').hide();
        }
    }

    $('#form-update').submit(function (e) {
        e.preventDefault();
        var error = 0;
        var message = "";

        if (error == 0) {
            var subjek = $("#subjek").val();
            var subjek = subjek.trim();
            if (subjek.length == 0) {
                error++;
                message = "Subjek wajib di isi.";
            }
        }
        if (error == 0) {
            var kepada = $("#kepada").val();
            var kepada = kepada.trim();
            if (kepada.length == 0) {
                error++;
                message = "Tujuan surat wajib di isi.";
            }
        }
        if (error == 0) {
            var tipe_surat = $("#tipe_surat").val();
            var tipe_surat = tipe_surat.trim();
            if (tipe_surat.length == 0) {
                error++;
                message = "Jenis Surat wajib di isi.";
            }
        }
        if (error == 0) {
            if (tipe_surat == 'Surat Panggilan Diklat') {
                var id_kategori_diklat = $("#id_kategori_diklat").val();
                var id_kategori_diklat = id_kategori_diklat.trim();
                if (id_kategori_diklat.length == 0) {
                    error++;
                    message = "Jenis Jenjang wajib di isi.";
                }
            }
            if (tipe_surat == 'Surat Panggilan Jenjang') {
                var id_jenis_jenjang = $("#id_jenis_jenjang").val();
                var id_jenis_jenjang = id_jenis_jenjang.trim();
                if (id_jenis_jenjang.length == 0) {
                    error++;
                    message = "Jenis Jenjang wajib di isi.";
                }
            }
        }
        if (error == 0) {
            var no_surat = $("#no_surat").val();
            var no_surat = no_surat.trim();
            if (no_surat.length == 0) {
                error++;
                message = "No Surat wajib di isi.";
            }
        }
        if (error == 0) {
            var tgl_surat = $("#tgl_surat").val();
            var tgl_surat = tgl_surat.trim();
            if (no_surat.length == 0) {
                error++;
                message = "Tanggal Surat wajib di isi.";
            }
        }
        if (error == 0) {
            var no_ba = $("#no_ba").val();
            var no_ba = no_ba.trim();
            if (no_ba.length == 0) {
                error++;
                message = "Nomor Berita Acara wajib di isi.";
            }
        }
        if (error == 0) {
            var tgl_ba = $("#tgl_ba").val();
            var tgl_ba = tgl_ba.trim();
            if (tgl_ba.length == 0) {
                error++;
                message = "Tanggal Berita Acara wajib di isi.";
            }
        }
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?= base_url('update-surat-bayar') . '/' . $brand->id_surat; ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    setTimeout("window.location='<?= base_url($page); ?>'", 500);
                    toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                }
            })
            e.preventDefault();
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });
</script>