<style>
    #btn_loading {
        display: none;
    }
    .space {
        margin-top: 8px;
    }
</style>
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i>Ubah <?= $title ?></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-9"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-database"></i> Master Data Billing Pembayaran</div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <form class="form-horizontal" id="form-upload" method="POST" enctype="multipart/form-data">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kode Pengajuan</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static"><?= $brand->kode_pengajuan; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama User</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static"><?= $brand->nama_pemohon; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static"><?= $brand->email; ?></p>
                                </div>
                            </div>
                            <div class="form-group row">      
                                <label for="inputFoto" class="col-sm-3 control-label">Bukti Pembayaran</label>
                                <div class="col-sm-6 space">
                                    <?php if ($brand->nama_file_upload != '') { ?>
                                        <a href="<?= base_url() . $brand->file_upload_pembayaran; ?>"><b><font face="verdana" size="2" color="red"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?= $brand->nama_file_upload; ?></font></b></a>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Status Pembayaran</label>
                                <div class="col-sm-3">
                                    <select name="status" class="form-control select-status" id="status">
                                        <option></option>
                                        <?php foreach ($status as $data) { ?>
                                            <option value="<?= $data->nama ?>" <?= ($data->nama == $brand->status) ? "selected" : ""; ?>><?= $data->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <div id="buka">
                                        <button type="submit" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                                        <a href="<?= base_url($page); ?>" type="button" class="btn red ajaxify klik">Kembali</a>
                                    </div>
                                    <div id="btn_loading">
                                        <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>             
                </div>
            </div>
        </div> 
    </div>
</div>

<script type="text/javascript">
    $('#form-upload').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var error = 0;
        var message = "";
        if (error == 0) {
            var status = $("#status").val();
            var status = status.trim();
            if (status.length == 0) {
                error++;
                message = "Status Pembayaran wajib di isi.";
            }
        }
        if (error == 0) {
            swal({
                title: "Purchase Data?",
                text: "Yakin Memproses Data Ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $(".confirm").attr('disabled', 'disabled');
                $.ajax({
                    method: 'POST',
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    url: '<?= base_url('update-pembayaran') . '/' . $brand->id_pembayaran; ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        setTimeout(location.reload.bind(location), 500);
                        toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                    } else {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                    }
                });
            });
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });

    $(function () {
        $(".select-status").select2({
            placeholder: " -- Pilih status -- "
        });
    });
</script>