<style>
    .field-icon {
        float: left;
        margin-left: 93%;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }
</style>
<!-- style loading -->
<div id ="loading2"></div>
<!-- -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        Ubah <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i>Ubah <?= $title ?></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-9"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-pencil-square"></i> <?= $title ?></div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                        <?= $exportPdf ?>
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <form class="form-horizontal" id="form-update" method="POST">
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="form-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">No Surat</label>
                                <div class="col-sm-6">
                                    <input type="text" id="no_surat" class="form-control" name="no_surat" placeholder="Nomor Surat" value="<?= $brand->no_surat; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Surat</label>
                                <div class="col-sm-4">
                                    <input type="text" id="tgl_surat" class="form-control tgl_ba" name="tgl_surat" placeholder="Tanggal Surat" value="<?= date('d-m-Y', strtotime($brand->tgl_surat)); ?>">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                                    <a href="<?= base_url($page); ?>" type="button" class="btn red ajaxify klik">Kembali</a>
                                </div>
                            </div>
                        </div>
                    </form>             
                </div>
            </div>
        </div> 
    </div>
</div>

<script type="text/javascript" src="<?= theme(); ?>/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript">
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var error = 0;
        var message = "";
        if (error == 0) {
            var no_surat = $("#no_surat").val();
            var no_surat = no_surat.trim();
            if (no_surat.length == 0) {
                error++;
                message = "Nomor surat wajib di isi.";
            }
        }
        if (error == 0) {
            var tgl_surat = $("#tgl_surat").val();
            var tgl_surat = tgl_surat.trim();
            if (tgl_surat.length == 0) {
                error++;
                message = "Tanggal surat wajib di isi.";
            }
        }
        if (error == 0) {
            swal({
                title: "Approve Data?",
                text: "Yakin Memproses Data Ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $(".confirm").attr('disabled', 'disabled');
                $.ajax({
                    method: 'POST',
                    url: '<?= base_url('update-file-pnbp') . '/' . $brand->id_perpanjangan; ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        swal("Success", result.pesan, "success");
                        setTimeout("window.location='<?= base_url($page); ?>'", 500);
                    } else {
                        swal("Warning", result.pesan, "warning");
                    }
                })
            });
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });

    $(function () {
        $(".tgl_ba").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        });

        $('.pencet').hide();
        $("#check-all").click(function () {
            if ($(this).is(":checked")) {
                $(".check-item").prop("checked", true);
                $('.pencet').fadeIn("slow");
            } else {
                $('.pencet').fadeOut("slow");
                $(".check-item").prop("checked", false);
            }
        });

        $('.pencet').hide();
        $(".check-item").click(function () {
            if ($(this).is(":checked")) {
                $('.pencet').fadeIn("slow");
            } else {
                $('.pencet').fadeOut("slow");
            }
        });
    });
</script>