<style>
    .field-icon {
        float: left;
        margin-left: 93%;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }
    #btn_loading {
        display: none;
    }
    /* The container */
    .container2 {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 22px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    /* Hide the browser's default checkbox */
    .container2 input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }
    /* Create a custom checkbox */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px;
        width: 20px;
        border-radius: 3px;
        background-color: #d4d2d2;
    }
    /* On mouse-over, add a grey background color */
    .container:hover input ~ .checkmark {
        background-color: #ccc;
    }
    /* When the checkbox is checked, add a blue background */
    .container2 input:checked ~ .checkmark {
        background-color: #2196F3;
    }
    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }
    /* Show the checkmark when checked */
    .container2 input:checked ~ .checkmark:after {
        display: block;
    }
    /* Style the checkmark/indicator */
    .container2 .checkmark:after {
        left: 7px;
        top: 4px;
        width: 5px;
        height: 11px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
</style>
<!-- style loading -->
<div id ="loading2"></div>
<!-- -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        Ubah <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i>Ubah <?= $title ?></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-7"> 

            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-database"></i> Data Informasi</div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <form class="form-horizontal" id="form-update" method="POST">
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="form-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">No registrasi</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="no_reg" placeholder="No registrasi" id="no_reg" value="<?= $brand->no_reg_pemohon; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static"><?= $brand->nama_pemohon; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static"><?= $brand->email; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Jenis Tingkatan</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static"><?= $brand->jenis_perpanjangan; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">No Berita Acara</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="no_ba" placeholder="No Berita Acara" id="no_ba" value="<?= $brand->no_ba; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Berita Acara</label>
                                <div class="col-sm-4">
                                    <input type="text" id="tgl_ba" class="form-control tgl_ba" name="tgl_ba" placeholder="Tanggal Berita Acara" value="<?= date('d-m-Y', strtotime($brand->tgl_ba)); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">No Sertifikat</label>
                                <div class="col-sm-6">
                                    <input type="text" id="no_sertifikat" class="form-control" name="no_sertifikat" placeholder="Nomor Sertifikat" value="<?= $brand->no_sertifikat; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Status Pengajuan</label>
                                <div class="col-sm-4">
                                    <select name="status_perpanjangan" class="form-control select-status-perpanjangan" id="status_perpanjangan">
                                        <option></option>
                                        <?php foreach ($status as $data) { ?>
                                            <option value="<?= $data->nama ?>" <?= ($data->nama == $brand->status_perpanjangan) ? "selected" : ""; ?>><?= $data->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> 
                            <div class="menu-show">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Catatan</label>
                                    <div class="col-sm-6">
                                        <textarea name="catatan" cols="40" rows="4"><?= $brand->catatan; ?></textarea>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <!-- /.box-body -->
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                                    <a href="<?= base_url($page); ?>" type="button" class="btn red ajaxify klik">Kembali</a>
                                </div>
                            </div>
                        </div>
                    </form>             
                </div>
            </div>
        </div> 
        <div class="col-md-5"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i> Kelengkapan Dokumen
                    </div>
                </div>
                <div class="portlet-body form">
                    <?php if ($brand->folder != NULL) { ?>
                        <form method="post" action="<?= base_url('download-berkas-file'); ?>" enctype="multipart/form-data">
                            <div class="form-body">
                                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                    <tr>  
                                        <th width="20px"><label class="container2"><input type="checkbox" id="check-all"><span class="checkmark"></span></label></th>
                                        <th>File</th>
                                    </tr>
                                    <?php
                                    foreach ($berkas as $res) {
                                        $kalimat = $res;
                                        $string = substr($kalimat, 38);
                                        echo '<tr>
                    <td><label class="container2"><input type="checkbox" name="berkas[]" class="check-item" value="' . $kalimat . '"><span class="checkmark"></span></label></th></td>
                    <td><font face="raleway" size="2" color="red"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>&nbsp;' . $string . '</font></td>
                    </tr>';
                                    }

                                    ?>
                                    <br>
                                </table>
                            </div>
                            <br/>
                            <div class="form-actions">
                                <div class="pencet">
                                    <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-download"></i> Download</button>
                                </div>
                            </div>
                        </form>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= theme(); ?>/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript">
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var error = 0;
        var message = "";
        if (error == 0) {
            var no_reg = $("#no_reg").val();
            var no_reg = no_reg.trim();
            if (no_reg.length == 0) {
                error++;
                message = "Nomor Registrasi wajib di isi.";
            }
        }
        if (error == 0) {
            var no_ba = $("#no_ba").val();
            var no_ba = no_ba.trim();
            if (no_ba.length == 0) {
                error++;
                message = "Nomor BA wajib di isi.";
            }
        }
        if (error == 0) {
            var tgl_ba = $("#tgl_ba").val();
            var tgl_ba = tgl_ba.trim();
            if (tgl_ba.length == 0) {
                error++;
                message = "Tanggal BA wajib di isi.";
            }
        }
        if (error == 0) {
            var no_sertifikat = $("#no_sertifikat").val();
            var no_sertifikat = no_sertifikat.trim();
            if (no_sertifikat.length == 0) {
                error++;
                message = "Nomor Sertifikat wajib di isi.";
            }
        }
        if (error == 0) {
            var status_perpanjangan = $("#status_perpanjangan").val();
            var status_perpanjangan = status_perpanjangan.trim();
            if (status_perpanjangan.length == 0) {
                error++;
                message = "Status Perpanjangan wajib di isi.";
            }
        }
        if (error == 0) {
            swal({
                title: "Approve Data?",
                text: "Yakin Memproses Data Ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $(".confirm").attr('disabled', 'disabled');
                $.ajax({
                    method: 'POST',
                    url: '<?= base_url('update-perpanjangan-validasi') . '/' . $brand->id_perpanjangan; ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        swal("Success", result.pesan, "success");
                        setTimeout("window.location='<?= base_url($page); ?>'", 500);
                    } else {
                        swal("Warning", result.pesan, "warning");
                    }
                })
            })
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });

    function menu_show() {
        if ($('#status_perpanjangan').val() == 'Belum Validasi') {
            $('.menu-show').show();
        } else {
            $('.menu-show').hide();
        }
    }

    $(function () {
        $(".tgl_ba").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })

        $(".select-status-perpanjangan").select2({
            placeholder: " -- Pilih Status Perpanjangan -- "
        });

        $('.menu-show').hide();
        menu_show();
        $('#status_perpanjangan').change(function () {
            menu_show();
        });

        $('.pencet').hide();
        $("#check-all").click(function () {
            if ($(this).is(":checked")) {
                $(".check-item").prop("checked", true);
                $('.pencet').fadeIn("slow");
            } else {
                $('.pencet').fadeOut("slow");
                $(".check-item").prop("checked", false);
            }
        });

        $('.pencet').hide();
        $(".check-item").click(function () {
            if ($(this).is(":checked")) {
                $('.pencet').fadeIn("slow");
            } else {
                $('.pencet').fadeOut("slow");
            }
        });
    });
</script>