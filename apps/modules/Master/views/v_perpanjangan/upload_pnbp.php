<style>
    #btn_loading {
        display: none;
    }
    .space {
        margin-top: 8px;
    }
</style>
<!-- style loading -->
<div id ="loading2"></div>
<!-- -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        Upload <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i>Upload <?= $title ?></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-10"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-upload"></i> Upload <?= $title ?></div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <form class="form-horizontal" id="form-upload" method="POST" enctype="multipart/form-data">
                        <div class="form-body">
                            <br><br>
                            <div class="form-group row">      
                                <label for="inputFoto" class="col-sm-2 control-label">Surat Perintah Bayar</label>
                                <div class="col-sm-6 space">
                                    <input type="file" name="file_perintah" id="file_perintah" onchange="return checkMandatory('file_perintah')"/>
                                    <br>
                                    <?php if ($brand->file_perintah != '') { ?>
                                        <a href="<?= base_url() . $brand->file_perintah; ?>"><b><font face="verdana" size="2" color="red"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?= $brand->nama_file_perintah; ?></font></b></a>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-2 space">
                                    <small class="label pull-center bg-red">format .pdf | max 500 kb </small>
                                </div>
                            </div>
                            <div class="form-group row">      
                                <label for="inputFoto" class="col-sm-2 control-label">File PNBP</label>
                                <div class="col-sm-6 space">
                                    <input type="file" name="file_pnbp" id="file_pnbp" onchange="return checkMandatory('file_pnbp')"/>
                                    <br>
                                    <?php if ($brand->file_pnbp != '') { ?>
                                        <a href="<?= base_url() . $brand->file_pnbp; ?>"><b><font face="verdana" size="2" color="red"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?= $brand->nama_file_pnbp; ?></font></b></a>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-2 space">
                                    <small class="label pull-center bg-red">format .pdf | max 500 kb </small>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <div id="buka">
                                        <button type="submit" class="btn green"><i class="fa fa-upload"></i> Upload</button>
                                        <a href="<?= base_url($page); ?>" type="button" class="btn red ajaxify klik">Kembali</a>
                                    </div>
                                    <div id="btn_loading">
                                        <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>             
                </div>
            </div>
        </div> 
    </div>
</div>

<script type="text/javascript">
    function checkMandatory(id) {
        var fileInput = document.getElementById(id).value;
        if (fileInput != '') {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                toastr.error('File harus format .pdf', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                document.getElementById(id).value = '';
                return false;
            }
            var ukuran = document.getElementById(id);
            // validasi ukuran size file
            if (ukuran.files[0].size > 507200) {
                toastr.error('File harus maksimal 500 kb', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                ukuran.value = '';
                return false;
            }
            return true;
        }
    }

    $('#form-upload').submit(function (e) {
        e.preventDefault();
        var error = 0;
        var message = "";
        var data = $(this).serialize();
        if (error == 0) {
            var file_perintah = $("#file_perintah").val();
            var file_perintah = file_perintah.trim();
            if (file_perintah.length == 0) {
                error++;
                message = "File Perintah wajib di isi.";
            }
        }
        if (error == 0) {
            var file_pnbp = $("#file_pnbp").val();
            var file_pnbp = file_pnbp.trim();
            if (file_pnbp.length == 0) {
                error++;
                message = "File PNBB wajib di isi.";
            }
        }
        if (error == 0) {
            swal({
                title: "Upload <?= $title ?>?",
                text: "Yakin Memproses Data Ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $(".confirm").attr('disabled', 'disabled');
                $.ajax({
                    method: 'POST',
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    url: '<?= base_url('uploading-pnbp') . '/' . $brand->id_perpanjangan; ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        setTimeout("window.location='<?= base_url($page); ?>'", 500);
                        toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                    } else {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        setTimeout(location.reload.bind(location), 500);
                        toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                    }
                })
            });
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });
</script>