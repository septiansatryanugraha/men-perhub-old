<div class="page-content">
    <style type="text/css">
        #slider {
            margin-left: -2%;
        }
        #btn_loading {
            display: none;
        }
        #btn_loading2 {
            display: none;
        }
        .space {
            margin-top: 8px;
        }
        .table-ok {
            font-family: "arial", serif;
            font-size: 14px;
            margin-bottom: 20px;
        }
        .table-ok, th, td {
            padding: 10px 0px 0px 30px;
            margin-left: -20px;
            text-align: left;
        }
    </style>
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i><?= $title ?></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-view"></i> Lihat Nilai</div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#diklat" data-toggle="tab">Keterangan</a></li>
                                        <li><a href="#nilai" data-toggle="tab">Detail Nilai</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="active tab-pane" id="diklat">
                                            <div class="portlet-body form"> 
                                                <div class="form-body">
                                                    <br>
                                                    <table class="table-ok">
                                                        <tr><td>Jenis Jenjang</td><td>:</td><td><b><?= $brand->kategori_diklat ?></b></td></tr>
                                                        <tr><td>Nilai</td><td>:</td><td><b><?= $brand->kumulatif ?></b></td></tr>
                                                        <tr><td>Status</td><td>:</td><td><?= $brand->status ?></td></tr>
                                                        <tr><td>Tanggal</td><td>:</td><td><?= date_indo(date($brand->created_date)) ?></td></tr>
                                                    </table><hr>
                                                </div>            
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="nilai">
                                            <div class="portlet-body form"> 
                                                <div class="form-body">
                                                    <br>
                                                    <table class="table-ok">
                                                        <tr><td>CAT ( 30% )</td><td>:</td><td><b><?= $brand->ne1; ?></b></td><td><b><?= $brand->na1; ?></b></td></tr>
                                                        <tr><td>Lisan ( 30% )</td><td>:</td><td><b><?= $brand->ne2; ?></b></td><td><b><?= $brand->na2; ?></b></td></tr>
                                                        <tr><td>Praktek ( 40% )</td><td>:</td><td><b><?= $brand->ne3; ?></b></td><td><b><?= $brand->na3; ?></b></td></tr>
                                                    </table><hr> 
                                                </div>            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

