<style>
    #btn_loading {
        display: none;
    }
    .space {
        margin-top: 8px;
    }
    .table-ok {
        font-family: "arial", serif;
        font-size: 14px;
        margin-bottom: 20px;
    }
    .table-ok, th, td {
        padding: 10px 0px 0px 30px;
        margin-left: -20px;
        text-align: left;
    }
</style>
<!-- style loading -->
<div id ="loading2"></div>
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i>Ubah <?= $title ?></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-7"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-database"></i> Nilai Kompetensi</div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <form class="form-horizontal" id="form_update" method="POST" enctype="multipart/form-data">
                        <input type="hidden"  id="total_kumulatif2"  name="nilai">
                        <input type="hidden"  id="hasil_final2" name="status_kelulusan">
                        <div class="form-body">
                            <br><br>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static"><?= $brand->nama_pemohon; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static"><?= $brand->email; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Jenis Diklat</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static"><?= $brand->kategori_diklat; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label for="inputEmail3" class="col-sm-3 control-label">CAT ( 30% )</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="ne1" placeholder="NE" onkeyup="hitung1()" id="ne1"/><br>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="na1" placeholder="NA" id="total_nilai" /><br>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Lisan ( 30% )</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="ne2" placeholder="NE" onkeyup="hitung1()" id="ne2"/><br>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="na2" placeholder="NA" id="total_nilai2" /><br>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Praktek ( 40% )</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="ne3" placeholder="NE" onkeyup="hitung1()" id="ne3"/><br>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="na3" placeholder="NA" id="total_nilai3" /><br>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Hasil</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder="Nilai Kumulatif" id="total_kumulatif" disabled /><br>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder="Hasil" id="hasil_final" disabled /><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <div id="buka">
                                        <button type="submit" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                                        <a href="<?= base_url($page); ?>" type="button" class="btn red ajaxify klik">Kembali</a>
                                    </div>
                                    <div id="btn_loading">
                                        <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>             
                </div>
            </div>
        </div> 
        <div class="col-md-5"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-history"></i> History Pengajuan Diklat PKB</div>
                </div>
                <div class="portlet-body form"> 
                    <div class="form-body">
                        <br>
                        <?php if (!empty($ValData)) { ?>
                            <?php foreach ($ValData as $res) { ?>
                                <table class="table-ok">
                                    <tr><td>Jenis Diklat</td><td>:</td><td><b><?= $res->jenis_diklat ?></b></td></tr>
                                    <tr><td>Nilai</td><td>:</td><td><?= $res->kumulatif ?></td></tr>
                                    <tr><td>Status</td><td>:</td><td><?= $res->status ?></td></tr>
                                    <tr><td>Tanggal</td><td>:</td><td><?= date_indo(date($res->created_date)) ?></td></tr>
                                </table><hr>
                            <?php } ?>
                        <?php } else { ?>
                            <?= 'Belum ada history Pengajuan' ?>
                        <?php } ?>
                    </div>            
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function hitung1() {
        var hasil_nilai;
        var hasil_nilai2;
        var hasil_nilai3;
        var hasil_final;

        var data1 = document.getElementById('ne1').value;
        console.log(data1);

        var total_nilai = 0.3 * data1;
        console.log(total_nilai);
        hasil1 = total_nilai.toFixed(1);
        document.getElementById("total_nilai").value = hasil1;

        var data2 = document.getElementById('ne2').value;
        console.log(data2);

        var total_nilai2 = 0.3 * data2;
        console.log(total_nilai2);
        hasil2 = total_nilai2.toFixed(1);
        document.getElementById("total_nilai2").value = hasil2;

        var data3 = document.getElementById('ne3').value;
        console.log(data3);

        var total_nilai3 = 0.4 * data3;
        console.log(total_nilai3);
        hasil3 = total_nilai3.toFixed(1);
        document.getElementById("total_nilai3").value = hasil3;


        var total_kumulatif = total_nilai + total_nilai2 + total_nilai3;
        document.getElementById("total_kumulatif").value = total_kumulatif;
        document.getElementById("total_kumulatif2").value = total_kumulatif;

        if ((data1 > 69.9) && (data2 > 69.9) && (data3 > 69.9) && (total_kumulatif > 75))
            hasil_final = 'Lulus';
        else
            hasil_final = 'Tidak Lulus';
        document.getElementById("hasil_final").value = hasil_final;
        document.getElementById("hasil_final2").value = hasil_final;
    }

    $('#form_update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var error = 0;
        var message = "";
        if (error == 0) {
            var nilai = $("#ne1").val();
            var nilai = nilai.trim();
            if (nilai.length == 0) {
                error++;
                message = "Nilai wajib di isi.";
            }
        }
        if (error == 0) {
            var nilai2 = $("#ne2").val();
            var nilai2 = nilai2.trim();
            if (nilai2.length == 0) {
                error++;
                message = "Nilai wajib di isi.";
            }
        }
        if (error == 0) {
            var nilai3 = $("#ne3").val();
            var nilai3 = nilai3.trim();
            if (nilai3.length == 0) {
                error++;
                message = "Nilai wajib di isi.";
            }
        }
        if (error == 0) {
            swal({
                title: "Approve Data?",
                text: "Yakin Memproses Data Ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $(".confirm").attr('disabled', 'disabled');
                $.ajax({
                    method: 'POST',
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    url: '<?= base_url('update-nilai-diklat') . '/' . $brand->id_diklat; ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        setTimeout("window.location='<?= base_url($page); ?>'", 500);
                        toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                    } else {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                    }
                })
            });
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });
</script>