<style type="text/css">
    #btn_loading {
        display: none;
    }
</style>
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        Tambah <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i>Tambah <?= $title ?></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-pencil-square"></i> Data <?= $title ?></div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <form class="form-horizontal" id="form-tambah" method="POST">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Jenis Surat </label>
                                <div class="col-sm-4">
                                    <select name="tipe_surat" id="tipe_surat" class="form-control select-tipe-surat" aria-describedby="sizing-addon2">
                                        <option></option>
                                        <option value="Surat Panggilan Diklat">Diklat PKB</option>
                                        <option value="Surat Panggilan Jenjang">Kenaikan Jenjang</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Subjek</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" placeholder="Subjek" name="subjek" id="subjek" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Nomor Surat </label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" placeholder="Nomor surat" name="no_surat" id="no_surat" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Surat</label>
                                <div class="col-sm-3">
                                    <input type="text" id="tgl_surat" class="form-control tgl_surat" name="tgl_surat" placeholder="Tanggal Surat" value="<?= date('d-m-Y'); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Nomor Berita Acara </label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" placeholder="Nomor berita acara" name="no_ba" id="no_ba" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Berita Acara</label>
                                <div class="col-sm-3">
                                    <input type="text" id="tgl_ba" class="form-control tgl_surat" name="tgl_ba" placeholder="Tanggal Surat" value="<?= date('d-m-Y'); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <div id="buka">
                                        <button type="submit" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                                        <a href="<?= base_url($page); ?>" type="button" class="btn red ajaxify klik">Kembali</a>
                                    </div>
                                    <div id="btn_loading">
                                        <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>              
    </div>
</div>

<script type="text/javascript">
    $('#form-tambah').submit(function (e) {
        e.preventDefault();
        var error = 0;
        var message = "";

        if (error == 0) {
            var tipe_surat = $("#tipe_surat").val();
            var tipe_surat = tipe_surat.trim();
            if (tipe_surat.length == 0) {
                error++;
                message = "Jenis Surat wajib di isi.";
            }
        }
        if (error == 0) {
            var subjek = $("#subjek").val();
            var subjek = subjek.trim();
            if (subjek.length == 0) {
                error++;
                message = "Subjek wajib di isi.";
            }
        }
        if (error == 0) {
            var no_surat = $("#no_surat").val();
            var no_surat = no_surat.trim();
            if (no_surat.length == 0) {
                error++;
                message = "No Surat wajib di isi.";
            }
        }
        if (error == 0) {
            var tgl_surat = $("#tgl_surat").val();
            var tgl_surat = tgl_surat.trim();
            if (no_surat.length == 0) {
                error++;
                message = "Tanggal Surat wajib di isi.";
            }
        }
        if (error == 0) {
            var no_ba = $("#no_ba").val();
            var no_ba = no_ba.trim();
            if (no_ba.length == 0) {
                error++;
                message = "Nomor Berita Acara wajib di isi.";
            }
        }
        if (error == 0) {
            var tgl_ba = $("#tgl_ba").val();
            var tgl_ba = tgl_ba.trim();
            if (tgl_ba.length == 0) {
                error++;
                message = "Tanggal Berita Acara wajib di isi.";
            }
        }
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?= base_url('save-surat'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-tambah").reset();
                    $("#buka").show();
                    $("#btn_loading").hide();
                    toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                }
            })
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });

    $(function () {
        $(".select-tipe-surat").select2({
            placeholder: " -- pilih -- "
        });
        // untuk datepicker
        $(".tgl_surat").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });
</script>