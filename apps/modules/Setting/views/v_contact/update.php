<style type="text/css">
    #btn_loading {
        display: none;
    }
</style>
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        Ubah <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i>Ubah <?= $title ?></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-database"></i> Data Konfigurasi Menu</div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <form id="form-update" class="form-horizontal" method="POST">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Email </label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="email" id="email" aria-describedby="sizing-addon2" value="<?= $brand->email; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Telpon</label>
                                <div class="col-sm-5">
                                    <input type="text" id="no_telp" name="no_telp" class="form-control" placeholder ="No Telp" aria-describedby="sizing-addon2" value="<?= $brand->no_telp; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Alamat</label>
                                <div class="col-sm-5">
                                    <textarea rows="4" cols="50" id="alamat" name="alamat"><?= $brand->alamat; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Deskripsi Aplikasi</label>
                                <div class="col-sm-5">
                                    <textarea rows="4" cols="50" id="deskripsi" name="deskripsi"><?= $brand->deskripsi; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <div id="buka">
                                        <button type="submit" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                                        <a href="<?= base_url($page); ?>" type="button" class="btn red ajaxify">Kembali</a>
                                    </div>
                                    <div id="btn_loading">
                                        <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.row -->
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var error = 0;
        var message = "";
        if (error == 0) {
            var no_telp = $("#no_telp").val();
            var no_telp = no_telp.trim();
            if (no_telp.length == 0) {
                error++;
                message = "telpon wajib di isi.";
            }
        }
        if (error == 0) {
            var email = $("#email").val();
            var email = email.trim();
            if (email.length == 0) {
                error++;
                message = "Email wajib di isi.";
            } else if (!cekemail(email)) {
                error++;
                message = "Inputan harus Email !! ";
            }
        }
        if (error == 0) {
            var alamat = $("#alamat").val();
            var alamat = alamat.trim();
            if (alamat.length == 0) {
                error++;
                message = "Alamat wajib di isi.";
            }
        }
        if (error == 0) {
            var deskripsi = $("#deskripsi").val();
            var deskripsi = deskripsi.trim();
            if (deskripsi.length == 0) {
                error++;
                message = "Deskripsi aplikasi wajib di isi.";
            }
        }
        if (error == 0) {
            swal({
                title: "Simpan Data?",
                text: "Yakin Memproses Data Ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $.ajax({
                    method: 'POST',
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    url: '<?= base_url('update-konfigurasi-kontak') . '/' . $brand->id; ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        swal("Success", result.pesan, "success");
                        setTimeout("window.location='<?= base_url($page); ?>'", 500);
                    } else {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        swal("Warning", result.pesan, "warning");
                    }
                });
            });
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });

    function cekemail(a) {
        re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(a);
    }
</script>