<style>
    #osas {
        color:red;
        font-weight:bold;
        margin-left:0px;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->

    <div class="box">
        <div class="row">
            <div class="col-md-9" id="newContain">

                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Data</h3>
                </div>

                <!-- /.box-header -->
                <!-- form start -->

                <form class="form-horizontal" id="form-tambah" method="POST">
                    <input type="hidden" name="created_by" value="<?= $userdata->nama; ?>">

                    <div class="box-body">

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email </label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" placeholder ="email" name="email" id="email" aria-describedby="sizing-addon2">
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Telpon </label>
                            <div class="col-sm-5">
                                <input type="text" id="telp" name="no_telp" class="form-control" placeholder ="No Telp" aria-describedby="sizing-addon2">
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Alamat</label>
                            <div class="col-sm-5">
                                <textarea rows="3" cols="40" id="alamat" name="alamat"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                        <button name="simpan" id="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>

                        <a class="klik ajaxify" href="<?= site_url('contact'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
                    </div>
                </form>

            </div>
            <!-- /.box -->

        </div>
        <!-- /.row -->
    </div>

</section>



<script type="text/javascript">
    //Proses Controller logic ajax

    function cekemail(a) {
        re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(a);
    }


    $('#form-tambah').submit(function (e) {
        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var telp = $("#telp").val();
        var telp = telp.trim();

        if (error == 0) {
            if (telp.length == 0) {
                error++;
                message = "telpon wajib di isi.";
            }

        }

        var email = $("#email").val();
        var email = email.trim();

        if (error == 0) {
            if (email.length == 0) {
                error++;
                message = "Email wajib di isi.";
            } else if (!cekemail(email)) {
                error++;
                message = "Inputan harus Email !! ";
            }
        }

        var alamat = $("#alamat").val();
        var alamat = alamat.trim();

        if (error == 0) {
            if (alamat.length == 0) {
                error++;
                message = "Alamat wajib di isi.";
            }
        }

        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?= site_url('Master/Contact/prosesAdd'); ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-tambah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    save_berhasil();
                    setTimeout(location.reload.bind(location), 500);
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    gagal();

                }
            })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });

</script>





