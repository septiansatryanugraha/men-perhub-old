<div class="page-content">
    <style type="text/css">
        #slider{
            margin-left: -2%;
        }
        #btn_loading {
            display: none;
        }
        #btn_loading2 {
            display: none;
        }
    </style>
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a>           
            </li>
            <?= (isset($breadcrumb)) ? $breadcrumb : ''; ?>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i> Profile
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-3">
                                <!-- Profile Image -->
                                <div class="box box-primary">
                                    <div class="box-body box-profile">
                                        <img class="profile-user-img img-responsive img-circle2" width="150px" src="<?= base_url(); ?>upload/user/<?= $this->session->userdata('foto') ?>" alt="User profile picture">
                                        <h3 class="profile-username text-center"><?= $userdata->nama; ?></h3>
                                        <?php
                                        foreach ($datagrup as $data) {
                                            if ($data->grup_id == $this->session->userdata('grup_id')) {

                                                ?>
                                                <p class="text-muted text-center"><?= $data->nama_grup; ?></p>
                                                <?php
                                            }
                                        }

                                        ?>  

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#settings" data-toggle="tab">Settings</a></li>
                                        <li><a href="#password" data-toggle="tab">Ubah Password</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="active tab-pane" id="settings">
                                            <form class="form-horizontal" id="form-update" method="POST">
                                                <div class="form-group">
                                                    <label for="inputUsername" class="col-sm-2 control-label">Username</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control" id="username" placeholder="Username" name="username" value="<?= $this->session->userdata('username') ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputNama" class="col-sm-2 control-label">Name</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control" placeholder="Name" name="nama" value="<?= $this->session->userdata('nama') ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputNama" class="col-sm-2 control-label">Level</label>
                                                    <div class="col-sm-6">
                                                        <?php
                                                        foreach ($datagrup as $data) {
                                                            if ($data->grup_id == $this->session->userdata('grup_id')) {

                                                                ?>
                                                                <input type="text" class="form-control" value="<?= $data->nama_grup; ?>" readonly>
                                                                <?php
                                                            }
                                                        }

                                                        ?>  
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputNama" class="col-sm-2 control-label">Status</label>
                                                    <div class="col-sm-6">
                                                        <?php
                                                        foreach ($dataStatus as $data) {
                                                            if ($data->id_status == $this->session->userdata('status')) {

                                                                ?>
                                                                <p class="form-control-static"><?= $data->nama; ?></p>
                                                                <?php
                                                            }
                                                        }

                                                        ?>
                                                    </div>
                                                </div>
                                                <div id="slider">
                                                    <img class="img-thumbnail" src="<?= base_url(); ?>/assets/tambahan/gambar/no-image.jpg" width="130px" />
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputFoto" class="col-sm-2 control-label">Foto</label>
                                                    <div class="col-sm-5">
                                                        <input type="file" class="form-control" placeholder="Foto" name="foto" id="gambar" onchange="return fileValidation()"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <div id="buka">
                                                            <button type="submit" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                                                        </div>
                                                        <div id="btn_loading">
                                                            <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!---- ini buat update password -->
                                        <div class="tab-pane" id="password">
                                            <form class="form-horizontal" id="form-update-pass" method="POST">
                                                <div class="form-group">
                                                    <label for="passLama" class="col-sm-2 control-label">Password Lama</label>
                                                    <div class="col-sm-6">
                                                        <input type="password" class="form-control" id="psw_lama" placeholder="Password Lama" name="passLama">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="passBaru" class="col-sm-2 control-label">Password Baru</label>
                                                    <div class="col-sm-6">
                                                        <input type="password" class="form-control" id="psw_baru" placeholder="Password Baru" name="passBaru">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="passKonf" class="col-sm-2 control-label">Konfirmasi Password</label>
                                                    <div class="col-sm-6">
                                                        <input type="password" class="form-control" id="confirmasi" placeholder="Konfirmasi Password" name="passKonf">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <div id="buka2">
                                                            <button type="submit" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                                                        </div>
                                                        <div id="btn_loading2">
                                                            <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#form-update').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            swal({
                title: "Simpan Data?",
                text: "Yakin Memproses Data Ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $.ajax({
                    method: 'POST',
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    url: '<?= base_url('update-profile'); ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    $("#buka").show();
                    $("#btn_loading").hide();
                    if (result.status == true) {
                        swal("Success", result.pesan, "success");
                        setTimeout(location.reload.bind(location), 500);
                    } else {
                        swal("Warning", result.pesan, "warning");
                    }
                });
            });
        });
    });

    $(document).ready(function () {
        $('#form-update-pass').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            swal({
                title: "Simpan Data?",
                text: "Yakin Memproses Data Ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $.ajax({
                    method: 'POST',
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    url: '<?= base_url('update-password'); ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    $("#buka").show();
                    $("#btn_loading").hide();
                    if (result.status == true) {
                        swal("Success", result.pesan, "success");
                        setTimeout(location.reload.bind(location), 500);
                    } else {
                        swal("Warning", result.pesan, "warning");
                    }
                });
            });
        });
    });
</script>