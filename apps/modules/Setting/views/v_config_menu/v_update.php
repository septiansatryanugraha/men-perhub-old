<style type="text/css">
    #btn_loading {
        display: none;
    }
</style>
<div class="page-content"> 
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        Ubah <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i>Ubah <?= $title ?></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-database"></i> Data Konfigurasi Menu</div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <form class="form-horizontal" id="form-update" method="POST">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Nama Menu</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" placeholder="nama menu" name="nama_konfig" id="nama_konfig" aria-describedby="sizing-addon2" value="<?= $brand->nama_konfig; ?>">
                                </div>
                            </div>	
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status </label>
                                <div class="col-sm-3">
                                    <select name="status" class="form-control select-status" id="status">
                                        <option></option>
                                        <?php foreach ($status as $data) { ?>
                                            <option value="<?= $data->nama ?>" <?= ($data->nama == $brand->status) ? "selected" : ""; ?>><?= $data->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <div id="buka">
                                        <button type="submit" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                                        <a href="<?= base_url($page); ?>" type="button" class="btn red ajaxify klik">Kembali</a>
                                    </div>
                                    <div id="btn_loading">
                                        <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>             
                </div>
            </div>
        </div>              
    </div>
</div>

<script type="text/javascript" src="<?= theme(); ?>/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript">
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var error = 0;
        var message = "";
        if (error == 0) {
            var nama_konfig = $("#nama_konfig").val();
            var nama_konfig = nama_konfig.trim();
            if (nama_konfig.length == 0) {
                error++;
                message = "Nama Menu wajib di isi.";
            }
        }
        if (error == 0) {
            var status = $("#status").val();
            var status = status.trim();
            if (status.length == 0) {
                error++;
                message = "Status wajib di isi.";
            }
        }
        if (error == 0) {
            swal({
                title: "Simpan Data?",
                text: "Yakin Memproses Data Ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $(".confirm").attr('disabled', 'disabled');
                $.ajax({
                    method: 'POST',
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    url: '<?= base_url('update-konfigurasi-menu') . '/' . $brand->id_config; ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    $("#buka").show();
                    $("#btn_loading").hide();
                    if (result.status == true) {
                        swal("Success", result.pesan, "success");
                        setTimeout("window.location='<?= base_url($page); ?>'", 500);
                    } else {
                        swal("Warning", result.pesan, "warning");
                    }
                });
            });
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });

    $(function () {
        $(".select-status").select2({
            placeholder: " -- Pilih status -- "
        });
    });
</script>	