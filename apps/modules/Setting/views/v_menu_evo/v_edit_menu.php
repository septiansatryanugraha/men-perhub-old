<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <li><i class="fa fa-angle-right"></i>Data <?= $title ?></li>
        </ul>
    </div>
    <style>
        .select-width {width:310px;}
        .gap_footer {
            margin-top: 100px;
        }
        #btn_loading {
            display: none;
        }
        #btn_loading2 {
            display: none;
        }
        .menu-panjang {
            width: 400px;
        }
    </style>
    <div class="row">
        <div class="col-md-5">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fas fa-list"></i> Menu Order
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="form-body">
                        <menu id="nestable-menu">
                            <button type="button" class="btn btn-warning" data-action="expand-all"><i class="fa fa-minus"></i> Expand All</button>
                            <button type="button" class="btn btn-primary" data-action="collapse-all"><i class="fa fa-plus"></i> Collapse All</button>
                        </menu>
                        <div class="form-group">
                            <div class="dd menu-panjang" id="nestable" style="margin-bottom: 35px;">
                                <?php
                                $ref = [];
                                $items = [];

                                foreach ($dataMenu3 as $data) {
                                    $thisRef = &$ref[$data->id_menu];
                                    $thisRef['parent'] = $data->parent;
                                    $thisRef['nama_menu'] = $data->nama_menu;
                                    $thisRef['link'] = $data->link;
                                    $thisRef['id_menu'] = $data->id_menu;
                                    $thisRef['icon'] = $data->icon;

                                    if ($data->parent == 0) {
                                        $items[$data->id_menu] = &$thisRef;
                                    } else {
                                        $ref[$data->parent]['child'][$data->id_menu] = &$thisRef;
                                    }
                                }

                                function get_menu($items, $class = 'dd-list')
                                {
                                    $html = "<ol class=\"" . $class . "\" id=\"menu-id\">";

                                    foreach ($items as $key => $value) {
                                        $html .= '<li class="dd-item dd3-item" data-id="' . $value['id_menu'] . '" >
                <div class="dd-handle dd3-handle"></div>
                <div class="dd3-content"><i class="' . $value['icon'] . '"></i>&nbsp;&nbsp; <span id="label_show' . $value['id_menu'] . '">' . $value['nama_menu'] . '</span> 
                <span class="span-right"><span id="link_show' . $value['id_menu'] . '"></span> &nbsp;&nbsp;' . anchor('edit-menu/' . $value['id_menu'], '<span tooltip="Edit Data"><i class="fa fa-edit"></i></span> ', ' class="ajaxify" ') . '</span> 
                </div>';
                                        if (array_key_exists('child', $value)) {
                                            $html .= get_menu($value['child'], 'child');
                                        }
                                        $html .= "</li>";
                                    }
                                    $html .= "</ol>";
                                    return $html;
                                }
                                print get_menu($items);

                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div id="buka2">          
                            <button name="simpan" type="submit" id="save2" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                        <div id="btn_loading2">
                            <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fas fa-edit"></i> Ubah Menu
                    </div>
                    <div class="actions">
                        <a href="<?= base_url('menu') ?>" class="btn default btn-sm ajaxify"><i class="fa fa-list-alt"></i> Kembali</a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form class="form-horizontal" id="form-update" method="POST">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama Menu</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" placeholder="nama menu" name="nama_menu" id="nama_menu" aria-describedby="sizing-addon2" value="<?= $dataMenu->nama_menu; ?>">
                                </div>
                            </div>  
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Icon</label>
                                <div class="col-sm-5 wrap">
                                    <input type="text" class="form-control ikonpicker signup-input text-value" placeholder="icon menu" name="icon" id="icon" aria-describedby="sizing-addon2" value="<?= $dataMenu->icon; ?>">
                                    <div class="ex">&times;</div>
                                </div>    
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Link Menu</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" placeholder="link menu" name="link" id="link" aria-describedby="sizing-addon2" value="<?= $dataMenu->link; ?>">
                                </div>
                            </div>  
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">kode Menu</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" placeholder="kode menu" name="kode_menu" id="kode_menu" aria-describedby="sizing-addon2" value="<?= $dataMenu->kode_menu; ?>">
                                </div>
                            </div>  
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Jenis Menu</label>
                                <div class="col-sm-5">
                                    <select name="parent" id="parent" class="form-control select-menu" aria-describedby="sizing-addon2">
                                        <option value="0" <?= ($dataMenu->parent == 0) ? "selected" : ""; ?>>Menu Utama</option>
                                        <option disabled="disabled">------------ SUB MENU FROM ------------</option>
                                        <?php foreach ($dataMenu2 as $data) { ?>
                                            <option value="<?= $data->id_menu ?>" <?= ($dataMenu->parent == $data->id_menu) ? "selected" : ""; ?>><?= $data->nama_menu; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Urutan</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" placeholder="urutan" name="urutan" id="urutan" aria-describedby="sizing-addon2" value="<?= $dataMenu->urutan; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Menu Available</label>
                                <div class="col-xs-3">
                                    <?php $menu = explode(",", $dataMenu->menu_file); ?>
                                    <input type='checkbox' name='menu_file[]' value="view" <?php
                                    foreach ($menu as $value) {
                                        echo ('view' == $value) ? 'checked' : '';
                                    }

                                    ?> /> &nbsp;View<br>
                                    <input type='checkbox' name='menu_file[]' value="add" <?php
                                    foreach ($menu as $value) {
                                        echo ('add' == $value) ? 'checked' : '';
                                    }

                                    ?> /> &nbsp;Add<br>
                                    <input type='checkbox' name='menu_file[]' value="edit" <?php
                                    foreach ($menu as $value) {
                                        echo ('edit' == $value) ? 'checked' : '';
                                    }

                                    ?> /> &nbsp;Edit<br>
                                    <input type='checkbox' name='menu_file[]' value="del" <?php
                                    foreach ($menu as $value) {
                                        echo ('del' == $value) ? 'checked' : '';
                                    }

                                    ?> /> &nbsp;Delete<br>                    
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="form-actions">
                            <div id="buka"> 
                                <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                                <input type="hidden" id="nestable-output">
                                <a class="klik ajaxify" href="<?= base_url('menu'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>
                            <div id="btn_loading">
                                <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url(); ?>assets/plugins/menu/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/menu/jquery.nestable.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ex').click(function () {
            $('.signup-input').val("");
        });

        var updateOutput = function (e) {
            var list = e.length ? e : $(e.target), output = list.data('output');
            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };

        // activate Nestable for list 1
        $('#nestable').nestable({group: 1}).on('change', updateOutput);

        // output initial serialised data
        updateOutput($('#nestable').data('output', $('#nestable-output')));

        $('#nestable-menu').on('click', function (e) {
            var target = $(e.target),
                    action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });

        $('#form-update').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            var error = 0;
            var message = "";
            if (error == 0) {
                var nama_menu = $("#nama_menu").val();
                var nama_menu = nama_menu.trim();
                if (nama_menu.length == 0) {
                    error++;
                    message = "Nama menu harus di isi.";
                }
            }
            if (error == 0) {
                var icon = $("#icon").val();
                var icon = icon.trim();
                if (icon.length == 0) {
                    error++;
                    message = "Icon harus di isi.";
                }
            }
            if (error == 0) {
                var link = $("#link").val();
                var link = link.trim();
                if (link.length == 0) {
                    error++;
                    message = "Link menu harus di isi.";
                }
            }
            if (error == 0) {
                var kode_menu = $("#kode_menu").val();
                var kode_menu = kode_menu.trim();
                if (kode_menu.length == 0) {
                    error++;
                    message = "Kode menu harus di isi.";
                }
            }
            if (error == 0) {
                var parent = $("#parent").val();
                var parent = parent.trim();
                if (parent.length == 0) {
                    error++;
                    message = "Jenis menu harus di isi.";
                }
            }
            if (error == 0) {
                var urutan = $("#urutan").val();
                var urutan = urutan.trim();
                if (urutan.length == 0) {
                    error++;
                    message = "Urutan menu harus di isi.";
                }
            }
            if (error == 0) {
                swal({
                    title: "Simpan Data?",
                    text: "Yakin Memproses Data Ini ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Simpan",
                    confirmButtonColor: '#dc1227',
                    customClass: ".sweet-alert button",
                    closeOnConfirm: false,
                    html: true
                }, function () {
                    $.ajax({
                        method: 'POST',
                        beforeSend: function () {
                            $("#buka").hide();
                            $("#btn_loading").show();
                        },
                        url: '<?= base_url('update-menu') . '/' . $dataMenu->id_menu; ?>',
                        type: "post",
                        data: data,
                        processData: false,
                        contentType: false,
                        cache: false,
                    }).done(function (data) {
                        var result = jQuery.parseJSON(data);
                        $("#buka").show();
                        $("#btn_loading").hide();
                        if (result.status == true) {
                            setTimeout("window.location='<?= base_url('menu'); ?>'", 500);
                            toastr.success(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                                "closeButton": true});
                        } else {
                            toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                                "closeButton": true});
                        }
                    })
                });
            }
        });

        $('.dd').on('change', function () {
            var dataString = {
                data: $("#nestable-output").val(),
            };
            console.log(dataString);
            $.ajax({
                type: "POST",
                url: "<?= base_url('save-sort-menu'); ?>",
                data: dataString,
                cache: false,
                success: function (data) {
                }, error: function (xhr, status, error) {
                    alert(error);
                },
            });
        });

        $("#save2").click(function () {
            $("#buka2").hide();
            $("#btn_loading2").show();
            var dataString = {
                data: $("#nestable-output").val(),
            };
            $.ajax({
                type: "POST",
                url: "<?= base_url('save-sort-menu'); ?>",
                data: dataString,
                cache: false,
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        $("#btn_loading2").hide();
                        $("#buka2").show();
                        setTimeout(location.reload.bind(location), 500);
                        toastr.success(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                    } else {
                        $("#btn_loading2").hide();
                        $("#buka2").show();
                        toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                    }
                }
            });

            $(".select-menu").select2({
                placeholder: " -- Pilih Jenis Menu -- "
            });

            $('.ikonpicker').iconpicker();
        });

        $(function () {
            $('.menu-show').hide();
            $('#type').change(function () {
                if ($('#type').val() == 'sub-menu') {
                    $('.menu-show').show();
                } else {
                    $('.menu-show').hide();
                }
            });
        });
    });

    (function () {
        if (window.localStorage) {
            if (!localStorage.getItem('firstLoad')) {
                localStorage['firstLoad'] = true;
                window.location.reload();
            } else
                localStorage.removeItem('firstLoad');
        }
    })();
</script>