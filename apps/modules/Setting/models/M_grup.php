<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_grup extends CI_Model
{
    const __tableName = 'grup';
    const __tableId = 'grup_id';

    public function getData()
    {
        $this->db->from(self::__tableName);
        $this->db->where('deleted_date IS NULL');
        $data = $this->db->get();

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE deleted_date IS NULL AND " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    // VIEW USER PRIVILEGE
    public function aksesGrup($id)
    {
        if (!$id)
            return FALSE;
        $query = "  SELECT 
                        a.id_menuakses, b.id_menu as menus, a.id_menu, b.nama_menu, a.grup_id, b.menuparent, 
                        c.nama_grup, a.view, a.add, a.edit, a.del, b.menu_file
					FROM 
                    (
                        SELECT * FROM menu_akses WHERE grup_id = '{$id}'
                    ) AS a
					RIGHT JOIN 
                    (
                        SELECT a.*, b.nama_menu AS menuparent 
                        FROM tbl_menu a 
                        LEFT JOIN tbl_menu b ON a.parent = b.id_menu AND b.deleted_date IS NULL
                        WHERE a.deleted_date IS NULL
                    ) AS b ON a.id_menu = b.id_menu
					LEFT JOIN grup c ON a.grup_id = c.grup_id AND c.deleted_date IS NULL
					ORDER BY urutan ASC";
        $data = $this->db->query($query);

        return $data->result();
    }
}
