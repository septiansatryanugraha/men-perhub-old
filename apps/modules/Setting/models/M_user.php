<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_User extends CI_Model
{
    const __tableName = 'admin';
    const __tableId = 'id';

    public function getData()
    {
        $sql = "SELECT " . self::__tableName . ".*
                ,grup.nama_grup AS grup_id
                ,status_grup.nama AS status 
                FROM " . self::__tableName . "
                LEFT JOIN(grup)on " . self::__tableName . ".grup_id = grup.grup_id
                LEFT JOIN(status_grup)on " . self::__tableName . ".status = status_grup.id_status
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND " . self::__tableName . ".hidden > 0
                ORDER BY " . self::__tableName . ".id DESC ";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE deleted_date IS NULL AND hidden > 0 AND " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function selectGroup()
    {
        $data = $this->db->get('grup');

        return $data->result();
    }
}
