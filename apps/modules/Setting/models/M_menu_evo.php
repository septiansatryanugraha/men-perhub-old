<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_menu_evo extends CI_Model
{
    const __tableName = 'tbl_menu';
    const __tableId = 'id_menu';

    public function selectDataMenu()
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE deleted_date IS NULL ORDER BY urutan ASC";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE deleted_date IS NULL AND " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function pilihMenu()
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE deleted_date IS NULL AND menu_file in ('view')";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectParent($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE deleted_date IS NULL AND parent = '{$id}' ";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function ajaxFind($find)
    {
        $this->db->from(self::__tableName);
        $this->db->where('deleted_date IS NULL');
        $this->db->where("menu_file", "view");
        $this->db->like('nama_menu', $find);

        return $this->db->get()->result_array();
    }
}
