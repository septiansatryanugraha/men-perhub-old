<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_evo extends AUTH_Controller
{
    const __title = 'Menu ';
    const __kode_menu = 'menu';
    const __folder = 'v_menu_evo/';
    const __tableName = 'tbl_menu';
    const __tableId = 'id_menu';
    const __model = 'M_menu_evo';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
    }

    public function index()
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['dataMenu'] = $this->M_menu_evo->selectDataMenu();
            $this->loadkonten(self::__folder . 'v_home', $data);
        }
    }

    public function ajaxMenu()
    {
        $dataMenu = $this->M_menu_evo->ajaxFind($this->input->get('q'));

        echo json_encode($dataMenu);
    }

    public function Edit($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">Data " . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_menu_evo->selectById($id);
            if ($brand != null) {
                $data['dataMenu'] = $brand;
                $data['dataMenu2'] = $this->M_menu_evo->pilihMenu();
                $data['dataMenu3'] = $this->M_menu_evo->selectDataMenu();
                $this->loadkonten(self::__folder . 'v_edit_menu', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    function parseJsonArray($jsonArray, $parentID = 0)
    {
        $return = [];
        foreach ($jsonArray as $subArray) {
            $returnSubSubArray = [];
            if (isset($subArray->children)) {
                $returnSubSubArray = $this->parseJsonArray($subArray->children, $subArray->id);
            }

            $return[] = ['id_menu' => $subArray->id, 'parentID' => $parentID];
            $return = array_merge($return, $returnSubSubArray);
        }

        return $return;
    }

    public function saveSort()
    {
        $errCode = 0;
        $errMessage = "";

        $readbleArray = $this->parseJsonArray(json_decode($_POST['data']));
        $this->db->trans_begin();
        $i = 0;
        foreach ($readbleArray as $row) {
            if ($errCode == 0) {
                try {
                    $i++;
                    $data = [
                        'parent' => $row['parentID'],
                        'urutan' => $i
                    ];
                    $result = $this->db->update(self::__tableName, $data, [self::__tableId => $row['id_menu']]);
                } catch (Exception $ex) {
                    $errCode++;
                    $errMessage = $ex->getMessage();
                }
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function prosesTambah()
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $namaMenu = $this->input->post('nama_menu');
        $icon = $this->input->post('icon');
        $link = $this->input->post('link');
        $kodeMenu = $this->input->post('kode_menu');
        $parent = $this->input->post('parent');
        $urutan = $this->input->post('urutan');
        $menuFile = $this->input->post('menu_file');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($namaMenu) == 0) {
                $errCode++;
                $errMessage = "Nama Menu wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($icon) == 0) {
                $errCode++;
                $errMessage = "Icon wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($link) == 0) {
                $errCode++;
                $errMessage = "Link wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($kodeMenu) == 0) {
                $errCode++;
                $errMessage = "Kode manu wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($parent) == 0) {
                $errCode++;
                $errMessage = "Parent wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($urutan) == 0) {
                $errCode++;
                $errMessage = "Urutan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'nama_menu' => $namaMenu,
                    'icon' => $icon,
                    'link' => $link,
                    'kode_menu' => $kodeMenu,
                    'parent' => $parent,
                    'urutan' => $urutan,
                    'menu_file' => implode(",", $menuFile),
                    'created_date' => $datetime,
                    'created_by' => $username,
                    'updated_date' => $datetime,
                    'updated_by' => $username,
                ];
                $result = $this->db->insert(self::__tableName, $data);
                $insertId = $this->db->insert_id();

                $out['menu'] = '<li class="dd-item dd3-item" data-id="' . $insertId . '" >
	                    <div class="dd-handle dd3-handle"></div>
	                    <div class="dd3-content"><i class="' . $icon . '"></i><span id="label_show' . $insertId . '">' . $namaMenu . '</span>
	                        <span class="span-right"><span id="link_show' . $insertId . '">' . $link . '</span> &nbsp;&nbsp; 
	                        	<a class="edit-button" id="' . $insertId . '" label="' . $namaMenu . '" link="' . $link . '" ><i class="fa fa-pencil"></i></a>
                           		<a class="del-button" id="' . $insertId . '"><i class="fa fa-trash"></i></a>
	                        </span> 
	                    </div>';
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $namaMenu = $this->input->post('nama_menu');
        $icon = $this->input->post('icon');
        $link = $this->input->post('link');
        $kodeMenu = $this->input->post('kode_menu');
        $parent = $this->input->post('parent');
        $urutan = $this->input->post('urutan');
        $menuFile = $this->input->post('menu_file');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_menu_evo->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($namaMenu) == 0) {
                $errCode++;
                $errMessage = "Nama Menu wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($icon) == 0) {
                $errCode++;
                $errMessage = "Icon wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($link) == 0) {
                $errCode++;
                $errMessage = "Link wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($kodeMenu) == 0) {
                $errCode++;
                $errMessage = "Kode manu wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($parent) == 0) {
                $errCode++;
                $errMessage = "Parent wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($urutan) == 0) {
                $errCode++;
                $errMessage = "Urutan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'nama_menu' => $namaMenu,
                    'icon' => $icon,
                    'link' => $link,
                    'kode_menu' => $kodeMenu,
                    'parent' => $parent,
                    'urutan' => $urutan,
                    'menu_file' => implode(",", $menuFile),
                    'updated_date' => $datetime,
                    'updated_by' => $username,
                ];
                $this->db->update(self::__tableName, $data, [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    function recursiveDelete($id)
    {
        $query = $this->M_menu_evo->selectParent($id);
        if ($query > 0) {
            foreach ($query as $current) {
                $this->recursiveDelete($current['id_menu']);
            }
        }
        $this->db->update(self::__tableName, ['deleted_date' => date('Y-m-d H:i:s')], [self::__tableId => $id]);
    }

    function deleteMenu()
    {
        $result = $this->recursiveDelete($_POST['id']);
        if ($result > 0) {
            $out = ['status' => false, 'pesan' => 'Data has been deleted'];
        } else {
            $out = ['status' => true, 'pesan' => 'Data has been deleted'];
        }

        echo json_encode($out);
    }
}
