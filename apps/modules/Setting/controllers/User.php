<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends AUTH_Controller
{
    const __title = 'User Adminstrator ';
    const __kode_menu = 'user';
    const __folder = 'v_user/';
    const __tableName = 'admin';
    const __tableId = 'id';
    const __model = 'M_user';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
        $this->load->model('M_utilities');
    }

    public function index()
    {
        $data['userdata'] = $this->session->userdata();
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $this->loadkonten(self::__folder . 'v_home', $data);
        }
    }

    public function ajaxList()
    {
        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);

        $list = $this->M_user->getData();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $user) {
            $no++;
            $row = [];
            $row[] = $no;
            $row[] = '<a href="' . base_url() . 'upload/user/' . $user->foto . '" target="_blank"><img class="img-thumbnail" src="' . base_url() . 'upload/user/' . $user->foto . '"   width="90" /></a>';
            $row[] = $user->email;
            $row[] = $user->nama;
            $row[] = $user->grup_id;
            $row[] = '<span class="badge bg-green">' . $user->status . '</span">';

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-user') . "/" . $user->id . "' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-user' data-toggle='tooltip' data-placement='top' data-id='" . $user->id . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function Add()
    {
        $data['userdata'] = $this->session->userdata();
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">" . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['datagrup'] = $this->M_user->selectGroup();
            $this->loadkonten(self::__folder . 'v_tambah-user', $data);
        }
    }

    public function prosesTambah()
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $nama = $this->input->post('nama');
        $email = $this->input->post('email');
        $usernameInput = $this->input->post('username');
        $password = $this->input->post('password');
        $grupId = $this->input->post('grup_id');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($nama) == 0) {
                $errCode++;
                $errMessage = "Nama wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($email) == 0) {
                $errCode++;
                $errMessage = "Email wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($usernameInput) == 0) {
                $errCode++;
                $errMessage = "Username wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($password) == 0) {
                $errCode++;
                $errMessage = "Password wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($grupId) == 0) {
                $errCode++;
                $errMessage = "Group wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'grup_id' => $grupId,
                    'nama' => $nama,
                    'email' => $email,
                    'username' => $usernameInput,
                    'password' => md5(sha1($password) . "e-kompetensi"),
                    'status' => 4,
                    'foto' => 'no-image.jpg',
                    'created_by' => $username,
                    'created_date' => $datetime,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                $result = $this->db->insert(self::__tableName, $data);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">" . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_user->selectById($id);
            if ($brand != null) {
                $data['dataUser'] = $brand;
                $data['datagrup'] = $this->M_user->selectGroup();
                $data['dataStatus'] = $this->M_utilities->selectStatusGrup([], ['aktif', 'belum aktif']);
                $this->loadkonten(self::__folder . 'v_update-user', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $nama = $this->input->post('nama');
        $email = $this->input->post('email');
        $usernameInput = $this->input->post('username');
        $password = $this->input->post('password');
        $grupId = $this->input->post('grup_id');
        $status = $this->input->post('status');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_user->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($nama) == 0) {
                $errCode++;
                $errMessage = "Nama wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($email) == 0) {
                $errCode++;
                $errMessage = "Email wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($usernameInput) == 0) {
                $errCode++;
                $errMessage = "Username wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($grupId) == 0) {
                $errCode++;
                $errMessage = "Group wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($status) == 0) {
                $errCode++;
                $errMessage = "Status wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'grup_id' => $grupId,
                    'nama' => $nama,
                    'email' => $email,
                    'username' => $usernameInput,
                    'status' => $status,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                if (strlen($password) > 0) {
                    $data = array_merge($data, [
                        'password' => md5(sha1($password) . "e-kompetensi"),
                    ]);
                }
                $result = $this->db->update(self::__tableName, $data, [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $errCode = 0;
        $errMessage = "";

        $id = $_POST[self::__tableId];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_user->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $result = $this->db->update(self::__tableName, ['deleted_date' => date('Y-m-d H:i:s')], [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di hapus'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
