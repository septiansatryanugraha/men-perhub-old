<?php
defined('BASEPATH') OR exit('No direct script access allowed');

const __title = 'Konfigurasi Profile ';

class Profile extends AUTH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_sidebar');
        $this->load->model('M_utilities');
    }

    public function index()
    {
        $data['userdata'] = $this->userdata;
        $data['title'] = 'Konfigurasi Profile ';
        $data['breadcrumb'] = '	<li><i class="fa fa-angle-right"></i><a class="ajaxify" href="#">Profile</a></li>';
        $data['datagrup'] = $this->M_utilities->selectGroup();
        $data['dataStatus'] = $this->M_utilities->selectStatusGrup([], ['aktif', 'belum aktif']);
        $this->loadkonten('v_profil/profile', $data);
    }

    public function update()
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]|max_length[30]');
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');
        $data = $this->input->post();

        $nama = $this->input->post('nama');
        $usernameInput = $this->input->post('username');
        $foto = $this->input->post('foto');
        if ($this->form_validation->run() == TRUE) {
            $config['upload_path'] = './upload/user/';
            $config['allowed_types'] = 'jpg|png';
            $this->load->library('upload', $config);
            $data = [
                'nama' => $nama,
                'username' => $usernameInput,
                'updated_by' => $username,
                'updated_date' => $datetime,
            ];
            if (!$this->upload->do_upload('foto')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $dataFoto = $this->upload->data();
                $data = array_merge($data, [
                    'foto' => $dataFoto['file_name'],
                ]);
                $this->session->set_userdata($data);
            }
            $this->db->update('admin', $data, ["id" => $this->session->userdata('id')]);
            if ($this->db->affected_rows() > 0) {
                $this->session->set_userdata('nama', $nama);
                $this->session->set_userdata('username', $username);
                $out = array('status' => true, 'pesan' => ' Profile berhasil di update');
            } else {
                $out = array('status' => false, 'pesan' => 'Maaf data gagal update profile !');
            }
        } else {
            $out = array('status' => false, 'pesan' => 'Maaf data gagal update profile !');
        }

        echo json_encode($out);
    }

    public function ubahPassword()
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $this->form_validation->set_rules('passLama', 'Password Lama', 'trim|required');
        $this->form_validation->set_rules('passBaru', 'Password Baru', 'trim|required');
        $this->form_validation->set_rules('passKonf', 'Password Konfirmasi', 'trim|required');

        $password = $this->input->post('passLama');
        if ($this->form_validation->run() == TRUE) {
            if (md5($this->input->post('passLama')) == $this->session->userdata('password')) {
                if ($this->input->post('passBaru') != $this->input->post('passKonf')) {
                    $out = array('status' => false, 'pesan' => 'Password Baru dan Konfirmasi Password harus sama !');
                } else {
                    $data = [
                        'password' => md5($password),
                        'updated_by' => $username,
                        'updated_date' => $datetime,
                    ];
                    $this->db->update('admin', $data, ["id" => $this->session->userdata('id')]);
                    if ($this->db->affected_rows() > 0) {
                        $this->session->set_userdata('password', $password);
                        $this->updateProfil();
                        $out = array('status' => true, 'pesan' => ' Password berhasil di update');
                    } else {
                        $out = array('status' => false, 'pesan' => 'Maaf gagal update password !');
                    }
                }
            } else {
                $out = array('status' => false, 'pesan' => 'Maaf gagal update password !');
            }
        } else {
            $out = array('status' => false, 'pesan' => 'Maaf gagal update password kosong !');
        }
        echo json_encode($out);
    }
}
