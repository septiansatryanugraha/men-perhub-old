<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_rekap_diklat extends CI_Model
{
    const __tableName = 'tbl_diklat';
    const __tableId = 'id_diklat';

    public function exportData($filter)
    {
        $status = $filter['status'];
        $diklat = $filter['diklat'];
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];
        $allDate = $filter['all_date'];

        $sql = "SELECT " . self::__tableName . ".*
                , tbl_kategori_diklat.kategori_diklat as kategori_diklat
                , tbl_user.nama_lengkap as nama_pemohon
                , tbl_user.no_reg as no_reg_pemohon
                , tbl_user.tempat_lahir as tempat_lahir_pemohon
                , tbl_user.tgl_lahir as tgl_lahir_pemohon
                , tbl_user.asal_instansi as asal_instansi_pemohon
                , kota.nama_kota as kota_pemohon
                , tbl_status_kepegawaian.kode as kode_status_kepegawaian_pemohon
                , tbl_status_kepegawaian.nama as status_kepegawaian_pemohon
                FROM " . self::__tableName . "
                LEFT JOIN tbl_kategori_diklat ON tbl_kategori_diklat.id_kategori_diklat = " . self::__tableName . ".id_kategori_diklat
                LEFT JOIN tbl_user ON tbl_user.id_user = " . self::__tableName . ".id_user
                LEFT JOIN kota ON kota.id_kota = tbl_user.id_kota
                LEFT JOIN tbl_status_kepegawaian ON tbl_status_kepegawaian.id = tbl_user.id_status_kepegawaian
                WHERE " . self::__tableName . ".deleted_date IS NULL";
        if (strlen($status) > 0 && $status != 'all') {
            $sql .= " AND " . self::__tableName . ".status_pengajuan = '{$status}'";
        }
        if (strlen($diklat) > 0 && $diklat != 'all') {
            $sql .= " AND " . self::__tableName . ".id_kategori_diklat = '{$diklat}'";
        }
        if ($allDate == 0) {
            if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
                $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
                $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
                $sql .= " AND " . self::__tableName . ".tanggal >= '{$tanggalAwal}' AND " . self::__tableName . ".tanggal <= '{$tanggalAkhir}'";
            }
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectDiklat()
    {
        $sql = "SELECT * 
                FROM tbl_kategori_diklat
                WHERE deleted_date IS NULL";
        $data = $this->db->query($sql);

        return $data->result();
    }
}
