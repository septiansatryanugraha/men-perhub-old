<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_rekap_jenjang extends CI_Model
{
    const __tableName = 'tbl_kenaikan_jenjang';
    const __tableId = 'id_jenjang';

    public function exportData($filter)
    {
        $status = $filter['status'];
        $jenjang = $filter['jenjang'];
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];
        $allDate = $filter['all_date'];

        $sql = "SELECT " . self::__tableName . ".*
                , jenis_jenjang.jenjang as jenis_jenjang
                , tbl_user.nama_lengkap as nama_pemohon
                , tbl_user.no_reg as no_reg_pemohon
                , tbl_user.tempat_lahir as tempat_lahir_pemohon
                , tbl_user.tgl_lahir as tgl_lahir_pemohon
                , tbl_user.asal_instansi as asal_instansi_pemohon
                , kota.nama_kota as kota_pemohon
                , tbl_status_kepegawaian.kode as kode_status_kepegawaian_pemohon
                , tbl_status_kepegawaian.nama as status_kepegawaian_pemohon
                FROM " . self::__tableName . "
                LEFT JOIN jenis_jenjang ON jenis_jenjang.id = " . self::__tableName . ".id_jenis_jenjang
                LEFT JOIN tbl_user ON tbl_user.id_user = " . self::__tableName . ".id_user
                LEFT JOIN kota ON kota.id_kota = tbl_user.id_kota
                LEFT JOIN tbl_status_kepegawaian ON tbl_status_kepegawaian.id = tbl_user.id_status_kepegawaian
                WHERE " . self::__tableName . ".deleted_date IS NULL";
        if (strlen($status) > 0 && $status != 'all') {
            $sql .= " AND " . self::__tableName . ".status_pengajuan = '{$status}'";
        }
        if (strlen($jenjang) > 0 && $jenjang != 'all') {
            $sql .= " AND " . self::__tableName . ".id_jenis_jenjang = '{$jenjang}'";
        }
        if ($allDate == 0) {
            if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
                $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
                $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
                $sql .= " AND " . self::__tableName . ".tanggal >= '{$tanggalAwal}' AND " . self::__tableName . ".tanggal <= '{$tanggalAkhir}'";
            }
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectJenjang()
    {
        $sql = "SELECT * 
                FROM jenis_jenjang
                WHERE deleted_date IS NULL
                AND jenjang <> 'Pembantu Penguji'";
        $data = $this->db->query($sql);

        return $data->result();
    }
}
