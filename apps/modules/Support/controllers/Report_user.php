<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_user extends AUTH_Controller
{
    const __folder = 'v_report-user/';
    const __kode_menu = 'report-user';
    const __title = 'Report User Penguji ';
    const __model = 'M_rekap_user';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
    }

    public function index()
    {
        $data['userdata'] = $this->session->userdata();
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $this->loadkonten('' . self::__folder . 'v_rekap-data', $data);
        }
    }

    public function ajaxReport()
    {
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview > 0) {
            $tanggalAwal = $this->input->post('tanggal_awal');
            $tanggalAkhir = $this->input->post('tanggal_akhir');
            $allDate = $this->input->post('all_date');

            $filter = [
                'tanggal_awal' => $tanggalAwal,
                'tanggal_akhir' => $tanggalAkhir,
                'all_date' => $allDate,
            ];

            $list = $this->M_rekap_user->exportData($filter);

            $data = [];
            foreach ($list as $key => $value) {
                $row = [];
                $row[] = $key + 1;
                $row[] = $value->no_reg;
                $row[] = $value->nama_lengkap;
                $row[] = $value->email;
                $row[] = $value->jkel;
                $row[] = date('d-m-Y', strtotime($value->tanggal));
                $data[] = $row;
            }
        } else {
            $data = ["You don't have permission"];
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function exportExcel()
    {
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview > 0) {
            $tanggalAwal = $this->input->post('tanggal_awal');
            $tanggalAkhir = $this->input->post('tanggal_akhir');
            $allDate = $this->input->post('all_date');

            $filter = [
                'tahun' => $tahun,
                'tanggal_awal' => $tanggalAwal,
                'tanggal_akhir' => $tanggalAkhir,
                'all_date' => $allDate,
            ];

            $data['excel'] = $this->M_rekap_user->exportData($filter);
            $data['title'] = "Report Data User Penguji E-Kopetensi " . date("Y-m-d h:i");
            $data['tanggalAwal'] = $tanggalAwal;
            $data['tanggalAkhir'] = $tanggalAkhir;
            $data['allDate'] = $allDate;

            $this->load->view('' . self::__folder . 'v_laporan_excel', $data);
        } else {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        }
    }
}
