<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_perpanjangan extends AUTH_Controller
{
    const __folder = 'v_report-perpanjangan/';
    const __kode_menu = 'report-perpanjangan';
    const __title = 'Report Perpanjangan Kompetensi';
    const __model = 'M_rekap_perpanjangan';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
        $this->load->model('M_utilities');
        $this->load->model('M_jenis_jenjang');
    }

    public function index()
    {
        $data['userdata'] = $this->session->userdata();
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['jenjang'] = $this->M_rekap_perpanjangan->selectJenjang();
            $data['status'] = $this->M_utilities->selectStatusGrup([], ['Pending', 'Proses Verifikasi']);
            $this->loadkonten('' . self::__folder . 'v_rekap-data', $data);
        }
    }

    public function ajaxReport()
    {
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview > 0) {
            $status = $this->input->post('status');
            $jenjang = $this->input->post('jenjang');
            $tanggalAwal = $this->input->post('tanggal_awal');
            $tanggalAkhir = $this->input->post('tanggal_akhir');
            $allDate = $this->input->post('all_date');

            $filter = [
                'status' => $status,
                'jenjang' => $jenjang,
                'tanggal_awal' => $tanggalAwal,
                'tanggal_akhir' => $tanggalAkhir,
                'all_date' => $allDate,
            ];

            $list = $this->M_rekap_perpanjangan->exportData($filter);

            $data = [];
            foreach ($list as $key => $value) {
                $row = [];
                $row[] = $key + 1;
                $row[] = $value->kode_pengajuan;
                $row[] = $value->nama_pemohon;
                $row[] = $value->email;
                $row[] = $value->jenis_perpanjangan;
                $row[] = date('d-m-Y', strtotime($value->tanggal));
                $data[] = $row;
            }
        } else {
            $data = ["You don't have permission"];
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function exportExcel()
    {
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview > 0) {
            $status = $this->input->post('status');
            $jenjang = $this->input->post('jenjang');
            $tanggalAwal = $this->input->post('tanggal_awal');
            $tanggalAkhir = $this->input->post('tanggal_akhir');
            $allDate = $this->input->post('all_date');

            $filter = [
                'status' => $status,
                'jenjang' => $jenjang,
                'tanggal_awal' => $tanggalAwal,
                'tanggal_akhir' => $tanggalAkhir,
                'all_date' => $allDate,
            ];

            $data['excel'] = $this->M_rekap_perpanjangan->exportData($filter);
            $data['title'] = "Report Data Pengajuan Perpanjangan Sertifikasi E-Kopetensi " . date("Y-m-d h:i");
            $data['status'] = ($status == 'all') ? "Semua Status Pengajuan" : $status;
            $data['jenjang'] = ($jenjang == 'all') ? "Semua Tingkat Jenjang" : $this->M_jenis_jenjang->selectById($jenjang)->jenjang;
            $data['tanggalAwal'] = $tanggalAwal;
            $data['tanggalAkhir'] = $tanggalAkhir;
            $data['allDate'] = $allDate;

            $this->load->view('' . self::__folder . 'v_laporan_excel', $data);
        } else {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        }
    }
}
