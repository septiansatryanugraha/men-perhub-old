<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<table><tr><td colspan="10" style="text-align: center; vertical-align: middle;"><h2>Report Data Perpanjangan Sertifikasi Kompetensi</h2></td></tr></table>
<br>
<table>
    <tr><td colspan="10">Filter</td></tr>
    <tr>
        <td></td>
        <td>Status Pengajuan<span class="pull right">:</span></td>
        <td colspan="8" style="text-align: left;"><?= $status ?></td>
    </tr>
    <tr>
        <td></td>
        <td>Tingkat Jenjang<span class="pull right">:</span></td>
        <td colspan="8" style="text-align: left;"><?= $jenjang ?></td>
    </tr>
    <tr>
        <td></td>
        <td>Tanggal Pengajuan<span class="pull right">:</span></td>
        <td colspan="8" style="text-align: left;"><?= ($allDate > 0) ? "Semua Periode" : date('d-m-Y', strtotime($tanggalAwal)) . " - " . date('d-m-Y', strtotime($tanggalAkhir)); ?></td>
    </tr>
</table>
<br><br>
<table border="1" width="80%">
    <thead>
        <tr>
            <th align="center">No</th>
            <th align="center">Kode Pengajuan</th>
            <th align="center">Jenis Perpanjangan</th>
            <th align="center">Email</th>
            <th align="center">Nama</th>
            <th align="center">Status Kepegawaian</th>
            <th align="center">Tempat / Tanggal Lahir</th>
            <th align="center">Asal Instansi</th>
            <th align="center">Status Perpanjangan</th>
            <th align="center">Tanggal Pengajuan</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($excel)) {
            foreach ($excel as $key => $data) {

                ?>
                <tr>
                    <td><?= $key + 1 ?></td>
                    <td><?= $data->kode_pengajuan ?></td>
                    <td><?= $data->jenis_perpanjangan ?></td>
                    <td><?= $data->email ?></td>
                    <td><?= $data->nama_pemohon ?></td>
                    <td><?= $data->status_kepegawaian_pemohon ?></td>
                    <td><?= $data->tempat_lahir_pemohon . ", " . date('d-m-Y', strtotime($data->tgl_lahir_pemohon)); ?>&nbsp;</td>
                    <td><?= $data->asal_instansi_pemohon ?></td>
                    <td><?= $data->status_perpanjangan ?></td>
                    <td><?= date('d-m-Y', strtotime($data->tanggal)); ?>&nbsp;</td>
                </tr>    
                <?php
            }
        } else {

            ?>
            <tr><td colspan="10">Belum Ada Data</td></tr>
        <?php } ?>
    </tbody>
</table>