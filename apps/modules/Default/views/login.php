<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <title>Login - E-Kompetensi</title>
        <link rel="icon" href="<?= base_url() ?>/assets/tambahan/gambar/logo-dishub.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?= theme(); ?>/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= theme(); ?>/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= theme(); ?>/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?= theme(); ?>/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="<?= theme(); ?>/pages/css/login-soft.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?= theme(); ?>/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?= theme(); ?>/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?= theme(); ?>/assets/css/layout.css" rel="stylesheet" type="text/css"/>
        <link id="style_color" href="<?= theme(); ?>/assets/css/themes/darkblue.css" rel="stylesheet" type="text/css"/>
        <script src="<?= theme(); ?>/global/plugins/jquery.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/sweetalert/sweetalert.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/toastr/toastr.css">
        <?= $script_captcha; // javascript recaptcha ?>
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <style>
        .field-icon {
            float: left;
            margin-left: 91%;
            margin-top: -25px;
            font-size: 25px;
            position: relative;
            z-index: 2;
            color: #ccc;
        }
    </style>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <img src="<?= $this->config->item('login_logo'); ?>" height="80px"/>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="menu-toggler sidebar-toggler">
        </div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form action="<?= base_url('process-login'); ?>" method="post" id="FormLogin">
                <center><h3 class="form-title">Silahkan Login</h3></center>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" id="username" type="text" autocomplete="off" placeholder="Username" name="username"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" id="password-field" type="password" autocomplete="off" placeholder="Password" name="password"/><span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                    </div>
                </div>
                <?= $captcha ?>
                <div class="col-8"><div class="icheck-primary">
                        <input type="checkbox" name="remember" id="remember">
                        <label for="remember">Ingat akun saya</label>
                    </div>
                </div>
                <div id='btn_loading'></div>
                <div id="hilang">
                    <div class="form-actions">      
                        <button type="submit" id="btnSignUp" class="btn blue pull-right">
                            <i class="fa fa-lock" aria-hidden="true"></i> &nbsp;Login
                        </button>
                    </div>
                </div> 
                <div id="buka">
                    <div class="form-actions">      
                        <button type="submit" id="btnSignUp" class="btn blue pull-right">
                            <i class="fa fa-unlock" aria-hidden="true"></i> &nbsp;Login
                        </button>
                    </div>
                </div>  
            </form>
            <!-- END LOGIN FORM -->
            <br>
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright">
            <?= $this->config->item('footer_login_title'); ?>
        </div>
        <script src="<?= theme(); ?>/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?= theme(); ?>/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= theme(); ?>/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?= theme(); ?>/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?= theme(); ?>/global/plugins/select2/select2.min.js"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= theme(); ?>/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?= theme(); ?>/assets/scripts/layout.js" type="text/javascript"></script>
        <script src="<?= theme(); ?>/assets/scripts/demo.js" type="text/javascript"></script>
        <script src="<?= theme(); ?>/pages/scripts/login-soft.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assets/toastr/toastr.js"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script type="text/javascript" >
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                Login.init();
                Demo.init();
                // init background slide images
                $.backstretch([
                    "<?= theme(); ?>/pages/media/bg/1.jpg",
                    "<?= theme(); ?>/pages/media/bg/2.jpg",
                    "<?= theme(); ?>/pages/media/bg/3.jpg",
                ], {
                    fade: 1000,
                    duration: 8000
                }
                );
            });

            function formValidation(oEvent) {
                oEvent = oEvent || window.event;
                var txtField = oEvent.target || oEvent.srcElement;
                var t1ck = true;
                var msg = " ";
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if (document.getElementById("username").value.length < 5) {
                    t1ck = false;
                    msg = msg + "Your name should be minimun 3 char length";
                }
                if (document.getElementById("password-field").value.length < 5) {
                    t1ck = false;
                    msg = msg + "Your name should be minimun 3 char length";
                }
                if (t1ck) {
                    document.getElementById("btnSignUp").disabled = false;
                } else {
                    document.getElementById("btnSignUp").disabled = true;
                }
            }

            function resetForm() {
                document.getElementById("btnSignUp").disabled = true;
                var frmMain = document.forms[0];
                frmMain.reset();
            }

            window.onload = function () {
                var btnSignUp = document.getElementById("btnSignUp");
                var username = document.getElementById("username");
                var password = document.getElementById("password-field");

                var t1ck = false;
                document.getElementById("btnSignUp").disabled = true;
                username.onkeyup = formValidation;
                password.onkeyup = formValidation;
            }

            $(".toggle-password").click(function () {
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });

            $(function () {
                $("#buka").hide();
                $('#FormLogin').submit(function (e) {
                    e.preventDefault();
                    $.ajax({
                        beforeSend: function () {
                            $("#buka").hide();
                            $("#hilang").hide();
                            $("#btn_loading").html("<div class='form-actions'><button class='btn blue pull-right'><i class='fa fa-refresh fa-spin'></i> &nbsp;Wait..</button></div>");
                            $("#btn_loading").show();
                        },
                        url: $(this).attr('action'),
                        type: "POST",
                        cache: false,
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (json) {
                            if (json.status == true) {
                                $("#btn_loading").hide();
                                $("#buka").show();
                                toastr.success(json.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                                    "closeButton": true});
                                window.location = json.url_home;
                            } else {
                                $("#btn_loading").hide();
                                $("#hilang").show();
                                toastr.error(json.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                                    "closeButton": true});
                            }
                        }
                    });
                });
            });
        </script>
    </body>
</html>