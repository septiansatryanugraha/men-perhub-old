<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_auth');
        $this->load->library('form_validation', 'Cookie');
    }

    public function index()
    {
        $session = $this->session->userdata('status');
        $cookie = get_cookie('wahyu');

        if ($session == true) {
            redirect('dashboard');
        } else if ($cookie <> '') {
            // cek cookie
            $data = $this->M_auth->get_by_cookie($cookie)->row();
            if ($data) {
                $this->_daftarkan_session($data);
            } else {
                $data = [
                    'username' => set_value('username'),
                    'password' => set_value('password'),
                    'remember' => set_value('remember'),
                    'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                    'script_captcha' => $this->recaptcha->getScriptTag(),
                ];
                $this->load->view('login', $data);
            }
        } else {
            $data = [
                'username' => set_value('username'),
                'password' => set_value('password'),
                'remember' => set_value('remember'),
                'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                'script_captcha' => $this->recaptcha->getScriptTag(),
            ];
            $this->load->view('login', $data);
        }
    }

    public function login()
    {
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('username', 'Username', 'required|min_length[4]|max_length[30]');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('g-recaptcha-response', '<strong>Captcha</strong>', 'callback_getResponseCaptcha');
            $recaptcha = $this->input->post('g-recaptcha-response');
            $response = $this->recaptcha->verifyResponse($recaptcha);

            if ($this->form_validation->run() == TRUE || !isset($response['success']) || $response['success'] == true) {
                $username = trim($_POST['username']);
                $password = trim($_POST['password']);
                $remember = trim($_POST['remember']);

                $data = $this->M_auth->login($username, $password);
                /* ketika status aktif tidak sama */
                if ($remember) {
                    $key = random_string('alnum', 64);
                    set_cookie('wahyu', $key, 3600 * 24 * 30); // set expired 30 hari kedepan
                    // simpan key di database
                    $update_key = ['cookie' => $key];
                    $this->M_auth->update_cookie($update_key, $data->id);
                }
                $this->_daftarkan_session($data);

                $session['id'] = $data->id;
                $session['username'] = $data->username;
                $session['status'] = $data->status;

                $stat = $data->status;
                if ($stat == 3) {
                    $this->session->set_userdata($session);
                    $status = 3;
                    $this->M_auth->update($session['id'], $status);
                    $data = ['last_login_user' => date('Y-m-d H:i:s')];
                    $this->M_auth->update_user($session['id'], $data);
                    $URL_home = base_url('Dashboard');
                    $json['status'] = true;
                    $json['url_home'] = $URL_home;
                    $json['pesan'] = "Success Login.";
                } else {
                    $json['pesan'] = "Maaf akun belum aktif.";
                }
                if ($data == false) {
                    $json['pesan'] = "Username / Password salah";
                } else {
                    $this->session->set_userdata($session);
                }
            } elseif (isset($recaptcha)) {
                $json['pesan'] = "Captcha harus di isi";
            } else {
                $json['pesan'] = "Maaf tidak bisa login, silahkan cek user dan password anda";
            }
        } else {
            redirect('login');
        }

        echo json_encode($json);
    }

    public function getResponseCaptcha($str)
    {
        $this->load->library('recaptcha');
        $response = $this->recaptcha->verifyResponse($str);
        if ($response['success']) {
            return true;
        } else {
            $this->form_validation->set_message('getResponseCaptcha', '%s is required.');
            return false;
        }
    }

    public function _daftarkan_session($data)
    {
        $sess = [
            'id' => $data->id,
            'username' => $data->username,
        ];
        $this->session->set_userdata($sess);
        $URL_home = base_url('Dashboard');
        $json['status'] = true;
        $json['url_home'] = $URL_home;
        $json['pesan'] = "Success Login.";
    }

    public function logout()
    {
        delete_cookie('wahyu');
        $this->session->sess_destroy();
        redirect('login');
    }
}
