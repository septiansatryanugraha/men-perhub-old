<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
// if (!isset($_SERVER['PHP_AUTH_USER'])) { 
// header('WWW-Authenticate: Basic realm="JDIH"'); 
// header('HTTP/1.0 401 Unauthorized'); 
//  echo 'Fail To Authenticating'; 
// exit; 
// } else {
// if ($_SERVER['PHP_AUTH_USER'] == 'wahyu' && ($_SERVER['PHP_AUTH_PW']) == 'wahyu');

class WebService extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_service');
	}
	
	
    	// function login web service
     	function checklogin(){
	    
         $datainput = json_decode(file_get_contents('php://input'), true);
               
                $data=array(
                    'email'=>$datainput['email'],
                    'password'=> base64_encode($datainput['password']),
                );
                $res=$this->M_service->login($data);
                
                $data2=array(
                    'last_login_user' => date('Y-m-d H:i:s')
                );
                
                $this->M_service->update_user($user,$data2);
                
                $email=$datainput['email'];


                if($res){
	                if($res[0]->tipe == "opd"){
		                
		                
	                	$res2=$this->M_service->getUser($res[0]->email);
	                	$res3 = $this->M_service->selek_photo_opd2($email);
	                	$res4=$this->M_service->getPassword($res[0]->email);
	                	$decode=base64_decode ($res4->password);
		                $result = array(
		                	'status'=> 'true',
		                	'tipe'=> 'opd',
		                	'data'=> $res2,
		                	'gambar'=> $res3,
		                	'encode_pass'=> $decode,
							
		            	);
		                echo json_encode($result);
					}
						
				    else if($res[0]->status == "Non aktif"){
		               
		                $result = array(
		                	'status'=> 'false',
							'pesan'=> 'Maaf akun belum aktif',
		            	);
		                echo json_encode($result);
						
	                } else {

	                	$res2=$this->M_service->getBiro($res[0]->email);
	                	$res3 = $this->M_service->selek_photo_opd2($email);
						
		                $result = array(
		                	'status'=> 'true',
		                	'tipe'=> 'biro',
		                	'data'=> $res2,
		                	'gambar'=> $res3,
		            	);
		                echo json_encode($result);
	                }
	            }
				
				  else{
	                $result = array(
	                	'status'=>  'false',
						'pesan'=> 'Maaf username atau password anda salah',
	            	);
	                echo json_encode($result);
	            }
			}
			
			
		// registrasi siswa
		public function daftar() {
		
		$date2 = date('Y-m-d');
        $date = date('Y-m-d H:i:s');
   
        $data = array(
				'nama_lengkap'	=> $this->input->post('nama_lengkap'),
				'nik'	        => $this->input->post('nik'),
				'jabatan'	    => $this->input->post('jabatan'),
				'dinas'	        => $this->input->post('dinas'),
				'nama'          => $this->input->post('nama_lengkap'),
				'nomor_hp'	    => $this->input->post('no_hp'),
		        'email'			=> $this->input->post('email'),
		        'password' 		=> base64_encode($this->input->post("password")),
		        'ip_address'	=> $this->input->ip_address(),
		        'user_agent'	=> $this->input->user_agent(),
		        'created_by'    => 'user opd' ,
		        'created_date'  => $date,
                'updated_date'  => $date,
                'tanggal'       => $date2
			);
		
		$data2=array(
			'nama' 		             => $this->input->post('nama_lengkap'),
			'email' 		   		 => $this->input->post('email'),
			'status' 		         => 'Aktif',	
			'tipe'                   => 'opd',		
			'password' 		   		 => base64_encode($this->input->post("password")),
		 );	
		
        
         $checkuser = $this->M_service->checkUsername($data);
	     if(!$checkuser){
		 $res=$this->M_service->saveRegis($data);
		 $res=$this->M_service->insertLogin($data2);
		 $result = array(
	     'status'=>  'true',
		 'pesan'=>   'Selamat pendaftaran berhasil !!',
	      );
		 echo json_encode($result);
		 } 
		 else {
		 $result = array(
	     'status'=>  'false',
		 'pesan'=>   'Username sudah terdaftar, gunakan Username yang lain !!',
	      );
		 echo json_encode($result);
		 }
      }
      
      
     
      

	   // selek web service dokumen (id)
	      public function selek_dokumen(){
	      $id_user= $this->input->post('id_user');
		  $res = $this->M_service->getDokumen($id_user);
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false',
           'pesan'=>  'Data masih kosong, belum pernah melakukan pengajuan'
           );  
           } 	
            echo json_encode($result);
		}

		// selek web service dokumen (id)
	      public function selek_detail_histori(){
	      $kode_naskah= $this->input->post('no_surat');
		  $res = $this->M_service->select_histori_naskah($kode_naskah);
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false',
           'pesan'=>   'Belum ada histori pengajuan naskah '
           );  
           } 	
            echo json_encode($result);
		}

		// selek web service dokumen (id)
	      public function selek_status_final(){
	      $id_user= $this->input->post('id_user');
		  $res = $this->M_service->selek_status_final($id_user);
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false',
           'pesan'=>   'Belum ada histori pengajuan naskah '
           );  
           } 	
            echo json_encode($result);
		}


		// selek web service dokumen (all)
	      public function selek_dokumen_all(){
		  $res = $this->M_service->getDokumen2();
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false'
           );  
           } 	
            echo json_encode($result);
		}
		
		// selek web service dokumen (all)
	      public function selek_dokumen_done(){
		  $res = $this->M_service->getDokumen_selesai();
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false'
           );  
           } 	
            header('content-type: application/json');
            echo json_encode($result);
		}
		
		// selek web service dokumen (all)
	      public function selek_panduan(){
		  $res = $this->M_service->getPanduan();
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false'
           );  
           } 	
            echo json_encode($result);
		}


		 // selek web service dokumen (all)
	      public function selek_dokumen_selesai(){
		  $res = $this->M_service->getDokumen3();
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
           
		   } else {
           $result = array(
           'status'=>  'false',
           'pesan'=>  'Data masih kosong'
           );  
           } 	
            echo json_encode($result);
		}
	  

		// selek web service dinas
	      public function selek_dinas(){
		  $res = $this->M_service->selek_dinas();
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false'
           );  
           } 	
            echo json_encode($result);
		}
		
		// selek web service dinas
	      public function selek_history(){
	      $id_user= $this->input->post('id_user');
		  $res = $this->M_service->getHistory($id_user);
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false'
           );  
           } 	
           echo json_encode($result);
		}

		
		// selek web service slider
	      public function selek_slider(){
		  $res = $this->M_service->selek_slider();
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false'
           );  
           } 	
           echo json_encode($result);
		}
		
		// selek web service dinas
	      public function selek_history_all(){
		  $res = $this->M_service->getHistory2();
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false'
           );  
           } 	
           echo json_encode($result);
		}
		
		// selek web kontak
	      public function selek_kontak(){
		  $res = $this->M_service->selek_kontak();
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false'
           );  
           } 	
           echo json_encode($result);
		}
		
		// untuk edit profile siswa foto
			function editphoto(){
				
				$config['upload_path']="./upload/gambar/";
				$config['allowed_types']='gif|jpg|png|jpeg';
				$config['max_size'] = '2048'; //maksimum besar file 2M
				$config['encrypt_name'] = TRUE;
				$config['overwrite'] = TRUE;
 
				$this->load->library('upload', $config);
				$path['link']= "upload/gambar/";
 
				$this->upload->do_upload('photo');
				$image_data = $this->upload->data();
				$nama = $this->input->post('nama');
        
        
			    $data=array(
				'nama' 		   		 => $this->input->post('nama'),
				'photo'              => $path['link'] . ''. $image_data['file_name'],				
				);
				
				$data2=array(
				'nama' 		   		 => $this->input->post('nama'),
				'photo'              => $path['link'] . ''. $image_data['file_name'],				
				);
				$res = $this->M_service->updatePhotoOpd($data);
				$res = $this->M_service->updateOpdLogin($data2);
				$res = $this->M_service->selek_photo_opd($nama);
				
				if($res){
				$result = array(
                	'status'=>  'true',
                	'pesan' =>  'Photo berhasil di update',
                	'gambar'  => $res,
            	);
				}

            	else
            	{
            	    $result = array(
                	'status'=>  'false',
					'pesan' =>  'photo gagal di update',
            	 );
            	 
            	}
				
            	echo json_encode($result);
		 
			}
		
			
	    // untuk edit profile member
		 function edit_opd(){
			
			$data = array(
			    'nama'          => $this->input->post('nama'),
				'nama_lengkap'	=> $this->input->post('nama_lengkap'),
				'nik'	        => $this->input->post('nik'),
				'jabatan'	    => $this->input->post('jabatan'),
				'dinas'	        => $this->input->post('kat_dinas'),
				'nomor_hp'	    => $this->input->post('nomor_hp'),
		        'password' 		=> base64_encode($this->input->post("password")),
				);

            $data2 = array(
                    'nama'              => $this->input->post('nama'),
					'password' 		    => $this->input->post('password')
				);
			$res = $this->M_service->updateOpd($data);
            $res = $this->M_service->updateOpdLogin($data2);
				
			if($res){
			$result = array(
                       'status'=>  'true',
                       'pesan' =>  'Profil berhasil di update',
            	     );
				}
                else
            	{
            	    $result = array(
                	'status'=>  'false',
                	'pesan' =>  'Profil gagal di update',
            	 );  
            	}
            	
            echo json_encode($result);
			}
				
}
