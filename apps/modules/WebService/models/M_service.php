<?php
class M_service extends CI_Model{

   	
	public function login($data) {
	   
		$this->db->select('*');
		$this->db->from('tbl_login');
		$this->db->where('email', $data['email']);
		$this->db->where('password', $data['password']);
		
		$this->db->limit(1);
		 
		$query = $this->db->get();
		if($query->num_rows() == 1) { 
			return $query->result();
		} else {
			return false;
		}
	}

	public function selek_slider() {
		
		$sql = " select * from tbl_slider WHERE status ='Aktif'";
		$data = $this->db->query($sql);
		return $data->result();
	}

	public function kode(){
		  $this->db->select('RIGHT(tbl_dokumen.kode,2) as kode', FALSE);
		  $this->db->order_by('kode','DESC');    
		  $this->db->limit(1);    
		  $query = $this->db->get('tbl_dokumen');  //cek dulu apakah ada sudah ada kode di tabel.    
		  if($query->num_rows() <> 0){      
			   //cek kode jika telah tersedia    
			   $data = $query->row();      
			   $kode = intval($data->kode) + 1; 
		  }
		  else{      
			   $kode = 1;  //cek jika kode belum terdapat pada table
		  } 
			  $tahun=date('Y');
			  $bulan=date('m');
			  $tahun=date('Y');
			  $batas = str_pad($kode, 3, "0", STR_PAD_LEFT);    
			  $kodetampil = "NSK".$tahun.$bulan.$batas;  //format kode
			  return $kodetampil;  
		 }
	
	function update_user($id,$data2)
    {
        $this->db->where('email', $id);
        $this->db->update('tbl_login', $data2);
        return TRUE;
    }

      public function getDokumen($id_user){
		$sql = "SELECT * FROM tbl_dokumen WHERE id_user='$id_user' order by id_dokumen desc";
		$query = $this->db->query($sql);
		return $query->result();
	}

	  public function getDokumen2(){
		$sql = "SELECT * FROM tbl_dokumen order by id_dokumen desc";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	  public function getHistory($id_user){
		$sql = "SELECT * FROM tbl_history_dokumen WHERE id_user='$id_user'";
		$query = $this->db->query($sql);
		return $query->result();
	}

	  public function getHistory2(){
		$sql = "SELECT * FROM tbl_history_dokumen order by id_history desc";
		$query = $this->db->query($sql);
		return $query->result();
	}

	 public function getDokumen3(){
		$sql = "SELECT * FROM tbl_dokumen WHERE keterangan_status in ('Proses Dikembalikan ke Perangkat Daerah','Sudah Dapat Diambil','Telah dikirim melalui Bagian Arsip dan Ekspedisi Biro Umum','Telah diambil') ";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	public function getDokumen_selesai(){
		$sql = "SELECT * FROM tbl_dokumen WHERE status_dokumen in ('Selesai') ";
		$query = $this->db->query($sql);
		return $query->result();
	}
    
	function selek_dinas()
	{
		$data = $this->db->get('tbl_cat_opd');
		return $data->result();
	}
	
		function selek_kontak()
	{
		$data = $this->db->get('tbl_kontak');
		return $data->result();
	}
	
	public function saveRegis($data){
		$query = $this->db->insert('tbl_user', $data); 
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	
	public function insertLogin($data){
		$query = $this->db->insert('tbl_login', $data); 
		return $query;
	}
	
		function getPanduan()
	{
		$data = $this->db->get('tbl_bantuan');
		return $data->result();
	}

	public function select_histori_naskah($id) {
        $sql = "SELECT * FROM tbl_history_dokumen WHERE no_surat = '{$id}' order by id_history desc";
        $data = $this->db->query($sql);
        return $data->result();
    }

     public function selek_status_final($id_user){
		$sql = " select * from tbl_dokumen WHERE id_user='$id_user' AND keterangan_status in ('Proses Dikembalikan ke Perangkat Daerah','Sudah Dapat Diambil','Telah dikirim melalui Bagian Arsip dan Ekspedisi Biro Umum','Telah diambil') ";
        $data = $this->db->query($sql);
        return $data->result();
	}
	
	
	
		public function updateOpd($data)
	{
		$this->db->where('nama',$data['nama']);
		$query=$this->db->update('tbl_user',$data); 
		return $query;
	}
	
		public function updateOpdLogin($data)
	{
		$this->db->where('nama',$data['nama']);
		$query=$this->db->update('tbl_login',$data); 
		return $query;
	}
	
		public function updatePhotoOpd($data)
	{
		$this->db->where('nama',$data['nama']);
		$query=$this->db->update('tbl_user',$data); 
		return $query;
	}
	
		public function updatePhotoLogin($data)
	{
		$this->db->where('nama',$data['nama']);
		$query=$this->db->update('tbl_login',$data); 
		return $query;
	}
	
	 public function selek_photo_opd($nama){
		$sql = "SELECT photo FROM tbl_login WHERE nama='$nama'";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	public function selek_photo_opd2($email){
		$sql = "SELECT photo FROM tbl_login WHERE email='$email'";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	
	 public function getUser($email){
		$sql = "SELECT * FROM tbl_user WHERE email='$email'";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	public function getPassword($email){
		$sql = "SELECT password FROM tbl_user WHERE email='$email'";
		$data = $this->db->query($sql);
        return $data->row();
	}

	public function getBiro($email){
		$sql = "SELECT * FROM tbl_biro WHERE email='$email'";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	
	
	    public function checkUsername($data){
		$this->db->select('email');
		$this->db->from('tbl_user');
		$this->db->where('email', $data['email']);
		$this->db->limit(1);
		 
		$query = $this->db->get();
		if($query->num_rows() == 1) { 
			return $query->result();
		} else {
			return false;
		}
	}
}
?>