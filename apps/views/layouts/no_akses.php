<div class="page-content">
      
      
      <!-- BEGIN PAGE HEADER-->
      <h3 class="page-title">
        <?= (isset($title))?$title:''; ?>
        <small><?= (isset($subtitle))?$subtitle:''; ?></small>
      </h3>
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li>
            <i class="fa fa-home"></i>
            <a class="ajaxify" href="<?= base_url() ?>Dashboard">Home</a>           
          </li>
          <?= (isset($breadcrumb))?$breadcrumb:''; ?>
        </ul>
        
      </div>
      <!-- END PAGE HEADER-->
      <!-- BEGIN PAGE CONTENT-->

      <div class="row">
  <div class="col-md-12">
    <div class="portlet box green">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-exclamation-triangle"></i> Error Page
        </div>
      </div>
      <div class="portlet-body">
                  <div class="caution" style=" margin-top:50px; text-align:center;"><img src="<?php echo base_url().'assets/' ?>tambahan/gambar/caution.gif" width="150px"></div>
                    <center><h1 class="page_error_code text-primary"><b>Oopppsss!!!</b></h1></center>
                     <center><h2 class="page_error_info">Maaf Anda tidak punya akses ke halaman <font color="red"><?php echo @$judul; ?></font></h2></center>
                    <div class="col-md-6 col-sm-6 col-xs-8 col-md-offset-3 col-sm-offset-3 col-xs-offset-2">                        
                    </div>
                
            </div>
        </div>
  
  </div>
    </div>
  </div> 

